# Table of content

[[_TOC_]]

# User Auth

User Authorization of the application should use standards from `RFC 6749` which stands for
*The OAuth 2.0 Authorization Framework* . All standards included in this document 
[RFC 6749](https://tools.ietf.org/html/rfc6749). 

## Authorization Code Grant

OAuth2 uses several steps to grand authorization of user requests, in order to follow
OAuth2 standard there should not be any **password** in any of the steps. 

The Authorization process goes from Client side through **UserAgent** to **Authorization Server** 
and **Resource Owner**. **Resource Owner** then grand access to **UserAgent** which gives it to the 
**Authorization Server**, and back to the **Client**. The next step happens when **Client** send 
Auth Code & Redirect URI to the **Authorization Server** and receives back AccessToken.

OAuth2 standards includes schema of this process in this [section](https://tools.ietf.org/html/rfc6749#section-4.1)

# Application Authorization Flow



# Application User Schema 

Visit [Graphviz](https://www.graphviz.org/download/) and follow a proper installation instruction.

You also may need [PlantUML integration IntelliJ plugin](https://plugins.jetbrains.com/plugin/7017-plantuml-integration).

Then see [plant uml](./user.plantuml).

# Worth to read / watch 

[OktaDev](https://www.youtube.com/watch?v=g_aVPdwBTfw)