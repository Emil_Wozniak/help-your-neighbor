module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true
  },
  extends: ["eslint:recommended", "plugin:react/recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    },
    settings: {
      react: {
        createClass: "createReactClass",
        fragment: "React.Fragment",
        version: "detect",
        flowVersion: "0.53"
      },
      propWrapperFunctions: [
        "forbidExtraProps",
        { property: "freeze", object: "Object" }
      ],
      linkComponents: ["Hyperlink", { name: "Link", linkAttribute: "to" }]
    },
    compilerOptions: {
      jsx: "react",
      target: "es6",
      module: "esnext",
      moduleResolution: "node",
      sourceMap: true,
      emitDecoratorMetadata: true,
      experimentalDecorators: true,
      removeComments: false,
      noImplicitAny: false,
      suppressImplicitAnyIndexErrors: true,
      outDir: "build/resources/main/static/app",
      lib: ["es2015", "es2017", "dom"],
      types: ["webpack-env", "jest"],
      allowJs: true,
      checkJs: false,
      baseUrl: "./",
      paths: { "app/*": ["src/main/webapp/app/*"] },
      importHelpers: true,
      esModuleInterop: true,
      allowSyntheticDefaultImports: true
    },
    include: ["src/main/webapp/app", "src/test/javascript/spec"],
    exclude: ["node_modules"]
  },
  plugins: ["react", "@typescript-eslint"],
  rules: {
    'no-undef': 0,
    'react/prop-types': 0,
    'react/jsx-no-undef': 0,
    "no-unused-vars": 0,
    "no-prototype-builtins": 0,
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
    'import / prefer-default-export': 0,
    "react/no-render-return-value": 0,
    "react/display-name": 0
  }
};
