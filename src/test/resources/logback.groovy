//
// Built on Sat Feb 13 18:49:38 CET 2021 by logback-translator
// For more information on configuration files in Groovy
// please see http://logback.qos.ch/manual/groovy.html

// For assistance related to this tool or configuration files
// in general, please contact the logback user mailing list at
//    http://qos.ch/mailman/listinfo/logback-user

// For professional support please see
//   http://www.qos.ch/shop/products/professionalSupport

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import org.springframework.boot.logging.logback.ColorConverter
import org.springframework.boot.logging.logback.ExtendedWhitespaceThrowableProxyConverter
import org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter

import static ch.qos.logback.classic.Level.*

conversionRule("clr", ColorConverter)
conversionRule("wex", WhitespaceThrowableProxyConverter)
conversionRule("wEx", ExtendedWhitespaceThrowableProxyConverter)

def PID = System.getProperty("PID") ?: ''
def CONSOLE_LOG_PATTERN =
        "%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} " +
        "%clr(%5p) " +
        "%clr(${PID}){magenta} " +
        "%clr(---){faint} %clr([%15.15t]){faint} " +
        "%clr(%-40.40logger{39}){cyan} " +
        "%clr(:){faint} %m%n%wEx"

scan()
appender("STANDARD_OUT", ConsoleAppender) {
    withJansi = true
    encoder(PatternLayoutEncoder) {
        pattern = "${CONSOLE_LOG_PATTERN}"
    }
}

root(WARN, ["STANDARD_OUT"])
logger("javax.activation", WARN)
logger("javax.mail", WARN)
logger("javax.management.remote", WARN)
logger("javax.xml.bind", WARN)
logger("ch.qos.logback", WARN)
logger("com.hazelcast", INFO)
logger("com.ryantenney", WARN)
logger("com.sun", WARN)
logger("com.zaxxer", WARN)
logger("io.undertow", WARN)
logger("io.undertow.websockets.jsr", ERROR)
logger("org.apache", WARN)
logger("org.apache.kafka", INFO)
logger("org.apache.catalina.startup.DigesterFactory", OFF)
logger("org.bson", WARN)
logger("org.hibernate.validator", WARN)
logger("org.hibernate", WARN)
logger("org.hibernate.ejb.HibernatePersistence", OFF)
logger("org.postgresql", WARN)
logger("org.springframework", ERROR)
logger("org.springframework.web", ERROR)
logger("org.springframework.security", ERROR)
logger("org.springframework.cache", ERROR)
logger("org.thymeleaf", WARN)
logger("org.xnio", WARN)
logger("springfox", WARN)
logger("sun.rmi", WARN)
logger("liquibase", INFO)
logger("org.liquibase.logging.Logger", INFO)
logger("org.liquibase", INFO)
logger("LiquibaseSchemaResolver", INFO)
logger("springfox.documentation.schema.property", ERROR)
logger("sun.net.www", INFO)
logger("sun.rmi.transport", WARN)