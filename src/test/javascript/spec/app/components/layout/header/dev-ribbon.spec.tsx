import React from 'react';
import {shallow} from 'enzyme';
import DevRibbon from "app/components/layout/header/dev-ribbon";

describe('DevRibbon test', () => {
    let mountedWrapper;

    const devProps = {
        ribbonEnv: 'dev',
        isInProduction: false,
    };
    const prodProps = {
        ribbonEnv: 'prod',
        isInProduction: true
    };

    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<DevRibbon {...props} />);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    it('Renders a devRibbon component in dev profile with div and a tag', () => {
        const component = wrapper();
        expect(component).toMatchSnapshot();
        expect(component.find('.ribbon').length).toEqual(1);
    });

    it('Renders a devRibbon component in prod profile with no content', () => {
        const component = wrapper(prodProps);
        expect(component).toMatchSnapshot();
        expect(component.find('.ribbon').length).toEqual(0);
    });

});
