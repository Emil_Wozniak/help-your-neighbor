import React from 'react';
import {shallow} from 'enzyme';
import AppHeader from 'app/components/layout/header/app-header';
import LoadingBar from 'react-redux-loading-bar';
import {UserEntity} from "app/entities/user.entity";
import DevRibbon from "app/components/layout/header/dev-ribbon";
import {Header} from 'antd/lib/layout/layout';
import {Menu} from 'antd';
import MenuItem from 'antd/lib/menu/MenuItem';
import {Link} from 'react-router-dom';
import CustomMenuItem from "app/components/layout/header/custom-menu-item";

describe('Header', () => {
    let mountedWrapper;

    const devProps = {
        user: {} as UserEntity,
        ribbonEnv: 'dev',
        isInProduction: false,
    };
    const prodProps = {
        ...devProps,
        ribbonEnv: 'prod',
    };

    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<AppHeader {...props} />);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a Header component in dev profile with LoadingBar, Menu, MenuItem and dev ribbon.', () => {
        const component = wrapper();
        expect(component).toMatchSnapshot();

        const header = component.find(Header);
        expect(header.length).toEqual(1);
        expect(header.find(LoadingBar).length).toEqual(1);
        expect(header.find(DevRibbon).length).toEqual(1);

        const menu = component.find(Menu);
        expect(menu.length).toEqual(1);
        expect(menu.find(MenuItem).length).toEqual(1);
        expect(menu.find(CustomMenuItem).length).toEqual(1)

        const item = component.find(MenuItem)
        expect(item.find(Link).length).toEqual(1)

        const customItem = component.find(CustomMenuItem)
        expect(customItem.find(Link).length).toEqual(1)

        const ribbon = component.find(DevRibbon);
        expect(ribbon.length).toEqual(1);
    });

});
