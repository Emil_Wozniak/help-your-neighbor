import React from 'react';
import {shallow} from 'enzyme';
import AppHeader from 'app/components/layout/header/app-header';
import LoadingBar from 'react-redux-loading-bar';
import {UserEntity} from "app/entities/user.entity";
import DevRibbon from "app/components/layout/header/dev-ribbon";
import {Footer, Header} from 'antd/lib/layout/layout';
import {Menu} from 'antd';
import MenuItem from 'antd/lib/menu/MenuItem';
import {Link} from 'react-router-dom';
import CustomMenuItem from "app/components/layout/header/custom-menu-item";
import AppFooter from "app/components/layout/footer/app-footer";

describe('Footer', () => {
    let mountedWrapper;

    const wrapper = () => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<AppFooter />);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a AppFooter component with text.', () => {
        const component = wrapper();
        expect(component).toMatchSnapshot();

        const footer = component.find(Footer);
        expect(footer.text().includes("This is your footer"));

    });

});
