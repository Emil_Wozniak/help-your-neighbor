import {FAILURE, REQUEST, SUCCESS} from 'app/components/reducers/action-type.util';
import thunk from 'redux-thunk';
import axios from 'axios';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import promiseMiddleware from 'redux-promise-middleware';

import authentication, {ACTION_TYPES, clearAuthentication} from 'app/components/reducers/authentication';

describe('Authentication reducer tests', () => {
    function isAccountEmpty(state): boolean {
        return Object.keys(state.account).length === 0;
    }

    describe('Common tests', () => {
        it('should return the initial state', () => {
            const toTest = authentication(undefined, {});
            expect(toTest).toMatchObject({
                loading: false,
                isAuthenticated: false,
                errorMessage: null, // Errors returned from server side
                loginSuccess: false,
                loginError: false, // Errors returned from server side
                showModalLogin: false,
                redirectMessage: null
            });
            expect(isAccountEmpty(toTest));
        });
    });

    describe('Requests', () => {
        it('should detect a request', () => {
            expect(authentication(undefined, {type: REQUEST(ACTION_TYPES.GET_USER)})).toMatchObject({
                loading: true
            });
            expect(authentication(undefined, {type: REQUEST(ACTION_TYPES.LOGIN_OAUTH)})).toMatchObject({
                loading: true
            });
            expect(authentication(undefined, {type: REQUEST(ACTION_TYPES.LOGOUT_OAUTH)})).toMatchObject({
                loading: true
            });
            expect(authentication(undefined, {type: REQUEST(ACTION_TYPES.UPDATE_USER)})).toMatchObject({
                loading: true
            });
        });
    });

    describe('Success', () => {

        it('should detect a success on get session and be authenticated', () => {
            const payload = {data: {activated: true}};
            const toTest = authentication(undefined, {type: SUCCESS(ACTION_TYPES.GET_USER), payload});
            expect(toTest).toMatchObject({
                loading: false,
                sessionHasBeenFetched: true,
                account: payload.data
            });
        });

        it('should detect a success on register OAuth2User and be authenticated', () => {
            const payload = {data: {activated: true}};
            const toTest = authentication(undefined, {type: SUCCESS(ACTION_TYPES.REGISTER_OAUTH), payload});
            expect(toTest).toMatchObject({
                loading: false,
                loginError: false,
                loginSuccess: true,
                isAuthenticated: true,
                idToken: undefined,
                account: undefined
            });
        });

        it('should detect a success on login OAuth2User and be authenticated', () => {
            const payload = {data: {activated: true}};
            const toTest = authentication(undefined, {type: SUCCESS(ACTION_TYPES.LOGIN_OAUTH), payload});
            expect(toTest).toMatchObject({
                idToken: undefined,
                loading: false,
                loginError: false,
                loginSuccess: true,
                isAuthenticated: true
            });
        });

        it('should detect a success on logout OAuth2User and not be authenticated', () => {
            const payload = {data: {activated: true}};
            const toTest = authentication(undefined, {type: SUCCESS(ACTION_TYPES.LOGOUT_OAUTH), payload});
            expect(toTest).toMatchObject({
                isAuthenticated: true
            });
        });
    });

    describe('Failure', () => {

        it('should detect a failure', () => {
            const payload = 'Something happened.';
            const toTest = authentication(undefined, {type: FAILURE(ACTION_TYPES.GET_USER), payload});
            expect(toTest).toMatchObject({
                loading: false,
                isAuthenticated: false,
                showModalLogin: true,
                errorMessage: payload
            });
            expect(isAccountEmpty(toTest));
        });

        it('should detect a failure on register', () => {
            const payload = 'Registration failed';
            const toTest = authentication(undefined, {type: FAILURE(ACTION_TYPES.REGISTER_OAUTH), payload});
            expect(toTest).toMatchObject({
                loading: false,
                loginError: true,
                loginSuccess: false,
                errorMessage: payload
            });
            expect(isAccountEmpty(toTest));
        });

        it('should detect a failure on login', () => {
            const payload = 'Login failed';
            const toTest = authentication(undefined, {type: FAILURE(ACTION_TYPES.LOGIN_OAUTH), payload});
            expect(toTest).toMatchObject({
                loading: false,
                loginError: true,
                loginSuccess: false,
                errorMessage: payload
            });
            expect(isAccountEmpty(toTest));
        });

        it('should detect a failure on logout', () => {
            const payload = "can't logout";
            const toTest = authentication(undefined, {type: FAILURE(ACTION_TYPES.LOGOUT_OAUTH), payload});
            expect(toTest).toMatchObject({
                loading: false,
                loginError: true,
                loginSuccess: false,
                errorMessage: payload
            });
            expect(isAccountEmpty(toTest));
        });

        it('should detect a failure on update OAuth User', () => {
            const payload = "can't update user";
            const toTest = authentication(undefined, {type: FAILURE(ACTION_TYPES.UPDATE_USER), payload});
            expect(toTest).toMatchObject({
                errorMessage: payload
            });
            expect(isAccountEmpty(toTest));
        });

    });

    describe('Other cases', () => {


        it('should properly reset the current state when a logout is requested', () => {
            const toTest = authentication(undefined, {type: ACTION_TYPES.LOGOUT});
            expect(toTest).toMatchObject({
                loading: false,
                isAuthenticated: false,
                loginSuccess: false,
                loginError: false,
                showModalLogin: true,
                errorMessage: null,
                redirectMessage: null
            });
            expect(isAccountEmpty(toTest));
        });

        it('should properly define an error message and change the current state to display the login modal', () => {
            const message = 'redirect me please';
            const toTest = authentication(undefined, {type: ACTION_TYPES.ERROR_MESSAGE, message});
            expect(toTest).toMatchObject({
                loading: false,
                isAuthenticated: false,
                loginSuccess: false,
                loginError: false,
                showModalLogin: true,
                errorMessage: null,
                redirectMessage: message
            });
            expect(isAccountEmpty(toTest));
        });

        it('should clear authentication', () => {
            const message = 'redirect me please';
            const toTest = authentication(undefined, {type: ACTION_TYPES.CLEAR_AUTH, message});
            expect(toTest).toMatchObject({
                loading: false,
                showModalLogin: true,
                isAuthenticated: false
            });
        });
    });

    describe('Actions', () => {
        let store;
        const resolvedObject = {value: 'whatever'};
        beforeEach(() => {
            const mockStore = configureStore([thunk, promiseMiddleware]);
            store = mockStore({authentication: {account: {langKey: 'pl'}}});
            axios.get = sinon.stub().returns(Promise.resolve(resolvedObject));
        });

        it('dispatches CLEAR_AUTH actions', async () => {
            const expectedActions = [
                {
                    message: 'message',
                    type: ACTION_TYPES.ERROR_MESSAGE
                },
                {
                    type: ACTION_TYPES.CLEAR_AUTH
                }
            ];
            await store.dispatch(clearAuthentication('message'));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
