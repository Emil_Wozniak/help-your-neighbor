import React from 'react';
import App from 'app/App';
import {render} from "enzyme";

test('renders learn react link', () => {
    const component = render(<App/>);
    expect(component).toBe(App);
});
