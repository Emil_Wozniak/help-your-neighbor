package pl.helpyourneighbor.model.user

import spock.lang.Shared
import spock.lang.Specification

import java.util.stream.IntStream

import static java.lang.Boolean.FALSE
import static java.lang.Boolean.TRUE
import static java.util.stream.Collectors.toList
import static pl.helpyourneighbor.config.constants.UserConstants.DEFAULT_ID
import static pl.helpyourneighbor.config.constants.UserConstants.DEFAULT_PHONE
import static pl.helpyourneighbor.model.provider.AuthProvider.GOOGLE

class UserSpec extends Specification {
    private User user1
    private User user2
    private User user3
    @Shared
    private Set<Authority> authorities
    @Shared
    private String email
    @Shared
    private String phone
    @Shared
    private Short announcementsAmount

    def setup() {
        authorities = Set.of(new Authority("ROLE_USER"))
        phone = "000000000"
        email = "test@hyn.pl"
        announcementsAmount = new Short("5")
        user1 = new User(
                DEFAULT_ID, email, DEFAULT_PHONE, TRUE, FALSE, "pl", GOOGLE, properties,
                announcementsAmount, authorities, null, null)
    }

    void cleanup() {
        user1 = null
        authorities = null
        phone = null
        email = null
        announcementsAmount = null
    }

    def "should not return true while in equals between object and null"() {
        expect:
        user1 != null
    }

    def "User equals should be symmetric"() {
        given:
        user2 = new User(DEFAULT_ID, email, DEFAULT_PHONE, TRUE, FALSE, "pl", GOOGLE, properties,
                announcementsAmount, authorities, null, null)
        expect:
        user1 == user2
    }

    def "id should not be important in equals method"() {
        given:
        user2 = new User(1, email, DEFAULT_PHONE, TRUE, FALSE, "pl", GOOGLE, properties,
                announcementsAmount, authorities, null, null)
        expect:
        user1 == user2
    }

    def "announcements_amount should not be important in equals method"() {
        given:
        user2 = new User(1, email, DEFAULT_PHONE, TRUE, FALSE, "pl", GOOGLE, properties,
                new Short("1"), authorities, null, null)
        expect:
        user1 == user2
    }

    def "announcements should not be important in equals method"() {
        given:
        user2 = new User(1, email, DEFAULT_PHONE, TRUE, FALSE, "pl", GOOGLE, properties,
                new Short("1"), authorities, List.of(), null)
        expect:
        user1 == user2
    }

    def "details should not be important in equals method"() {
        given:
        user2 = new User(1, email, DEFAULT_PHONE, TRUE, FALSE, "pl", GOOGLE, properties,
                new Short("1"), authorities, List.of(), new UserDetails())
        expect:
        user1 == user2

    }

    def "user equals method should be transitive"() {
        given:
        user2 = user1
        user3 = user1

        expect:
        user1 == user2 && user2 == user3 && user1 == user3
    }

    def "User equals method should be consistent"(User user) {
        given:
        user2 = user1

        expect:
        user2 == user1

        where:
        user << IntStream
                .range(0, 10)
                .mapToObj {
                    new User(
                            DEFAULT_ID, email, DEFAULT_PHONE,
                            TRUE, FALSE, "pl", GOOGLE, properties,
                            new Short("5"), authorities,
                            null, null)
                }
                .collect(toList())
    }
}
