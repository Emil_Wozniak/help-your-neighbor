package pl.helpyourneighbor

import io.netty.channel.embedded.EmbeddedChannel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.core.env.Environment
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

import java.util.stream.Collectors

import static java.util.stream.Collectors.*
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
class HelpYourNeighborApplicationSpec extends Specification {

    @Autowired
    private Environment environment

    @Autowired
    private ApplicationContext context

    EmbeddedChannel channel

    @SuppressWarnings('unused')
    void setup() {
        channel = new EmbeddedChannel()
    }

    def "application should start up properly"() {
        expect:
        environment != null
    }

    def "active profile is test"() {
        given:
        def profiles = environment.activeProfiles
        expect:
        profiles.contains("test")
    }

    def "test context should not have enabled liquibase"() {
        given:
        def beans = context.getBeanDefinitionNames()
        def liquibaseBeans = beans.toList().stream().filter{ it.contains("liquibase")}.collect(toList())
        expect:
        liquibaseBeans.size() == 0
    }
}
