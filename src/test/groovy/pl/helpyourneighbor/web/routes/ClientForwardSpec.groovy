package pl.helpyourneighbor.web.routes

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mock.web.MockHttpSession
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.web.FilterChainProxy
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import static pl.helpyourneighbor.OAuthUtils.createOAuth2User

@SpringBootTest
@RunWith(SpringRunner.class)
class ClientForwardSpec extends Specification {

    @Autowired
    private FilterChainProxy springSecurityFilterChain
    @Autowired
    private WebApplicationContext wac
    private MockMvc mvc
    private MockHttpSession session
    private OAuth2AuthenticationToken principal

    def setup() {
        principal = createOAuth2User()
        this.session = new MockHttpSession()
        this.session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, new SecurityContextImpl(principal))
        this.mvc = webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build()
    }

    def "persist / with principals"() {
        when:
        def result = this.mvc.perform(get("/").session(session)).andReturn().response

        then: "status is OK"
        result.status == 200

        and: "header contains 1 element"
        result.headerNames.size() == 12
    }

    def "persist / without principals"() {
        when:
        def result = this.mvc.perform(get("/")).andReturn().response

        then: "status is OK"
        result.status == 200

        and: "header contains 1 element"
        result.headerNames.size() == 12
    }
}
