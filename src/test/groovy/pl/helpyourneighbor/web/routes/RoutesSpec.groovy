package pl.helpyourneighbor.web.routes

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mock.web.MockHttpSession
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.web.FilterChainProxy
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.web.context.WebApplicationContext
import pl.helpyourneighbor.config.Fibonacci
import spock.lang.Specification

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import static pl.helpyourneighbor.OAuthUtils.createOAuth2User

@SpringBootTest
@RunWith(SpringRunner.class)
class RoutesSpec extends Specification {

    @Autowired
    private FilterChainProxy springSecurityFilterChain
    @Autowired
    private WebApplicationContext wac
    private MockMvc mvc
    private OAuth2AuthenticationToken principal
    private MockHttpSession session

    def setup() {
        principal = createOAuth2User()
        this.session = new MockHttpSession()
        this.session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, new SecurityContextImpl(principal))
        this.mvc = webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build()
    }

    def "should contains GET path #route without principals returns status code 200"(String route) {
        when:
        def result = getResponse(get(route))

        then:
        result.status == 200

        where:
        route                    | _
        "/"                      | _
        "/route/management/info" | _
    }

    def "should contains GET path /route#route without principals returns status code #status"(String route, int status) {
        when:
        def result = getResponse(get("/route${route}"))

        then:
        result.status == status

        where:
        route              | status
        "/management/info" | 200
        "/register"        | 200
        "/account"         | 204
    }

    def "should contains GET secured path /route#route without principals returns status code 401"(String route) {
        when:
        def result = getResponse(get("/route${route}"))

        then: "status is Unauthorized"
        result.status == 401

        where:
        route              | _
        "/user"            | _
        "/user/1"          | _
        "/user/criteria"   | _
        "/announcements"   | _
        "/announcements/1" | _
        "/hashtags"        | _
        "/hashtags/1"      | _
        "/marker-images"   | _
    }

    def "should contains POST secured path /route#route without principals returns status code 403"(String route) {
        when:
        def result = getResponse(post("/route${route}"))

        then: "status is Forbidden"
        result.status == 403

        where:
        route            | _
        "/user"          | _
        "/user/criteria" | _
        "/announcements" | _
        "/hashtags"      | _
    }

    def "should contains DELETE secured path /route#route without principals returns status code 403"(String route) {
        when:
        def result = getResponse(delete("/route${route}"))

        then: "status is Forbidden"
        result.status == 403

        where:
        route              | _
        "/user/1"          | _
        "/announcements/1" | _
        "/hashtags/1"      | _
    }

    def "should contains GET to secured path /route#route, with principals returns status code 200 and content type application/json"(String route) {
        when:
        def result = getResponse(get("/route${route}").session(session))

        then: "status is OK 200"
        result.status == 200

        and: "content type is an application/json"
        result.contentType == "application/json"

        and: "result is not empty or black content"
        !result.contentAsString.isBlank()

        where:
        route            | _
        "/announcements" | _
        "/hashtags"      | _
        "/marker-images" | _
    }

    def "should contains GET secured path /route/hashtags/#id, while principals returns status code 204"(String id) {
        when:
        def result = getResponse(get("/route/hashtags/${id}").session(session))

        then: "status is NO CONTENT"
        result.status == 204

        where:
        id << Fibonacci.sequence(1, 20)
    }

    private def getResponse(MockHttpServletRequestBuilder request) {
        this.mvc.perform(request).andReturn().response
    }
}
