package pl.helpyourneighbor.web.socket

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.rsocket.context.LocalRSocketServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import pl.helpyourneighbor.model.announcement.Hashtag
import pl.helpyourneighbor.model.search.AnnouncementMessageRequest
import pl.helpyourneighbor.model.search.AnnouncementMessageResponse
import pl.helpyourneighbor.model.search.MapCoordinates
import reactor.core.publisher.Flux
import reactor.test.StepVerifier
import spock.lang.Specification

import static java.time.LocalDate.now

@SpringBootTest
class AnnouncementSocketSpec extends Specification {

    private static RSocketRequester requester
    private AnnouncementMessageRequest request
    private Flux<AnnouncementMessageResponse> result

    @Autowired
    RSocketRequester.Builder builder
//    @Value("${spring.rsocket.server.port}") Integer port
    @LocalRSocketServerPort
    Integer port
    @Autowired
    RSocketStrategies strategies

    @SuppressWarnings('unused')
    def setup() {
        request = new AnnouncementMessageRequest(
                "Warszawa",
                false,
                now(),
                new Hashtag(1L, "#malowanie", List.of()),
                now(),
                now(),
                new MapCoordinates(
                        52.21111,
                        52.21111,
                        21.21111,
                        21.21111,
                        10
                ))

        requester = builder
                .connectTcp("localhost", port)
                .block()

    }

    @SuppressWarnings('unused')
    def cleanup() {
        request = null
        requester.rsocket().dispose();
    }

    def "FindAll"() {
        given:
        result = requester
                .route("announcements")
                .data(request)
                .retrieveFlux(AnnouncementMessageResponse.class)

        expect:
        result != null

        and:
        StepVerifier
                .create(result)
                .expectSubscription()
                .any {
                    it.consumeNextWith(
                            o ->
                                    o.getAnnouncement() == null
                    )
                }.equals(true)
    }
}
