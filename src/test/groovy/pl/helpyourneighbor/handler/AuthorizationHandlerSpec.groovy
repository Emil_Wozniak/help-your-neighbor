package pl.helpyourneighbor.handler

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.mock.web.MockHttpSession
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.web.FilterChainProxy
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext
import pl.helpyourneighbor.HelpYourNeighborApplication
import spock.lang.Specification

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import static pl.helpyourneighbor.OAuthUtils.createOidUser
import static pl.helpyourneighbor.config.AppConstant.JSON

@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelpYourNeighborApplication.class)
class AuthorizationHandlerSpec extends Specification {

    @Autowired
    private WebApplicationContext wac
    @Autowired
    private FilterChainProxy chain
    @Autowired
    private Environment env
    private MockMvc mvc
    private ObjectMapper mapper
    private MockHttpSession session
    private OAuth2AuthenticationToken principal
    private MockHttpServletResponse response

    void setup() {
        this.mvc = webAppContextSetup(this.wac).addFilter(chain).build()
        principal = createOidUser()
        session = new MockHttpSession()
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, new SecurityContextImpl(principal))
        mapper = new ObjectMapper()
        response = mvc
                .perform(get("/route/register").session(session))
                .andReturn().response
        println("Session props:")
        println(session)
        println(response.properties)
    }

    void cleanup() {
        wac = null
        chain = null
        mvc = null
        principal = null
        session = null
        mapper = null
        response = null
    }

    def "register OAuth2 User should return status 200"() throws Exception {
        expect:
        response.status == 200
    }

    def "register OAuth2 User should return non empty content"() throws Exception {
        expect:
        response.contentAsString != ""
    }

    def "register OAuth2 User should return content type application/json"() throws Exception {
        expect:
        response.contentType == JSON
    }
}
