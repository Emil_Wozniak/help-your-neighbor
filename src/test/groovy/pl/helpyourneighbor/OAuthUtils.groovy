package pl.helpyourneighbor

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.oauth2.core.oidc.OidcIdToken
import org.springframework.security.oauth2.core.oidc.OidcUserInfo
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser
import org.springframework.security.oauth2.core.user.DefaultOAuth2User
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority
import pl.helpyourneighbor.model.user.Authority

import static java.time.Instant.now
import static java.util.Collections.singletonList
import static pl.helpyourneighbor.model.provider.AuthProvider.GOOGLE
import static pl.helpyourneighbor.model.provider.AuthProvider.GOOGLE_ISS

class OAuthUtils {

    static String USER = "ROLE_USER"

    static def attributes = [
            "sub"        : "my-id",
            "email"      : "jsasiad25@gmail.com",
            "name"       : "Pan Janusz",
            "authorities": new Authority(USER),
            "imageUrl"   : null,
            "iss"        : GOOGLE_ISS,
            "locale"     : "pl",
            "provider"   : GOOGLE.getName()
    ] as Map<String, Object>

    static def createOAuth2User() {
        def principals = new OAuth2UserAuthority(USER, attributes)
        def authorities = singletonList(principals) as List<GrantedAuthority>
        OAuth2User user = new DefaultOAuth2User(authorities, attributes, "sub")
        return new OAuth2AuthenticationToken(user, authorities, "whatever")
    }

    static def createOidUser() {
        def principals = new OAuth2UserAuthority(USER, attributes)
        def authorities = singletonList(principals) as List<GrantedAuthority>
        def idToken = new OidcIdToken("test", now(), now() + 90, attributes)
        def userInfo = new OidcUserInfo(attributes)
        def user = new DefaultOidcUser(authorities, idToken, userInfo)
        return new OAuth2AuthenticationToken(user, authorities, "whatever")
    }
}
