package pl.helpyourneighbor.config


import java.util.stream.IntStream

import static java.lang.Math.*

class Fibonacci {
    static int fib(int n) {
        double phi = (1 + sqrt(5)) / 2;
        return (int) round(pow(phi, n) / sqrt(5));
    }

    static List<Integer> sequence(int from, int to) {
        return IntStream
                .range(from, to)
                .map { fib(it) }
                .toArray()

    }
}
