package pl.helpyourneighbor.util.types

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import spock.lang.Specification

import static org.springframework.web.servlet.function.RequestPredicates.DELETE
import static org.springframework.web.servlet.function.RequestPredicates.GET
import static org.springframework.web.servlet.function.RouterFunctions.route
import static org.springframework.web.servlet.function.ServerResponse.ok
import static pl.helpyourneighbor.util.types.CustomPredicateRoute.POST
import static pl.helpyourneighbor.util.types.CustomPredicateRoute.nestPath

class CustomPredicateRouteSpec extends Specification {

    def "NestPath should takes array of routes"() {
        when: "routes has 4 elements"
        def response = nestPath("tested",
                route(GET(""), this::handle),
                route(GET("/{id}"), this::handle),
                route(POST(""), this::handle),
                route(DELETE("/{id}"), this::handle))

        then: "contains pathName"
        response.toString().contains("tested")

        and: "contains all 4 routes"
        def props = response.toString()
        props.contains("(GET && )")
        props.contains("(GET && /{id})")
        props.contains("((POST && ) && Accept: application/json)")
        props.contains("(DELETE && /{id})")
    }

    private static ServerResponse handle(ServerRequest _) {
        return ok().body("ok")
    }
}
