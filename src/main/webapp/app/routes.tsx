import React from "react";
import {Switch} from "react-router-dom";
import Home from "app/modules/home/home";
import ErrorBoundaryRoute from "app/components/error/error-boundary-route";
import PageNotFound from "app/components/error/page-not-found";
import ErrorBoundary from "app/components/error/error-boundry";
import {AUTHORITIES} from "app/config/constants";
import {Col, Layout, Row} from "antd";
import Profile from "app/modules/profile/profile";
import Authenticate from "app/modules/login/authenticate";
import Remove from "app/modules/remove/remove";
import Logout from "./modules/login/logout";
import PrivateRoute from "app/components/auth/private-route";

const {Content} = Layout;

const Routes = () => {
    const user = [AUTHORITIES.USER];
    const admin = [AUTHORITIES.ADMIN];
    const authorities = [AUTHORITIES.ADMIN, AUTHORITIES.USER];
    return (
        <ErrorBoundary>
            <Content className="content">
                <Row style={{marginTop: "2rem"}}>
                    <Col span={2}/>
                    <Col span={20}>
                        <Switch>
                            <ErrorBoundaryRoute path="/" exact component={Home}/>
                            <ErrorBoundaryRoute path="/config.auth" component={Authenticate}/>
                            <ErrorBoundaryRoute path="/remove" component={Remove}/>
                            <ErrorBoundaryRoute path="/logout" component={Logout}/>
                            <PrivateRoute path="/profile" component={Profile} hasAuthorities={user}/>
                            <ErrorBoundaryRoute component={PageNotFound}/>
                        </Switch>
                    </Col>
                    <Col span={2}/>
                </Row>
            </Content>
        </ErrorBoundary>
    );
};

export default Routes;
