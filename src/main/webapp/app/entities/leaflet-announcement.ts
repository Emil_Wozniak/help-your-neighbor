import {LatLngLiteral} from 'leaflet';

export type ILeafletAnnouncement = {
    position: LatLngLiteral;
    message: string;
    type: number;
    contact: {email: string, phone?: string};
    isPaid: boolean;
}