export interface IHashtagModel {
    id?: number;
    name?: string
}

export const defaultValue = (): IHashtagModel => ({})