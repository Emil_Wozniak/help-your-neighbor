export interface ILocation {
    id?: number;
    latitude?: number;
    longitude?: number;
    city?: string;
    address?: string;
    announcements?: any
}

export const createLocation = (values: ILocation): ILocation =>
    ({id: null,...values, announcements: null })

export const defaultValue = (): ILocation => ({})