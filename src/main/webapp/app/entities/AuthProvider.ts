export enum AuthProvider {
    GOOGLE = "GOOGLE",
    LOCAL = "LOCAL",
    DEFAULT = ""
}

export const getProvider = (value) => Object
    .keys(AuthProvider)
    .filter(key => key === value)
    .map(key => ({name: key}))[0];