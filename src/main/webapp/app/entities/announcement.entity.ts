import {ILocation} from "app/entities/location.entity";
import {IHashtagModel} from "app/entities/hashtag.entity";
import {UserEntity} from "app/entities/user.entity";

export interface IAnnouncementModel {
    id?: number;
    content?: string;
    dateOfPublication?: Date | string;
    dateEndOfPublication?: Date | string;
    dateSaveAnnouncement?: Date | string;
    isPaid?: boolean;
    isDone?: boolean;
    isPro?: boolean;
    image?: number;
    locations?: ReadonlyArray<ILocation>;
    user?: UserEntity;
    hashtags?: ReadonlyArray<IHashtagModel>;
}

export const createFromValues = (values: IAnnouncementModel, locations: Array<ILocation>) =>
    ({ id: null, ...values, locations })


export const defaultValue = (): IAnnouncementModel => ({})
