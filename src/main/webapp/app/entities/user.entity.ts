import {AuthProvider} from "app/entities/AuthProvider";

/**
 * Based on GoogleLoginResponse.
 * @see GoogleLoginResponse
 */
export interface UserEntity {
    name?: string;
    email?: string;
    phone?: string;
    password?: string;
    imageUrl?: string;
    activated?: boolean;
    banned?: boolean;
    provider?: AuthProvider;
    authorities?: ReadonlyArray<AuthorityEntity>
}

export const defaultUserEntity = {} as UserEntity

type Principals = {
    attributes: {
        activated: boolean;
        email: string;
        name: string
        phone: string
        picture: string
        provider: string
    }
    authorities: ReadonlyArray<AuthorityEntity>
}
export const userFromPrincipal = ({
                         attributes: {activated, email, name, picture, phone, provider},
                         authorities
                     }: Principals): UserEntity => ({
    name,
    email,
    phone,
    imageUrl: picture,
    activated,
    provider: provider === "google" ? AuthProvider.GOOGLE : AuthProvider.DEFAULT,
    authorities
})