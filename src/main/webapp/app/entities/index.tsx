import React from "react";
import { Switch } from "react-router-dom";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from "app/components/error/error-boundary-route";

type RoutesProps = {
  match: any;
};

const Routes = ({ match }: RoutesProps) => (
  <div>
    <Switch>{/* prettier-ignore */}</Switch>
  </div>
);

export default Routes;
