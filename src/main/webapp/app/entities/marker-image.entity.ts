export interface IMarkerImageModel {
    id?: number;
    type?: string;
    description?: string;
    name?: string
}

export const defaultValue = (): IMarkerImageModel => ({})