import React from 'react';
import GridContainer from "app/components/layout/grid/grid-container";

const Logout = () => (
    <GridContainer key="logout-menu">
        <h2>You have been successfully logout</h2>
    </GridContainer>
);

export default Logout;