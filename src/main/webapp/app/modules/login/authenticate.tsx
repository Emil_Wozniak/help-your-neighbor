import React from 'react';
import "../../styles/login.scss";
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {Col, Row} from 'antd';
import SignupForm from "app/components/register/signup-form";
import LoginContainer from "app/components/login/login-container";
import {getSession} from "app/components/reducers/authentication";
import {UserEntity} from "app/entities/user.entity";
import {IRootState} from 'app/components/reducers';
import AuthButtons from "app/components/authenticate/auth-buttons";
import Logo from "app/components/layout/icons/logo";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {GOOGLE_HREF} from "app/config/constant/main.constant";

interface LoginSocialProps extends StateProps, DispatchProps {
}

interface LoginSocialState {
    user?: UserEntity
    register: boolean;
}

class Authenticate extends React.Component<LoginSocialProps, LoginSocialState> {
    constructor(props: LoginSocialProps) {
        super(props);
        this.state = {user: null, register: true}
    }

    componentDidMount() {
        this.props.getSession()
    }

    render() {
        const {register} = this.state;
        return this.props.isAuthenticated
            ? <Redirect to="/"/>
            : (
                <Row style={{margin: "2rem"}}>
                    <Col span={4}/>
                    <Col span={16}>
                        <div className="login-container">
                            <div className="login-content">
                                <AuthButtons register={register} change={this.change}/>
                                <div className="social">
                                    <Row justify="center">
                                        <Logo scale={0.4} className="center"/>
                                    </Row>
                                    <br/>
                                    {!register
                                        ? <LoginContainer authenticated={this.props.isAuthenticated}/>
                                        : <SignupForm/>
                                    }
                                    <p style={{fontSize: 12, color: "#555", textAlign: "center"}}>
                                        OR
                                    </p>
                                    <br/>
                                    <Row>
                                        <a href={GOOGLE_HREF} className="btn gp reverse">
                                            <FontAwesomeIcon color="#fff" icon={["fab", "google"]}/>
                                            {register ? " Sing up" : " Login"}
                                            <span> with Google</span>
                                        </a>
                                    </Row>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col span={4}/>
                </Row>
            );
    }

    change = (next: boolean) => (_) => this.setState({register: next})
}

const mapStateToProps = ({authentication}: IRootState) => ({
    user: authentication.account,
    error: authentication.error,
    loginError: authentication.loginError,
    isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = {getSession};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Authenticate);