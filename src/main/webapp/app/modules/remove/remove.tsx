import React from 'react';
import {Divider} from 'antd';

const Remove = () => (
    <div className="profile">
        <div className="container">
            <Divider orientation="left">
                <h1>Goodbye</h1>
            </Divider>
            <div style={{marginTop: "2rem"}}/>
            <h2>
                Your account have been removed successfully
            </h2>
        </div>
    </div>
);

export default Remove;