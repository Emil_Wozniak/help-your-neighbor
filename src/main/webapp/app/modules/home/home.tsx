import React, {useEffect} from 'react';
import {Col, Row} from 'antd';
import {HOME} from "app/components/styles/styles";
import {connect} from 'react-redux';
import {IRootState} from "app/components/reducers";
import {getSession} from "app/components/reducers/authentication";
import MapWrapper from "app/components/map/map-wrapper";
import {translate} from "app/config/i18n/Translate";

interface HomeProps extends StateProps, DispatchProps {
}

const Home = ({getSession, isAuthenticated}: HomeProps) => {
    useEffect(() => {
        (async () => {
            await getSession();
        })()
    }, [])

    return (
        <div style={isAuthenticated ? {height: '80%'} : HOME}>
            {!isAuthenticated
                ? (
                    <Row gutter={[48, 48]} className="welcome">
                        <Col span={12} className="title fade-in">
                            <h1>{translate("global.home.title")}</h1>
                            <h2><q>{translate("global.home.subtitle")}</q></h2>
                        </Col>
                        <Col span={12} className="welcome maps">
                            <div className="map-hidden">
                                <MapWrapper active={isAuthenticated}/>
                            </div>
                        </Col>
                    </Row>)
                : (
                    <div className="welcome maps">
                        <div className="map-visible">
                            <MapWrapper active={isAuthenticated}/>
                        </div>
                    </div>
                )}
        </div>
    );
}


const mapStateToProps = ({authentication}: IRootState) => ({
    isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = {
    getSession,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Home);