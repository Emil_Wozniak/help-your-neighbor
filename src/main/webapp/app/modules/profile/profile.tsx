import React from 'react';
import {defaultUserEntity, UserEntity} from "app/entities/user.entity";
import {Menu} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import ProfileItem from "app/components/profile/profile-item";
import {IRootState} from "app/components/reducers";
import {getUser, logoutOAuth2Google, removeAccount, updateUser} from "app/components/reducers/authentication";
import {getEntities as getAnnouncements} from "app/components/reducers/announcement.reducer";
import {getEntities as getHashtags} from "app/components/reducers/hashtag.reducer";
import {connect} from 'react-redux';
import ActivateProfile from "app/components/profile/activate-profile";
import {RouteComponentProps} from 'react-router-dom';
import AnnouncementTable from 'app/components/layout/grid/announcement-table/announcement-table';
import {translate} from "app/config/i18n/Translate";

const SubMenu = Menu.SubMenu;

const Title = () => (
    <>
        <FontAwesomeIcon
            icon="user-circle"
            style={{marginRight: 4}}/>
        <b>{translate("profile.title")}</b>
    </>
)

interface ProfileProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

interface ProfileState {
    user: UserEntity
}

class Profile extends React.Component<ProfileProps, ProfileState> {
    constructor(props) {
        super(props)
        this.state = {user: defaultUserEntity}
    }

    componentDidMount() {
        if (this.state.user === defaultUserEntity) {
            this.props.getSession()
            this.setState({user: this.props.user})
        }
        this.props.getAnnouncements()
    }

    shouldComponentUpdate(nextProps, nextState, _) {
        if (this.props.announcements != nextProps.announcements) {
            return true;
        }
        return this.state.user !== nextProps.user && nextProps.user !== undefined;
    }

    handleClick = (_) => {
    };

    render() {
        const {user} = this.props
        const {imageUrl, name, email, phone, activated} = user;
        return (
            <div className="profile">
                <div className="container">
                    <div className="user">
                        <div className="avatar">
                            {imageUrl && (<img src={imageUrl} alt={name}/>)}
                        </div>
                        <div>
                            <h3>{name}</h3>
                            <p>{email}</p>
                        </div>
                        {phone && phone != ""
                            ? null
                            : <ActivateProfile
                                history={this.props.history}
                                user={user}
                                update={this.props.updateUser}/>}
                        <div className="profile-menu">
                            <Menu
                                key="profile menu"
                                onClick={this.handleClick}
                                defaultSelectedKeys={activated ? ['1'] : []}
                                defaultOpenKeys={activated ? ['profile-settings'] : []}
                                mode="inline">
                                <SubMenu
                                    key="profile-settings"
                                    title={<Title/>}>
                                    <ProfileItem
                                        label={translate("profile.logout")}
                                        title="Logout"
                                        symbol="sign-out-alt"
                                        action={this.logout}
                                        to="http://localhost:8088/logout"/>
                                    <ProfileItem
                                        label={translate("profile.delete")}
                                        title="Delete account"
                                        symbol="search"
                                        action={this.delete}
                                        to="/remove"/>
                                </SubMenu>
                                <SubMenu
                                    key="Review"
                                    title={<div>
                                        <FontAwesomeIcon icon="search"/>
                                        <b>{translate("profile.announcements.title")}</b>
                                    </div>}>
                                    <AnnouncementTable
                                        announcements={this.props.announcements}
                                        symbol="scroll"
                                    />
                                </SubMenu>
                            </Menu>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    private logout = () => this.props.logoutOAuth2Google(this.props.history)

    private delete = () => this.props.removeAccount(this.props.user.email)

}


const mapStateToProps = ({authentication, announcement}: IRootState) => ({
    user: authentication.account,
    announcements: announcement.entities,
});

const mapDispatchToProps = {
    getSession: getUser,
    logoutOAuth2Google,
    removeAccount,
    updateUser,
    getAnnouncements,
    getHashtags
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Profile);