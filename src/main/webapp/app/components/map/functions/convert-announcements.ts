import {ILeafletAnnouncement} from "app/entities/leaflet-announcement";
import {IAnnouncementModel} from "app/entities/announcement.entity";

export const convertAnnouncements = (announcements: ReadonlyArray<IAnnouncementModel>) =>
    announcements
        .concat()
        .filter(it => it)
        .map((it): ILeafletAnnouncement => {
            const {locations, content, image, user: {email, phone}} = it
            const isPaid = it["paid"]
            return ({
                position: {lat: locations[0].latitude, lng: locations[0].longitude},
                message: content,
                type: image,
                contact: {email, phone},
                isPaid
            });
        })