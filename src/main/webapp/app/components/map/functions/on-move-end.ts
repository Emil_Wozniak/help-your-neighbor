import axios from "axios";
import {LeafletEvent} from "leaflet";

export type MoveCoordinates = {
    north: number;
    south: number;
    east: number;
    west: number;
    zoom: number;
}

export const onMoveEnd = (event: LeafletEvent) => {
    const map = event.target;
    const move: MoveCoordinates = {
        north: map.getBounds().getNorth(),
        south: map.getBounds().getSouth(),
        east: map.getBounds().getEast(),
        west: map.getBounds().getWest(),
        zoom: map.getZoom()
    }

    axios.post("/route/coordinates", move)
    // console.table(move)
};