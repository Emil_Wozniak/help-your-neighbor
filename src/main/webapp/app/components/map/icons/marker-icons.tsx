import {IMarkerImageModel} from "app/entities/marker-image.entity";
import {Icon, Point} from 'leaflet';

export type IconModel = {
    name: string
    icon: Icon;
}
//   x, y, round
export const iconPoint = new Point(36, 36, true)
export const popup = new Point(0, 0, true)
export const popupAnchor = new Point(-18, -20, true)
export const paidShadowAnchor = new Point(9, 9, true)
export const shadowSize = new Point(44, 44, true)
export const paidShadowSize = new Point(54, 54, true)

export const createIcons = (icons: ReadonlyArray<IMarkerImageModel>): Array<IconModel> =>
    icons
        .filter(it => it)               // only non null
        .map(({name}) => {
                const image = require(`../../../assets/img/emots/${name.toLowerCase()}.png`)
                const shadow = require("../../../assets/img/coin/shadow.png")
                return ({
                    name: name.toLowerCase(),
                    icon: new Icon({
                        iconUrl: image,
                        iconRetinaUrl: image,
                        iconSize: iconPoint,        // non null
                        iconAnchor: popup,          // non null
                        popupAnchor,                // non null
                        tooltipAnchor: popup,       // non null
                        shadowUrl: shadow,
                        shadowRetinaUrl: "",
                        shadowSize,
                        shadowAnchor: popup,        // non null
                        className: 'leaflet-div-icon'
                    })
                });
            }
        )

export const paidIcon = (icon: IconModel) => {
    const {options} = icon.icon
    const {iconUrl, iconRetinaUrl} = options
    const paid = require("../../../assets/img/coin/paid_shadow.png")
    return new Icon({
        iconUrl,
        iconRetinaUrl,
        iconSize: iconPoint,        // non null
        iconAnchor: popup,          // non null
        popupAnchor,                // non null
        tooltipAnchor: popup,       // non null
        shadowUrl: paid,
        shadowRetinaUrl: "",
        shadowSize: paidShadowSize,
        shadowAnchor: paidShadowAnchor,        // non null
        className: 'leaflet-div-icon'
    });
}