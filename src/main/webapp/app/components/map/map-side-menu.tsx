import React, {useState} from 'react';
import {Button, Layout, Menu} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import AddAnnouncement from "app/components/map/add-announcement";
import {LatLngLiteral} from 'leaflet';
import FetchAnnouncements from "app/components/map/fetch-announcements";
import {translate} from "app/config/i18n/Translate";


const {Sider, Header} = Layout;

interface MapSideMenuProps {
    location?: LatLngLiteral;
}

const MapSideMenu = (props: MapSideMenuProps) => {
    const [collapsed, setCollapsed] = useState(true)
    const toggle = () => setCollapsed(!collapsed);
    return (
        <Layout id="map-side-menu" className="leaflet-side-menu leaflet-control">
            <Sider trigger={null}
                   theme="light"
                   id="map-sider"
                   collapsible
                   collapsed={collapsed}>
                <Menu
                    collapsedWidth={0}
                    id="map-sider-map-menu"
                    theme="light"
                    defaultSelectedKeys={['1']}
                    mode="inline">
                    <AddAnnouncement
                        location={props.location}
                        symbol="bullhorn"
                        key="user-announcement"
                        aria-label="user-announcement"
                        title={collapsed ? "" : translate("map.menu.add")}
                    />
                    <FetchAnnouncements
                        label={translate("map.menu.fetch-all")}
                        title={translate("map.menu.fetch-all")}
                        symbol="search"
                        />
                </Menu>
            </Sider>
            <Layout id="map-side-menu-icon" className="custom-ant-layout">
                <Header className="site-layout-background">
                    <Button
                        className='trigger'
                        type="text"
                        onClick={toggle}
                        style={{color: "black"}}>{
                        collapsed
                            ? <FontAwesomeIcon icon="angle-right"/>
                            : <FontAwesomeIcon icon="angle-left"/>
                    }</Button>
                </Header>
            </Layout>
        </Layout>
    );
}

export default MapSideMenu