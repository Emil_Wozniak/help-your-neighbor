import React from 'react';
import MapSideMenu from "app/components/map/map-side-menu";
import LeafletMap from "app/components/map/leaflet-map";
import {ILeafletAnnouncement} from "app/entities/leaflet-announcement";

interface MapWrapperProps {
    active: boolean;
    // announcements?: ReadonlyArray<ILeafletAnnouncement>;
}

const MapWrapper = ({active}: MapWrapperProps) => (
    <div id="map-wrapper" aria-label="map-wrapper"
         className={active ? "leaflet-wrapper" : "leaflet-wrapper fade-in-late"}>
        <LeafletMap isLogin={active} auto redirect >
            {active && <MapSideMenu/>}
        </LeafletMap>
    </div>
)

export default MapWrapper;