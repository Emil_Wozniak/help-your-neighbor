import React, {useEffect, useState} from 'react';
import {Button, DatePicker, Form, Input, Menu, Select, Switch} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import ValidateInput from "app/components/layout/grid/validate-input";
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {createFromValues, IAnnouncementModel} from "app/entities/announcement.entity";
import {IPayload, IPayloadResult} from "app/components/reducers/redux-action-type";
import {createLocation, ILocation} from "app/entities/location.entity";
import {UserEntity} from "app/entities/user.entity";
import {LatLngLiteral} from 'leaflet';
import {IRootState} from "app/components/reducers";
import {createEntity as addEntity} from "app/components/reducers/announcement.reducer";
import {getEntities as getImages} from "app/components/reducers/marker-image.reducer";
import {connect} from 'react-redux';
import SearchLocation from "app/components/map/search-location";
import {Error} from 'app/config/notification-middleware';
import moment from 'moment';
import {missingSwitch} from "app/components/layout/grid/form-validators";
import {translate} from "app/config/i18n/Translate";

const {RangePicker} = DatePicker;

interface AddAnnouncementProps extends StateProps, DispatchProps {
    user: UserEntity
    addEntity: (entity: IAnnouncementModel) => IPayload<IAnnouncementModel> | IPayloadResult<IAnnouncementModel>;
    symbol: IconProp;
    style?: React.CSSProperties;
    className?: string;
    title: string;
    location?: LatLngLiteral;
}

const AddAnnouncement = ({addEntity, images, user, getImages, title, ...props}: AddAnnouncementProps) => {
    const [location, setLocation] = useState(null as ILocation)
    const [address, setAddress] = useState('');
    const [form] = Form.useForm();

    useEffect(() => {
        getImages()
    }, [])

    const icon = <FontAwesomeIcon color="black" icon={props.symbol} style={{marginRight: 4}}/>
    const onFinish = (values) => {
        const range = values.announcementDates
        if (location === null || location === "") {
            Error("Adres nie może być pusty")
        } else {
            const announcement = createFromValues(
                {
                    ...values,
                    user,
                    dateSaveAnnouncement: new Date().toISOString().substring(0, 10),
                    dateOfPublication: range[0].toDate().toISOString().substring(0, 10),
                    dateEndOfPublication: range[1].toDate().toISOString().substring(0, 10)
                },
                Array.of(location))
            form.resetFields();
            setAddress("")
            addEntity(announcement)
        }
    };
    const handleChangeLocation = (properties) => {
        const text = `${properties.ShortLabel}, ${properties.City}`
        setAddress(text);
    };

    const addLocation = (response) => {
        setAddress(response.text)
        handleChangeLocation(response.results[0].properties)
        // noinspection SpellCheckingInspection
        const {latlng: {lat, lng}, properties: {City}} = response.results[0]
        const obj = createLocation({
            latitude: parseFloat(lat),
            longitude: parseFloat(lng),
            city: City,
            address: response.text
        })
        setLocation(obj)
    }

    const rangeConfig = {
        rules: [{type: 'array' as const, required: true, message: 'Proszę określić daty!'}],
    };

    const now = moment(moment.now())

    return (
        <Menu.SubMenu
            icon={icon}
            title={<span style={{padding: "4px 15px"}}>{title}</span>}
            {...props} >
            <div id="add-announcement-form"
                 className="add-announcement-form"
                 role="menuitem"
                 style={{marginTop: 6}}>
                <Form
                    scrollToFirstError
                    style={{margin: '0 1rem 0 1rem', display: 'inline-block', width: "100%"}}
                    form={form}
                    onFinish={onFinish}
                    labelCol={{span: 4}}
                    wrapperCol={{span: 14}}
                    layout="horizontal">
                    <ValidateInput name="image" label={translate("map.form.type")} InitialValue={1}>
                        <Select loading autoFocus>
                            {images && images.length > 0 &&
                            images.map(({id, name, type}, i) => (
                                <Select.Option key={`profile-marker-image-${name}-${i}`} value={id}>
                                    {type}
                                </Select.Option>
                            ))}
                        </Select>
                    </ValidateInput>
                    <ValidateInput name="content" label={translate("map.form.content")}>
                        <Input.TextArea style={{maxHeight: 80}}/>
                    </ValidateInput>
                    <Form.Item
                        hasFeedback
                        style={{maxWidth: "calc(90% - 8px)"}}
                        wrapperCol={{span: 16}}
                        labelCol={{span: 8}}
                        name="announcementDates"
                        label={translate("map.form.duration")}
                        {...rangeConfig}>
                        <RangePicker
                            disabledDate={(date) => date.isBefore(now)}
                            defaultPickerValue={[now, now]}
                            defaultValue={[now, now]}
                            format="YYYY-MM-DD"/>
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{span: 16}}
                        labelCol={{span: 8}}
                        name="paid"
                        label={translate("map.form.paid")}
                        rules={missingSwitch("is missing")}
                        hasFeedback
                        style={{width: 'calc(90% - 8px)'}}
                    >
                        <Switch
                            autoFocus
                            defaultChecked={false}/>
                    </Form.Item>
                    <SearchLocation address={address} addLocation={addLocation}/>
                    <Form.Item>
                        <Button
                            type="dashed"
                            htmlType="submit"
                            style={{width: "100%", marginLeft: 92}}>
                            {translate("map.form.submit")}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </Menu.SubMenu>
    );
};

const mapStateToProps = ({authentication, announcement, markerImage}: IRootState) => ({
    user: authentication.account,
    announcements: announcement.entities,
    images: markerImage.entities
});

const mapDispatchToProps = {
    getImages,
    addEntity
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AddAnnouncement);