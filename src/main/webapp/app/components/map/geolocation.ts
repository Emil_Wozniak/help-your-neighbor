import {LatLngLiteral} from 'leaflet';
import {useState} from 'react';

export const getLocation = async (): Promise<LatLngLiteral> => {
    const [loc, setLoc] = useState({lat: 54.2297, lng: 24.0122} as LatLngLiteral)
    if (navigator.geolocation) {
       await navigator.geolocation.getCurrentPosition(
            ({coords}) => {
                setLoc({lng: coords.longitude, lat: coords.latitude});
            }, (error) => console.error("Error Code = " + error.code + " - " + error.message))
    }
    return loc;
}