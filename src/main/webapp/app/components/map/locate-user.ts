import L from 'leaflet';

// noinspection SpellCheckingInspection
export const locateUser = (map, search, setSearch) =>
    search
        ? map
            .locate({setView: true, watch: true})
            .on('locationfound', event => {
                const marker = L.marker([event.latitude, event.longitude])
                    .bindPopup('Your are here :)');
                map.addLayer(marker);
                setSearch(false)
            })
            .on('locationerror', error => {
                console.error(error.message);
                setSearch(false)
            })
        : map;