import React, {useEffect} from 'react';
import {LayersControl, Marker, Popup} from 'react-leaflet';
import {IconModel, iconPoint, paidIcon} from "app/components/map/icons/marker-icons";
import {convertAnnouncements} from "app/components/map/functions/convert-announcements";
import {IAnnouncementModel} from "app/entities/announcement.entity";
import {useStateBasedOnProps} from "app/config/use-state-based-on-props";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {translate} from "app/config/i18n/Translate";

type AnnouncementMarkersProps = {
    announcements: ReadonlyArray<IAnnouncementModel>;
    icons: Array<IconModel>
}

const {Overlay} = LayersControl

const AnnouncementMarkers = ({announcements, icons}: AnnouncementMarkersProps) => {
    const [markers, setMarkers] = useStateBasedOnProps(convertAnnouncements(announcements))
    useEffect(() => {
        if (markers.length !== announcements.length) {
            setMarkers(convertAnnouncements(announcements))
        }
    }, [markers])

    const createMail = (contact: { email: string; phone?: string }, message: string) =>
        `mailto:${contact.email}?subject=Pomóż sąsiadowi&body=${message}`;

    return (
        <>
            {icons.length > 0 && markers.length > 0 && markers
                .map(({position, message, type, contact, isPaid}, i) =>
                    <Overlay
                        checked
                        aria-label={`announcements-over-layer-${icons[type - 1].name}`}
                        name={translate(`marker-image.${icons[type - 1].name}.type`)}
                        key={`announcements-${message}-${i}`}>
                        <Marker
                            icon={!isPaid ? icons[type - 1].icon : paidIcon(icons[type -1 ])}
                            position={position}>
                            <Popup
                                closeButton
                                closeOnClick
                                offset={iconPoint}>
                                {isPaid && <FontAwesomeIcon
                                    size="2x"
                                    icon="money-bill"
                                    style={{color: "#D6CF66"}}/>}
                                <div style={{textAlign: "center"}}>
                                    <b style={{fontSize: 14}}>
                                        Opis:
                                    </b>
                                </div>
                                {message}
                                <br/>
                                <div className="contact">
                                    <br/>
                                    <b>Kontakt</b>
                                    <address>
                                        <a href={createMail(contact, message)}>
                                            <FontAwesomeIcon icon="envelope"/>&nbsp;
                                            {contact.email}
                                        </a>
                                        <br/>
                                        {contact.phone && contact.phone !== "000000000" &&
                                        <a href={`tel:${contact.phone}`}>
                                            <FontAwesomeIcon icon="phone"/>&nbsp;
                                            {contact.phone}
                                        </a>}
                                    </address>
                                </div>
                            </Popup>
                        </Marker>
                    </Overlay>
                )
            }
        </>
    );
}

export default AnnouncementMarkers;