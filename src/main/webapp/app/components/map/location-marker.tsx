import React, {useState} from 'react';
import {Marker, Popup, useMapEvents} from 'react-leaflet';

/**
 * Component is responsible for add {@code Marker} and redirect map view
 * into the user geolocation.
 */
const LocationMarker = () => {
    const [position, setPosition] = useState(null)
    const map = useMapEvents({
        click() {
            map.locate()
        },
        locationfound(e) {
            setPosition(e.latlng)
            map.flyTo(e.latlng, map.getZoom())
        },
    })

    return position === null ? null : (
        <Marker position={position}>
            <Popup>
                Jesteś tutaj
            </Popup>
        </Marker>
    )
};

export default LocationMarker;