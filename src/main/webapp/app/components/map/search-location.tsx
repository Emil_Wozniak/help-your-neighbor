import React, {useEffect} from 'react';
import {BOTTOM_LEFT} from "app/config/constants";
import EsriLeafletGeoSearch from "react-esri-leaflet/plugins/EsriLeafletGeoSearch";
import CheckCircle from "app/components/map/check-circle";
import {renderToStaticMarkup} from 'react-dom/server';
import {translate} from "app/config/i18n/Translate";

type SearchLocationProps = {
    address: string;
    addLocation: (response) => void;
}

const SearchLocation = ({address, addLocation}: SearchLocationProps) => {

    const getSearchInput = (name: string) => document.getElementById(name);
    const addElement = (output: HTMLElement, parentId: string) => getSearchInput(parentId).appendChild(output);

    useEffect(() => {
        const elements = document.getElementsByClassName("geocoder-control")
        const searchInput = getSearchInput("search-location-input")
        const icons = getSearchInput("search-location-checked").children.length
        if (elements.length > 0) {
            const search = Array.from(elements).filter(it => it.classList.contains("leaflet-control"))[0]
            searchInput.appendChild(search)
        }
        if (address != "" && icons < 1) {
            const output = document.createElement("div")
            output.innerHTML = `${renderToStaticMarkup(<CheckCircle/>)}`
            addElement(output, "search-location-checked");
        }
    })

    return (
        <div className="ant-row ant-form-item search-location" style={{width: "calc(90% - 8px)"}}>
            <div className="ant-col ant-col-8 ant-form-item-label">
                <label
                    className="ant-form-item-required"
                    title={translate("map.form.address")}>
                    {translate("map.form.address")}
                </label>
            </div>
            {/* Here EsriLeafletGeoSearch goes */}
            <div id="search-location-input-container" className="ant-col ant-col-14 ant-form-item-control">
                <div id="search-location-input">
                </div>
                {address !== "" &&
                <small style={{color: 'gray'}}>
                    {address}
                </small>}
            </div>
            <div id="search-location-input-container" className="ant-col ant-col-2 ant-form-item-control">
                <div id="search-location-checked">
                </div>
            </div>
            {/* This will be extracted */}
            <div className="leaflet-control">
                <EsriLeafletGeoSearch
                    id="esri-leaflet-search"
                    title="Znajdź adres"
                    placeholder={address}
                    useMapBounds={false}
                    position={BOTTOM_LEFT}
                    eventHandlers={{
                        results: (response) => {
                            if (response && response.results.length > 0) addLocation(response)
                            else console.error("location not found")
                        }
                    }}/>
            </div>
        </div>
    );
};

export default SearchLocation;