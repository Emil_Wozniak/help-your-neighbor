import React from 'react';
import {getEntities as getAnnouncements} from "app/components/reducers/announcement.reducer";
import {connect} from 'react-redux';
import {Button, Menu} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {ProfileItemProps} from "app/components/profile/profile-item";

interface IFetchAnnouncements extends ProfileItemProps, DispatchProps {
}

const FetchAnnouncements = ({action, getAnnouncements, ...props}: IFetchAnnouncements) => {
    const {title, symbol, label = true} = props;
    return (
        <Menu.Item
            icon={<FontAwesomeIcon color="black" icon={symbol} style={{marginRight: 4}}/>}
            {...props}
            key={`fetch-${label}-key`}
            title={title.toUpperCase()}>
            <Button type="text" onClick={() => getAnnouncements()}>
                {label}
            </Button>
        </Menu.Item>
    );
};

const mapDispatchToProps = {
    getAnnouncements,
};

type DispatchProps = typeof mapDispatchToProps;

export default connect(null, mapDispatchToProps)(FetchAnnouncements);