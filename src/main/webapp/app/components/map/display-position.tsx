import React, {useCallback, useEffect, useState} from 'react';
import {Button} from 'antd';
import {usePromise} from "app/config/usePromise";

const DisplayPosition = ({map, coords, zoom}) => {
    const [position, setPosition] = useState(map.getCenter())
    const onClick = useCallback(() => {
        map.setView(coords, zoom)
    }, [map])

    const onMove = useCallback(() => {
        setPosition(map.getCenter())
    }, [map])

    usePromise(() => {
        map.on('move', onMove)
        return () => {
            map.off('move', onMove)
        }
    },[map, onMove])
    return (
        <Button onClick={onClick}>
            Show my localization
        </Button>
    )

};

export default DisplayPosition;