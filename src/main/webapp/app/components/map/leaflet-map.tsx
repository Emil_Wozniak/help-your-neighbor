import React, {useEffect, useState} from 'react';
import {LayersControl, MapContainer, TileLayer, ZoomControl} from 'react-leaflet';
import {LatLngLiteral} from 'leaflet';
import LocationMarker from "app/components/map/location-marker";
import DisplayPosition from "app/components/map/display-position";
import {ILeafletAnnouncement} from "app/entities/leaflet-announcement";
import AnnouncementMarkers from "app/components/map/announcement-markers";
import {CyclOSM, Esri_WorldImagery, OSM_MAP, TOP_RIGHT} from "app/config/constants";
import {locateUser} from "app/components/map/locate-user";
import {createIcons, IconModel} from "app/components/map/icons/marker-icons";
import {IRootState} from "app/components/reducers";
import {getEntities as getImages} from "app/components/reducers/marker-image.reducer";
import {createEntity as addEntity} from "app/components/reducers/announcement.reducer";
import {connect} from 'react-redux';
import {useStateBasedOnProps} from "app/config/use-state-based-on-props";
import {onMoveEnd} from "app/components/map/functions/on-move-end";
import {translate} from "app/config/i18n/Translate";

interface LeafletMapProps extends StateProps, DispatchProps {
    isLogin?: boolean;
    auto?: boolean
    zoom?: number;
    redirect?: boolean;
    findButton?: boolean;
    children?: JSX.Element | JSX.Element [] | string;
}

/**
 * Abstract type of Leaflet map for all maps.
 *
 * Leaflet map is using OSM map tiles by default, it will center on
 * position target by {@param coords}, with zoom value equals {@param zoom}.
 * Map then can be redirected into user position if {@param redirect}
 * is set in constructor.
 *
 * @param zoom              {@link Map} zoom value
 * @param auto              if set render view will be redirected automatically to the user location
 * @param redirect    if set redirect user to his position when map is clicked
 * @param findButton        if set render find button
 * @param announcements     read only array of {@link ILeafletAnnouncement}
 * @constructor required params: {@param zoom} {@param coords}
 * @see OSM_MAP
 */
const LeafletMap = (
    {
        isLogin = false,
        auto,
        zoom = 18,
        redirect,
        findButton,
        announcements,
        images,
        children
    }: LeafletMapProps) => {
    const warsaw = {lat: 52.20981129611399, lng: 21.00551334076086} as LatLngLiteral;
    const [search, setSearch] = useState(true)
    const [map, setMap] = useState(null)
    const [icons, setIcons] = useState([] as Array<IconModel>)
    const [markers, setMarkers] = useStateBasedOnProps(announcements)
    useEffect(() => {
        getImages()
        if (map && auto && search) locateUser(map, search, setSearch);
        if (announcements !== markers) setMarkers(announcements)
        if (images) {
            const models = createIcons(images);
            setIcons(models)
        }
    }, [markers])
    if (map) {
        map.on('moveend', onMoveEnd);
    }
    const standard = translate("map.layer.standard")
    const cycle = translate("map.layer.cycle")
    const world = translate("map.layer.world")
    return (
        <div style={{zIndex: 1, position: "absolute", width: isLogin ? "100%" : "50%"}}>
            {findButton && map
                ? <DisplayPosition
                    coords={warsaw}
                    zoom={zoom}
                    map={map}/>
                : null}
            <MapContainer
                id="leaflet-map-container-component"
                aria-label="main map container"
                zoomControl={false}
                center={warsaw}
                zoom={zoom}
                scrollWheelZoom={true}
                whenCreated={setMap}>
                <LayersControl collapsed={false} position={TOP_RIGHT}>
                    <LayersControl.BaseLayer checked name={standard}>
                        <TileLayer id="tile-layer-default" {...OSM_MAP}/>
                    </LayersControl.BaseLayer>
                    <LayersControl.BaseLayer name={cycle}>
                        <TileLayer {...CyclOSM} id="tile-layer-cycle"/>
                    </LayersControl.BaseLayer>
                    <LayersControl.BaseLayer name={world}>
                        <TileLayer id="tile-layer-esri" {...Esri_WorldImagery} />
                    </LayersControl.BaseLayer>
                    <AnnouncementMarkers announcements={announcements} icons={icons}/>
                </LayersControl>
                <ZoomControl position={TOP_RIGHT} aria-label="map-zoom"/>
                {children &&
                <div className="geocoder-control">
                    {children}
                </div>}
                {redirect && <LocationMarker aria-label="location-marker-default"/>}
            </MapContainer>
        </div>
    );
}

const mapStateToProps = ({announcement, markerImage, locale}: IRootState) => ({
    locale: locale.currentLocale,
    announcements: announcement.entities,
    images: markerImage.entities
});

const mapDispatchToProps = {
    getImages,
    addEntity
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LeafletMap);