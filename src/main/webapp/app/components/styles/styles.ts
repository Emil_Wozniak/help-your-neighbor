export const ROOT = '#FFD700';
export const ROOT_DARK = '#DAA520';
export const SECONDARY_COLOR ='#125AB3' ;
export const SECONDARY_COLOR_LIGHT = '#125AB3';
export const SECONDARY_COLOR_VERY_LIGHT = '#c8b2e3';

export const HOME = { minHeight: '50vh', maxHeight: '83vh', };
