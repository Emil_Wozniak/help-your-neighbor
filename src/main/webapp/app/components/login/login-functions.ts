import { useGoogleLogin, useGoogleLogout, UseGoogleLoginProps } from 'react-google-login'

export const googleLogin = ({ signIn, loaded }) => useGoogleLogin(signIn)

export const googleLogout = ({ signOut, loaded }) => useGoogleLogout(signOut)