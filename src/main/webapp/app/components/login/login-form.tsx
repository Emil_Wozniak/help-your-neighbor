import React, {Component} from 'react';
import {login} from "app/components/auth/api-utils";
import {ACCESS_TOKEN} from "app/components/auth/auth-constants";
import Alert from 'react-s-alert';

type LoginFormProps = {}

type LoginFormState = {
    email: string
    password: string
}

class LoginForm extends Component<LoginFormProps, LoginFormState> {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    handleInputChange = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value} as Pick<LoginFormState, keyof LoginFormState>);
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const loginRequest = Object.assign({}, this.state);

        login(loginRequest)
            .then(response => {
                localStorage.setItem(ACCESS_TOKEN, response.accessToken);
                Alert.success("You're successfully logged in!");
                window.location.href = "/"
            }).catch(error => {
            Alert.error((error && error.message) || 'Oops! Something went wrong. Please try again!');
        });
    }

    render() {
        const {email, password} = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="input-group">
                    <input
                        type="email"
                        name="email"
                        className="form-control"
                        placeholder="Email"
                        value={email}
                        onChange={this.handleInputChange} required/>
                    <input
                        type="password"
                        name="password"
                        className="form-control"
                        placeholder="Password"
                        value={password}
                        onChange={this.handleInputChange} required/>
                </div>
                <div>
                    <button type="submit">
                        Login
                    </button>
                </div>
            </form>
        );
    }
}

export default LoginForm;