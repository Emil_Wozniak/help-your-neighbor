import React from 'react';
import LoginForm from "app/components/login/login-form";

type LoginProps = {
    authenticated?: boolean;
}

const LoginContainer = ({authenticated}: LoginProps) =>
    <LoginForm/>;

export default LoginContainer;