import React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route, RouteProps} from 'react-router-dom';
import {IRootState} from 'app/components/reducers';
import ErrorBoundary from "app/components/error/error-boundry";

interface IOwnProps extends RouteProps {
    hasAuthorities?: string[];
}

export interface IPrivateRouteProps extends IOwnProps, StateProps {
}

const goTo = props => ({
    pathname: '/config.auth',
    search: props.location.search,
    state: {from: props.location},
});

export const PrivateRouteComponent = ({
                                          component: Component,
                                          isAuthenticated,
                                          sessionHasBeenFetched,
                                          isAuthorized,
                                          hasAuthorities = [],
                                          ...rest
                                      }: IPrivateRouteProps) => {
    const checkAuthorities = (props) =>
        hasAuthorities.length > 0 ? (
            <ErrorBoundary>
                <Component {...props} />
            </ErrorBoundary>
        ) : (
            <div className="insufficient-authority">
                <div className="alert alert-danger">
                    You are not authorized to access this page.
                </div>
            </div>
        );

    const renderRedirect = (props) => {
        return !sessionHasBeenFetched
            ? <div/>
            : hasAuthorities.length > 0
                ? checkAuthorities(props)
                : <Redirect to={goTo(props)}/>;
    };

    if (!Component) throw new Error(`A component needs to be specified for private route for path ${(rest as any).path}`);

    return <Route {...rest} render={renderRedirect}/>;
};

export const hasAuthority = (authorities: ReadonlyArray<AuthorityEntity>, hasAuthorities: string[]) =>
    (authorities && authorities.length !== 0)
        ? (hasAuthorities.length === 0)
        ? true
        : hasAuthorities.some(rule => authorities
            .map(it => it.name)
            .includes(rule))
        : false;

const mapStateToProps = (
    {authentication: {isAuthenticated, account, sessionHasBeenFetched}}: IRootState,
    {hasAuthorities = []}: IOwnProps
) => ({
    isAuthenticated,
    isAuthorized: hasAuthority(account.authorities, hasAuthorities),
    sessionHasBeenFetched,
});

type StateProps = ReturnType<typeof mapStateToProps>;

/**
 * A route wrapped in an authentication check so that routing happens only when you are authenticated.
 * Accepts same props as React router Route.
 * The route also checks for authorization if hasAnyAuthorities is specified.
 */
export const PrivateRoute = connect(mapStateToProps, null, null, {pure: false})(PrivateRouteComponent);

export default PrivateRoute;
