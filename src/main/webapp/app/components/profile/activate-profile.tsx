import React, {useState} from 'react';
import {Button, Card, Form, Input, Row} from 'antd';
import {SubMenuProps} from 'antd/lib/menu/SubMenu';
import {UserEntity} from "app/entities/user.entity";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {formatAlert} from "app/components/layout/grid/form-alert";
import {PHONE_REGEX} from "app/config/constants";
import {
    accountInactive,
    btnLabel,
    layout,
    phoneItemIntRules,
    subtitle
} from "app/components/profile/activate-constants";
import ActivateHint from "app/components/profile/activate-hint";

interface ActivateProfileProps extends SubMenuProps {
    user: UserEntity
    update: Function
    history: any
}

const ActivateProfile = ({update, user, history}: ActivateProfileProps) => {
    const [form] = Form.useForm();
    const [requiredMark] = useState<boolean | 'optional'>('optional');
    const [phone, setPhone] = useState(null as null | string)

    const inputChange = (event, condition, callback) => {
        const {value} = event.currentTarget
        if (value.match(condition)) callback(value)
        else formatAlert(event);
    };

    const onFinish = (values: any) => {
        const data: UserEntity = {...user}
        data.phone = values.phone
        return update(data, history)
    };

    const onFinishFailed = (errorInfo: any) => console.error('Failed:', errorInfo);

    return (
        <Row>
            <Card className="profile-activate">
                <Card.Meta title={accountInactive} description={subtitle}/>
                <br/>
                <Row align="middle" justify="center">
                    <Form
                        {...layout}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        scrollToFirstError
                        form={form}
                        layout="vertical"
                        requiredMark={requiredMark}>
                        <Form.Item
                            rules={phoneItemIntRules}
                            validateStatus="error"
                            name="phone"
                            required
                            style={{width: "100%", marginLeft: "4rem"}}
                        >
                            <Input
                                id="phone-number-input"
                                size="small"
                                aria-label="phone number"
                                aria-errormessage="incorrect phone number"
                                prefix={<FontAwesomeIcon icon="phone"/>}
                                suffix={<ActivateHint/>}
                                maxLength={12}
                                pattern="[0-9]{9}"
                                onChange={(event) =>
                                    inputChange(event, PHONE_REGEX, setPhone)}
                                value={phone}
                                placeholder="numer telefonu"/>
                        </Form.Item>

                        <Form.Item style={{width: "100%", marginLeft: "4rem"}}>
                            <Button htmlType="submit" size="small" type="primary">
                                {btnLabel}
                            </Button>
                        </Form.Item>
                    </Form>
                </Row>
            </Card>
        </Row>
    );
};

export default ActivateProfile;