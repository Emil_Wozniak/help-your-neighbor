import React from 'react';
import {Menu} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IconProp} from '@fortawesome/fontawesome-svg-core'
import LinkButton from "app/components/layout/grid/link-button";
import {MenuItemProps} from 'antd/lib/menu/MenuItem';

// @ts-ignore
export interface ProfileItemProps extends MenuItemProps {
    label: string;
    title: string;
    symbol: IconProp;
    link?: boolean;
    action?: Function;
    to?: string;
}

const ProfileItem = ({action, ...props}: ProfileItemProps) => {
    const {title, symbol, to, label, link = true} = props;
    const key = `profile item ${label}`;
    // @ts-ignore
    return (<Menu.Item
            icon={<FontAwesomeIcon color="black" icon={symbol} style={{marginRight: 4}}/>}
            {...props}
            key={`profile-${label}-key`}
            title={title.toUpperCase()}>
            {link &&
            <LinkButton
                aria-label={`profile item ${label}`}
                label={key}
                style={{color: "#000"}}
                onClick={() => action()}
                to={to}>
                {label}
            </LinkButton>
            }
        </Menu.Item>
    );
};

export default ProfileItem;