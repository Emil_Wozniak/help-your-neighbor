export const accountInactive = "Konto nieaktywne";
export const subtitle = "Aktywuj numerem telefonu";
export const btnLabel = "Aktywuj";
export const phoneRegex = /[0-9]{9}/
export const phoneLabel = "Telefon";
export     const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

export const phoneItemIntRules = [
    {
        required: true,
        message: 'Numer telefonu nie może być pusty'
    }
]