import React from 'react';
import {Menu} from 'antd';
import {SubMenuProps} from 'antd/lib/menu/SubMenu';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface ProfileFormProps extends SubMenuProps {
    onClick: Function;
    symbol: IconProp;
}

const ProfileSubmenu = (props: ProfileFormProps) => (
    <Menu.SubMenu {...props}>
        <div
            role="menuitem"
            style={{height: 200}}
            title="Actualize">
        </div>
    </Menu.SubMenu>
);

export default ProfileSubmenu;