import React from 'react';

const PageNotFound = () => (
    <div>
        The page does not exist.
    </div>
);

export default PageNotFound;