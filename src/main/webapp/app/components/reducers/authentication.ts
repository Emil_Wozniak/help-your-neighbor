import axios from 'axios';

import {FAILURE, REQUEST, SUCCESS} from 'app/components/reducers/action-type.util';
import {userFromPrincipal, UserEntity} from "app/entities/user.entity";
import {AUTH_TOKEN_KEY, SERVER_URL} from "app/config/constants";
import { setLocale } from 'app/components/reducers/locale';
import {Storage} from "app/config/i18n/StorageType"

export const ACTION_TYPES = {
    REGISTER_OAUTH: 'authentication/REGISTER_OAUTH',
    REGISTER_ERROR: 'authentication/REGISTER_ERROR',
    LOGIN_OAUTH: 'authentication/LOGIN_OAUTH',
    LOGOUT_OAUTH: 'authentication/LOGIN_OAUTH',
    GET_USER: 'authentication/GET_USER',
    GET_SESSION: 'authentication/GET_SESSION',
    LOGOUT: 'authentication/LOGOUT',
    CLEAR_AUTH: 'authentication/CLEAR_AUTH',
    ERROR_MESSAGE: 'authentication/ERROR_MESSAGE',
    UPDATE_USER: 'authentication/UPDATE_USER',
    DELETE_OAUTH_USER: 'DELETE_OAUTH_USER'
};


const SESSION = "UI" + "SESSION"

const initialState = {
    loading: false,
    isAuthenticated: false,
    loginSuccess: false,
    loginError: false, // Errors returned from server side
    showModalLogin: false,
    error: null,
    account: {} as UserEntity,
    errorMessage: (null as unknown) as string, // Errors returned from server side
    redirectMessage: (null as unknown) as string,
    sessionHasBeenFetched: false,
    idToken: (null as unknown) as string,
    logoutUrl: (null as unknown) as string,
};

export type AuthenticationState = Readonly<typeof initialState>;

/**
 * Reducer
 */
export default (state: AuthenticationState = initialState, action): AuthenticationState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.GET_USER):
            return {
                ...state,
                loading: true,
            };
        case REQUEST(ACTION_TYPES.GET_SESSION):
            return {
                ...state,
                loading: true,
            };
        case REQUEST(ACTION_TYPES.LOGIN_OAUTH):
            return {
                ...state,
                loading: true
            }
        case REQUEST(ACTION_TYPES.LOGOUT_OAUTH):
            return {
                ...state,
                loading: false
            }
        case REQUEST(ACTION_TYPES.UPDATE_USER):
            return {
                ...state,
                loading: true
            }
        //    SUCCESS
        case SUCCESS(ACTION_TYPES.GET_USER): {
            return {
                ...state,
                loading: false,
                sessionHasBeenFetched: true,
                account: action.payload.data,
            };
        }
        case SUCCESS(ACTION_TYPES.REGISTER_OAUTH):
            return {
                ...state,
                account: action.payload.data.account,
                idToken: action.payload.data.idToken,
                isAuthenticated: true,
                loading: false,
                loginError: false,
                loginSuccess: true,
            };
        case SUCCESS(ACTION_TYPES.LOGIN_OAUTH):
            return {
                ...state,
                idToken: action.payload.data.idToken,
                loading: false,
                loginError: false,
                loginSuccess: true,
                isAuthenticated: true
            };
        case SUCCESS(ACTION_TYPES.LOGOUT_OAUTH):
            return {
                ...state,
                isAuthenticated: false
            }
        case SUCCESS(ACTION_TYPES.GET_SESSION): {
            const {data} = action.payload
            const activated = data.authenticated ? data.authenticated : false
            const user = data.principal ? userFromPrincipal(data.principal) : {}
            return {
                ...state,
                isAuthenticated: activated,
                loading: false,
                sessionHasBeenFetched: true,
                account: user,
            };
        }
        //    FAILURE
        case FAILURE(ACTION_TYPES.GET_USER):
            return {
                ...state,
                loading: false,
                isAuthenticated: false,
                sessionHasBeenFetched: true,
                showModalLogin: true,
                errorMessage: action.payload,
            };
        case FAILURE(ACTION_TYPES.REGISTER_OAUTH):
            return {
                ...state,
                loading: false,
                loginError: true,
                loginSuccess: false,
                errorMessage: action.payload
            };
        case FAILURE(ACTION_TYPES.LOGIN_OAUTH):
            return {
                ...state,
                loading: false,
                loginError: true,
                loginSuccess: false,
                errorMessage: action.payload
            };
        case FAILURE(ACTION_TYPES.LOGOUT_OAUTH):
            return {
                ...state,
                loading: false,
                loginError: true,
                loginSuccess: false,
                errorMessage: action.payload
            }
        case FAILURE(ACTION_TYPES.GET_SESSION):
            return {
                ...state,
                loading: false,
                isAuthenticated: false,
                sessionHasBeenFetched: true,
                showModalLogin: true,
                errorMessage: action.payload,
            };
        case FAILURE(ACTION_TYPES.UPDATE_USER):
            return {
                ...state,
                errorMessage: action.payload
            }
        case ACTION_TYPES.LOGOUT:
            return {
                ...initialState,
                showModalLogin: true,
            };
        case ACTION_TYPES.ERROR_MESSAGE:
            return {
                ...initialState,
                showModalLogin: true,
                redirectMessage: action.message,
            };
        case ACTION_TYPES.CLEAR_AUTH:
            return {
                ...state,
                loading: false,
                showModalLogin: true,
                isAuthenticated: false,
            };
        case ACTION_TYPES.REGISTER_OAUTH:
            return {
                ...state,
                account: action.payload.data.account,
                idToken: action.payload.data.idToken,
                loading: true
            }
        case ACTION_TYPES.REGISTER_ERROR:
            return {
                ...state,
                error: action.payload.data,
                loading: false,
                loginError: true,
                loginSuccess: false
            }
        default:
            return state;
    }
};

const PATH = "/route/users"
const REGISTER = "/route/register"
const ACCOUNT = "/route/account"

const getXSRF = () => {
    const keys = document.cookie
        ? document.cookie
            .split(';')
            .find(cookie => cookie.startsWith(AUTH_TOKEN_KEY))
        : ""
    return keys ? keys.split("=")[1] : "";
};

const headers = () => ({
    withCredentials: true,
    headers: {'X-XSRF-TOKEN': getXSRF()}
})

export const getSession = () => async (dispatch, getState) => {
    await dispatch({
        type: ACTION_TYPES.GET_SESSION,
        payload: axios.get(ACCOUNT, headers()),
    });

    const { account } = getState().authentication;
    if (account && account.langKey) {
        const langKey = Storage.session.get('locale', account.langKey);
        await dispatch(setLocale(langKey));
    }
}

export const getUser = () => async dispatch =>
    await dispatch({
        type: ACTION_TYPES.GET_USER,
        payload: axios.get(REGISTER, headers()),
    });

export const displayAuthError = message => ({type: ACTION_TYPES.ERROR_MESSAGE, message});

export const logoutOAuth2Google = (history) => async dispatch => {
    clearAuthToken();
    const response = dispatch({
        type: ACTION_TYPES.LOGOUT_OAUTH,
        payload: await axios.post(`${SERVER_URL}/logout`, headers())
    })
    return response
        .catch(error => console.error('BAD REQUEST', error))
        .then((_) => setTimeout(
            () => history.push(`${SERVER_URL}/`),
            100))
}

export const removeAccount = (email: any) => async dispatch => {
    const route = `${PATH}/${email}`
    dispatch({
        type: ACTION_TYPES.DELETE_OAUTH_USER,
        payload: await axios.delete(`${route}`, headers())
    }).then((_) => clearAuthToken())
}

export const updateUser = (user: UserEntity, history) => async dispatch => {
    const response = dispatch({
        type: ACTION_TYPES.UPDATE_USER,
        payload: await axios.put(PATH, user, headers())
    })
    response.then(setTimeout(
        () => history.push(`/`),
        100))
}

export const clearAuthToken = () => {
    if (window.sessionStorage.getItem(AUTH_TOKEN_KEY)) {
        window.sessionStorage.removeItem(AUTH_TOKEN_KEY);
    }
    if (window.sessionStorage.getItem(SESSION)) {
        window.sessionStorage.removeItem(SESSION)
    }
};

export const clearAuthentication = messageKey => (dispatch, _) => {
    clearAuthToken();
    dispatch(displayAuthError(messageKey));
    dispatch({type: ACTION_TYPES.CLEAR_AUTH});
};
