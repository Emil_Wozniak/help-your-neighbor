import axios from 'axios';

import {FAILURE, REQUEST, SUCCESS} from 'app/components/reducers/action-type.util';
import {
    ICrudDeleteAction,
    ICrudGetAction,
    ICrudGetAllAction,
    ICrudPutAction
} from "app/components/reducers/redux-action-type";
import {defaultValue, IHashtagModel} from "app/entities/hashtag.entity";


export const ACTION_TYPES = {
    FETCH_HASHTAG_LIST: 'hashtag/FETCH_HASHTAG_LIST',
    FETCH_HASHTAG: 'hashtag/FETCH_HASHTAG',
    CREATE_HASHTAG: 'hashtag/CREATE_HASHTAG',
    UPDATE_HASHTAG: 'hashtag/UPDATE_HASHTAG',
    DELETE_HASHTAG: 'hashtag/DELETE_HASHTAG',
    RESET: 'hashtag/RESET'
};

const initialState = {
    loading: false,
    errorMessage: null,
    entities: [] as ReadonlyArray<IHashtagModel>,
    entity: defaultValue as IHashtagModel,
    updating: false,
    updateSuccess: false
};

export type HashtagState = Readonly<typeof initialState>;

// Reducer

export default (state: HashtagState = initialState, action): HashtagState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.FETCH_HASHTAG_LIST):
        case REQUEST(ACTION_TYPES.FETCH_HASHTAG):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                loading: true
            };
        case REQUEST(ACTION_TYPES.CREATE_HASHTAG):
        case REQUEST(ACTION_TYPES.UPDATE_HASHTAG):
        case REQUEST(ACTION_TYPES.DELETE_HASHTAG):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                updating: true
            };
        case FAILURE(ACTION_TYPES.FETCH_HASHTAG_LIST):
        case FAILURE(ACTION_TYPES.FETCH_HASHTAG):
        case FAILURE(ACTION_TYPES.CREATE_HASHTAG):
        case FAILURE(ACTION_TYPES.UPDATE_HASHTAG):
        case FAILURE(ACTION_TYPES.DELETE_HASHTAG):
            return {
                ...state,
                loading: false,
                updating: false,
                updateSuccess: false,
                errorMessage: action.payload
            };
        case SUCCESS(ACTION_TYPES.FETCH_HASHTAG_LIST):
            return {
                ...state,
                loading: false,
                entities: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.FETCH_HASHTAG):
            return {
                ...state,
                loading: false,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.CREATE_HASHTAG):
        case SUCCESS(ACTION_TYPES.UPDATE_HASHTAG):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.DELETE_HASHTAG):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: {} as IHashtagModel
            };
        default:
            return state;
    }
};

const API_URL = '/route/hashtags';

// Actions

export const getEntities: ICrudGetAllAction<IHashtagModel> = () => ({
    type: ACTION_TYPES.FETCH_HASHTAG_LIST,
    payload: axios.get<IHashtagModel>(`${API_URL}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IHashtagModel> = id => {
    const requestUrl = `${API_URL}/${id}`;
    return {
        type: ACTION_TYPES.FETCH_HASHTAG,
        payload: axios.get<IHashtagModel>(requestUrl)
    };
};

export const createEntity: ICrudPutAction<IHashtagModel> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.CREATE_HASHTAG,
        payload: axios.post(API_URL, entity)
    });
    dispatch(getEntities());
    return result;
};

export const updateEntity: ICrudPutAction<IHashtagModel> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.UPDATE_HASHTAG,
        payload: axios.put(API_URL, entity)
    });
    dispatch(getEntities());
    return result;
};

export const deleteEntity: ICrudDeleteAction<IHashtagModel> = id => async dispatch => {
    const requestUrl = `${API_URL}/${id}`;
    const result = await dispatch({
        type: ACTION_TYPES.DELETE_HASHTAG,
        payload: axios.delete(requestUrl)
    });
    dispatch(getEntities());
    return result;
};

export const reset = () => ({
    type: ACTION_TYPES.RESET
});
