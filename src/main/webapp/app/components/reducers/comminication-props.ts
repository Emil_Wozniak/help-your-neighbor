export const ROUTE = "route"
export const AUTH_ROUTE_REGISTER = ROUTE + "/config.auth/register"
export const AUTH_ROUTE_LOGIN = ROUTE + "/config.auth/login"
export const AUTH_ROUTE_ACCOUNT = ROUTE + "/config.auth/account"
export const ROUTE_OAUTH2_LOGOUT = "http://localhost:8088/logout"
export const TOKEN_HEADER = {xsrfHeaderName: 'X-XSRF-TOKEN'};
export const MANAGEMENT_INFO = ROUTE + "/management/info";