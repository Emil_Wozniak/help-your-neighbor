import axios from 'axios';

import {FAILURE, REQUEST, SUCCESS} from 'app/components/reducers/action-type.util';
import {
    ICrudDeleteAction,
    ICrudGetAction,
    ICrudGetAllAction,
    ICrudPutAction
} from "app/components/reducers/redux-action-type";
import {defaultValue, IMarkerImageModel} from "app/entities/marker-image.entity";

export const ACTION_TYPES = {
    FETCH_MARKER_IMAGE_LIST: 'hashtag/FETCH_MARKER_IMAGE_LIST',
};

const initialState = {
    loading: false,
    errorMessage: null,
    entities: [] as ReadonlyArray<IMarkerImageModel>,
    entity: defaultValue as IMarkerImageModel,
    updating: false,
    updateSuccess: false
};

export type MarkerImageState = Readonly<typeof initialState>;

// Reducer

export default (state: MarkerImageState = initialState, action): MarkerImageState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.FETCH_MARKER_IMAGE_LIST):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                loading: true
            };
        case FAILURE(ACTION_TYPES.FETCH_MARKER_IMAGE_LIST):
            return {
                ...state,
                loading: false,
                updating: false,
                updateSuccess: false,
                errorMessage: action.payload
            };
        case SUCCESS(ACTION_TYPES.FETCH_MARKER_IMAGE_LIST):
            return {
                ...state,
                loading: false,
                entities: action.payload.data
            };
        default:
            return state;
    }
};

const API_URL = '/route/marker-images';

// Actions

export const getEntities: ICrudGetAllAction<IMarkerImageModel> = () => ({
    type: ACTION_TYPES.FETCH_MARKER_IMAGE_LIST,
    payload: axios.get<IMarkerImageModel>(`${API_URL}?cacheBuster=${new Date().getTime()}`)
});
