import axios from 'axios';

import {FAILURE, SUCCESS} from 'app/components/reducers/action-type.util';
import {MANAGEMENT_INFO} from "app/components/reducers/comminication-props";

export const ACTION_TYPES = {
    GET_PROFILE: 'applicationProfile/GET_PROFILE'
};

const initialState = {
    ribbonEnv: 'dev',
    inProduction: true,
};

export type ApplicationProfileState = Readonly<typeof initialState>;

export default (state: ApplicationProfileState = initialState, action): ApplicationProfileState => {
    switch (action.type) {
        case SUCCESS(ACTION_TYPES.GET_PROFILE): {
            const {data} = action.payload;
            return {
                ...state,
                ribbonEnv: data.ribbon ? "dev" : "prod",
                inProduction: data.profiles.includes('prod'),
            };
        }
        case FAILURE(ACTION_TYPES.GET_PROFILE):
            console.error("Can't get profile settings!")
            return {
                ...state,
            }
        default:
            return state;
    }
};


export const getProfile = () => dispatch => dispatch({
    type: ACTION_TYPES.GET_PROFILE,
    payload: axios.get(MANAGEMENT_INFO)
});

