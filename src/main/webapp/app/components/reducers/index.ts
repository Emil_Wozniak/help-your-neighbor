import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import applicationProfile, { ApplicationProfileState } from './application-profile';
import authentication, { AuthenticationState } from './authentication';
import announcement, {AnnouncementState} from "app/components/reducers/announcement.reducer";
import hashtag, {HashtagState} from "app/components/reducers/hashtag.reducer";
import markerImage, {MarkerImageState} from "app/components/reducers/marker-image.reducer";

export interface IRootState {
    readonly authentication: AuthenticationState;
    readonly locale: LocaleState;
    readonly applicationProfile: ApplicationProfileState;
    readonly announcement: AnnouncementState;
    readonly hashtag: HashtagState;
    readonly markerImage: MarkerImageState;
    readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
    applicationProfile,
    locale,
    authentication,
    announcement,
    hashtag,
    markerImage,
    loadingBar
})

export default rootReducer;