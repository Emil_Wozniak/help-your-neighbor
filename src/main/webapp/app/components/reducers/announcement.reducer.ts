import axios from 'axios';

import { REQUEST, SUCCESS, FAILURE } from 'app/components/reducers/action-type.util';
import {defaultValue, IAnnouncementModel} from "app/entities/announcement.entity";
import {
    ICrudDeleteAction,
    ICrudGetAction,
    ICrudGetAllAction,
    ICrudPutAction
} from "app/components/reducers/redux-action-type";


export const ACTION_TYPES = {
    FETCH_ANNOUNCEMENT_LIST: 'announcement/FETCH_ANNOUNCEMENT_LIST',
    FETCH_ANNOUNCEMENT: 'announcement/FETCH_ANNOUNCEMENT',
    CREATE_ANNOUNCEMENT: 'announcement/CREATE_ANNOUNCEMENT',
    UPDATE_ANNOUNCEMENT: 'announcement/UPDATE_ANNOUNCEMENT',
    DELETE_ANNOUNCEMENT: 'announcement/DELETE_ANNOUNCEMENT',
    RESET: 'announcement/RESET'
};

const initialState = {
    loading: false,
    errorMessage: null,
    entities: [] as ReadonlyArray<IAnnouncementModel>,
    entity: defaultValue as IAnnouncementModel,
    updating: false,
    updateSuccess: false
};

export type AnnouncementState = Readonly<typeof initialState>;

// Reducer

export default (state: AnnouncementState = initialState, action): AnnouncementState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.FETCH_ANNOUNCEMENT_LIST):
        case REQUEST(ACTION_TYPES.FETCH_ANNOUNCEMENT):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                loading: true
            };
        case REQUEST(ACTION_TYPES.CREATE_ANNOUNCEMENT):
        case REQUEST(ACTION_TYPES.UPDATE_ANNOUNCEMENT):
        case REQUEST(ACTION_TYPES.DELETE_ANNOUNCEMENT):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                updating: true
            };
        case FAILURE(ACTION_TYPES.FETCH_ANNOUNCEMENT_LIST):
        case FAILURE(ACTION_TYPES.FETCH_ANNOUNCEMENT):
        case FAILURE(ACTION_TYPES.CREATE_ANNOUNCEMENT):
        case FAILURE(ACTION_TYPES.UPDATE_ANNOUNCEMENT):
        case FAILURE(ACTION_TYPES.DELETE_ANNOUNCEMENT):
            return {
                ...state,
                loading: false,
                updating: false,
                updateSuccess: false,
                errorMessage: action.payload
            };
        case SUCCESS(ACTION_TYPES.FETCH_ANNOUNCEMENT_LIST):
            return {
                ...state,
                loading: false,
                entities: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.FETCH_ANNOUNCEMENT):
            return {
                ...state,
                loading: false,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.CREATE_ANNOUNCEMENT):
        case SUCCESS(ACTION_TYPES.UPDATE_ANNOUNCEMENT):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.DELETE_ANNOUNCEMENT):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: {} as IAnnouncementModel
            };
        default:
            return state;
    }
};

const API_URL = '/route/announcements/';

// Actions

export const getEntities: ICrudGetAllAction<IAnnouncementModel> = () => ({
    type: ACTION_TYPES.FETCH_ANNOUNCEMENT_LIST,
    payload: axios.get<IAnnouncementModel>(`${API_URL}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IAnnouncementModel> = id => {
    const requestUrl = `${API_URL}/${id}`;
    return {
        type: ACTION_TYPES.FETCH_ANNOUNCEMENT,
        payload: axios.get<IAnnouncementModel>(requestUrl)
    };
};

export const createEntity: ICrudPutAction<IAnnouncementModel> = (entity) => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.CREATE_ANNOUNCEMENT,
        payload: axios.post(API_URL, entity)
    });
    dispatch(getEntities());
    return result;
};

export const updateEntity: ICrudPutAction<IAnnouncementModel> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.UPDATE_ANNOUNCEMENT,
        payload: axios.put(API_URL, entity)
    });
    dispatch(getEntities());
    return result;
};

export const deleteEntity: ICrudDeleteAction<IAnnouncementModel> = id => async dispatch => {
    const requestUrl = `${API_URL}/${id}`;
    const result = await dispatch({
        type: ACTION_TYPES.DELETE_ANNOUNCEMENT,
        payload: axios.delete(requestUrl)
    });
    dispatch(getEntities());
    return result;
};

export const reset = () => ({
    type: ACTION_TYPES.RESET
});
