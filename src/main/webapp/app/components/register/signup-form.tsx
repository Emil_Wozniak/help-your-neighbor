import React, {Component} from 'react';
import {signup} from "app/components/auth/api-utils";
import Alert from 'react-s-alert';

type SignupFormState = {
    name: string;
    email: string;
    password: string;
}

type SignupFormProps = {}

class SignupForm extends Component<SignupFormProps, SignupFormState> {
    constructor(props) {
        super(props);
        this.state = {name: '', email: '', password: ''}
    }

    handleInputChange = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value} as Pick<SignupFormState, keyof SignupFormState>);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const assign = Object.assign({}, this.state);
        signup(assign)
            .then(_ => {
                Alert.success("You're successfully registered. Please login to continue!");
                window.location.href = "/login"
            })
            .catch((error) => {
                Alert.error((error && error.message) || 'Oops! Something went wrong. Please try again!');
            });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="input-group">
                    <input
                        type="email"
                        name="email"
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.handleInputChange}
                        required/>
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleInputChange}
                        required/>
                </div>
                <div>
                    <button type="submit">
                        Sign Up
                    </button>
                </div>
            </form>
        );
    }
}

export default SignupForm;