import React from 'react';
import {languages, locales} from 'app/config/i18n/translation';
import {Button} from 'antd';
import {Storage} from 'app/config/i18n/StorageType';
import Flag from 'react-country-flags'
import "./custom-header.scss"

type LocaleItemProps = {
    setLocale: Function;
    locale: string;
}
type LocaleItemState = {
    locale: string
}

class LocaleItem extends React.Component<LocaleItemProps, LocaleItemState> {
    constructor(props) {
        super(props);
        this.state = {locale: "pl"}
    }

    componentDidMount() {
        this.setState({locale: this.props.locale});
    }

    handleLocaleChange = (event) => {
        const {title} = event.currentTarget
        Storage.session.set('locale', title);
        this.setState({locale: title})
        this.props.setLocale(title);
    };

    /**
     * Returns correct country code.
     *
     * @author emil.wozniak.591986@gmail.com
     * @since 11.02.2021
     * @param language other than current user locale.
     * @return country code
     */
    from = (language: string) => language === "en" ? "gb" : "pl"

    render() {
        const {locale} = this.props;
        return (
            <div aria-label="locale" className="locale">
                {Object
                    .keys(languages).length > 1 && locale && locales
                    .filter(it => it !== this.state.locale)
                    .map((it, i) =>
                        <Button
                            className="flag-btn"
                            key={`lang-${i}`}
                            title={it}
                            type="text"
                            onClick={this.handleLocaleChange}>
                            <Flag country={this.from(it)} className="flag"/>
                        </Button>)}
            </div>
        );
    }
}

export default LocaleItem;