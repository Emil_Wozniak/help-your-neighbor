import React from "react";
import LoadingBar from "react-redux-loading-bar";
import {Button, Layout, Menu} from "antd";
import DevRibbon from "app/components/layout/header/dev-ribbon";
import {Link} from "react-router-dom";
import ErrorBoundary from "app/components/error/error-boundry";
import Logo from "app/components/layout/icons/logo";
import ANONYMOUS from "-!svg-react-loader!app/assets/img/announ_user.svg";
import {UserEntity} from "app/entities/user.entity";
import CustomMenuItem from "app/components/layout/header/custom-menu-item";
import {languages, locales} from 'app/config/i18n/translation';
import LocaleItem from "app/components/layout/header/locale-item";

const {Header} = Layout;
const {Item} = Menu;

export type IHeaderProps = {
    user?: UserEntity;
    ribbonEnv?: string;
    isInProduction?: boolean;
    setLocale: Function;
    locale: string;
};

// @ts-ignore
const AUTH = "config.auth-page-link";

const AppHeader = ({ribbonEnv = "dev", isInProduction = false, user = {}, setLocale, locale}: IHeaderProps) => (
    <ErrorBoundary>
        <Header data-testid="header" className="header-color header">
            <LoadingBar className="loading-bar"/>
            <DevRibbon isInProduction={isInProduction} ribbonEnv={ribbonEnv}/>
            <div style={{marginTop: 16}}>
                <Menu mode="horizontal">
                    <Item>
                        <Link to="/">
                            <div
                                className="bg-white radius-circle"
                                style={{height: 64, width: 64, margin: 8}}>
                                <Logo
                                    style={{padding: 2}}
                                    scale={0.2}
                                    className="bg-white radius-circle"/>
                            </div>
                        </Link>
                    </Item>
                    <Item>
                        <LocaleItem setLocale={setLocale} locale={locale} />
                    </Item>
                    <CustomMenuItem>
                        {user && user.imageUrl
                            ? <Link
                                to="/profile"
                                translate="yes"
                                title="Authorization"
                                id={AUTH}
                                aria-label={AUTH}
                                key={AUTH}
                                type="button">
                                <img
                                    src={user.imageUrl}
                                    style={{height: 48, width: 48, color: "#fff"}}
                                    alt={`user-${user.name}-google-image`}/>
                            </Link>
                            : <Link
                                to="/config.auth"
                                translate="yes"
                                title="Authorization"
                                id={AUTH}
                                aria-label={AUTH}
                                key={AUTH}
                                type="button">
                                <ANONYMOUS style={{height: 48, width: 48, color: "#fff"}}/>
                            </Link>
                        }
                    </CustomMenuItem>
                </Menu>
            </div>
        </Header>
    </ErrorBoundary>
);

export default AppHeader;
