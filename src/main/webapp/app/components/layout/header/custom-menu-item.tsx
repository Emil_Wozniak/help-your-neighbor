import React from 'react';

type CustomMenuItemProps = {
    children: JSX.Element | string
}
const CustomMenuItem = ({children}: CustomMenuItemProps) => (
    <li style={{float: "right", listStyleType: "none"}}>
        {children}
    </li>
);

export default CustomMenuItem;