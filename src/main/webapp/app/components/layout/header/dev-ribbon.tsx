import React from "react";

type DevRibbonProps = {
    isInProduction: boolean;
    ribbonEnv: string;
};

const DevRibbon = ({isInProduction, ribbonEnv}: DevRibbonProps) =>
    isInProduction === false
        ? (
            <div className="ribbon dev">
                <a href="">{ribbonEnv}</a>
            </div>
        )
        : <div/>;

export default DevRibbon;
