import React from 'react';

type LogoProps = {
    style?: React.CSSProperties;
    scale: number
    className?: string
}

const dimension = 300;

const size = (scale: number) => ({
    height: `${dimension * scale}px`,
    width: `${dimension * scale}px`,
    backgroundSize: "contain",
});

const Logo = ({scale = 1, className = "", style={}}: LogoProps): JSX.Element => (
    <div className={`${className}`} style={style}>
        <div className={"logo "} style={size(scale)}/>
    </div>
);

export default Logo;