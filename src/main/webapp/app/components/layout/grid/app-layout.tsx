import React from 'react';
import {Layout} from 'antd';

type AppLayoutProps = {
    size?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;
    children: JSX.Element | JSX.Element[]
}

const AppLayout = ({size, children}: AppLayoutProps) => (
    <Layout style={{minHeight: `${size * 10}vh`}}>
        {children}
    </Layout>
);

export default AppLayout;