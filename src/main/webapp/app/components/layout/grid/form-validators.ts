// noinspection JSUnusedGlobalSymbols
export const missingArea = (msg: string | JSX.Element) => [
    {
        required: true,
        message: msg,
        validator: (_, value) =>
            value
                ? Promise.resolve()
                : Promise.reject("can't be empty"),
    },
];

export const missingSwitch = (msg: string | JSX.Element) => [
    {
        required: true,
        message: msg,
    },
];