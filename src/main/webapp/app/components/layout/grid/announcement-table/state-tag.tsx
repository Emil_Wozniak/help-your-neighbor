import {Col, Row, Tag} from 'antd';
import React from 'react';
import {StateTagProps} from "app/components/layout/grid/announcement-table/announcement-columns";


const StateTag = ({name, value}: StateTagProps) => (
    <Row justify="end">
        <Col>{name}:</Col>
        <Col>
            <Tag color={value ? 'volcano' : 'green'} key={`tag-state-${name}`}>
                {value ? "TAK" : "NIE"}
            </Tag>
        </Col>
    </Row>
);

export default StateTag;