import React from "react";
import {ILocation} from "app/entities/location.entity";
import StateTag from "app/components/layout/grid/announcement-table/state-tag";
import {IHashtagModel} from "app/entities/hashtag.entity";
import { Col, Row } from "antd";
import {translate} from "app/config/i18n/Translate";

export type StateTagProps = {
    name: string;
    value: boolean;
}

type Dates = {
    name: string;
    value: Date | string;
}

export type AnnouncementColumn = {
    content?: string;
    dateOfPublication?: Date | string;
    dateEndOfPublication?: Date | string;
    dateSaveAnnouncement?: Date | string;
    dates: Array<Dates>;
    tags?: Array<StateTagProps>;
    locations?: ReadonlyArray<ILocation>;
    hashtags?: ReadonlyArray<IHashtagModel>;
}

export const AnnouncementColumns =(labels: Array<string>) => [
    {
        title: labels[0],
        dataIndex: 'content',
        key: 'content',
        width: '40%',
        render: (text: string) => (<small>{text}</small>)
    },
    {
        title: labels[1],
        dataIndex: 'dates',
        key: 'dates',
        width: '20%',
        render: (dates: Array<Dates>) => dates.map(({name, value}, i) => (
            <Row justify="end" key={`announcement-status-${i}`}>
                <Col>{name}:&nbsp;</Col>
                <Col><b>{value}</b></Col>
            </Row>
        ))
    },
    {
        title: labels[2],
        key: 'tags',
        dataIndex: 'tags',
        width: '20%',
        render: (tags: Array<StateTagProps>) =>
            tags
                .map(({name, value,}, i) => (
                    <StateTag key={`announcement-status-${i}`} name={name} value={value}/>
                ))
    },
    {
        title: labels[3],
        key: 'locations',
        dataIndex: 'locations',
        width: '20%',
        render: (locations: Array<ILocation>) => {
            if (locations) {
                const {address} = locations[0]
                return (
                    <div>
                        <small>{address}</small>
                    </div>
                );
            } else return (<div/>)
        },
    }
];