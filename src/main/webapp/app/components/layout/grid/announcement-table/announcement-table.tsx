import React from 'react';
import {IAnnouncementModel} from "app/entities/announcement.entity";
import {Menu, Table} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {
    AnnouncementColumn,
    AnnouncementColumns
} from "app/components/layout/grid/announcement-table/announcement-columns";
import {translate} from "app/config/i18n/Translate";

interface GetAnnouncementProps {
    announcements: ReadonlyArray<IAnnouncementModel>;
    symbol: IconProp;
}

const AnnouncementTable = ({announcements, ...props}: GetAnnouncementProps) => {
    const icon = <FontAwesomeIcon color="black" icon={props.symbol} style={{marginRight: 4}}/>
    const content = translate("profile.announcements.description")
    const dates = translate("profile.announcements.dates")
    const state = translate("profile.announcements.state")
    const locations = translate("profile.announcements.locations")
    const labels = [content, dates, state, locations]
    return (
        <Menu.SubMenu
            icon={icon}
            title={translate("profile.announcements.subtitle")}
            key="get-announcements"
            {...props}
        >
            <div role="menuitem" style={{height: '100%'}} title="">
                <Table
                    size="middle"
                    dataSource={announcements
                        .concat()
                        .map(({
                                  content,
                                  dateEndOfPublication,
                                  dateOfPublication,
                                  dateSaveAnnouncement,
                                  isPaid,
                                  isDone,
                                  isPro,
                                  locations
                              }): AnnouncementColumn => ({
                            content,
                            dates: [
                                {name: "Start", value: dateOfPublication},
                                {name: "Koniec", value:dateEndOfPublication},
                                {name: "Dodane", value:dateSaveAnnouncement},
                            ],
                            tags: [
                                {name: "Płatne", value: isPaid},
                                {name: "Ukończone", value: isDone},
                                {name: "Pro", value: isPro}
                            ],
                            locations
                        }))}
                    columns={AnnouncementColumns(labels)}/>
            </div>
        </Menu.SubMenu>
    );
};

export default AnnouncementTable;