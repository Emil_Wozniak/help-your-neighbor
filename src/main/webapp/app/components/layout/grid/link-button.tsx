import React, { HTMLAttributes } from 'react';
import {Color} from "app/components/types/Color";
import {AClickEvent} from "app/components/types/a-click-event";

interface LinkButtonProps {
    label: string
    to: string;
    onClick: (x: AClickEvent) => void;
    box?: boolean;
    color?: Color;
    style: React.CSSProperties;
    children?: JSX.Element | JSX.Element [] | string;
}

/**
 * @param label       key of html element and aria-label
 * @param to        destination link
 * @param onClick   event function
 * @param box       if true tag will be shown in box
 * @param color     default txt color black, can take any value of {@link Color}, classNames is in grid.scss
 * @param children  any value
 * @param linkProps     any props from {@link HTMLAnchorElement}
 */
const LinkButton = ({label, to, onClick, box = false, color = "black", children}: LinkButtonProps,
                    linkProps: HTMLAttributes<HTMLAnchorElement>) => (
        <a className={`link-button ${box ? "box" : ""} ${color}`}
           key={`${label}-link-button`}
           aria-label={label}
           {...linkProps}
           href={to}
           onClick={onClick}>
            {children}
        </a>
    );

export default LinkButton;