import React from 'react';
import {missingSwitch} from "app/components/layout/grid/form-validators";
import {SwitchProps} from 'antd/lib/switch';
import {Form} from 'antd';

interface SwitchFormItemProps extends SwitchProps {
    name: string;
    label: string;
    isChecked?: any
    children: JSX.Element | string
}

const ValidateSwitch = ({name, label, children, isChecked}: SwitchFormItemProps) => (
    <Form.Item
        wrapperCol={{span: 16}}
        labelCol={{span: 8}}
        name={name}
        label={label}
        rules={missingSwitch("is missing")}
        hasFeedback
        style={{width: 'calc(90% - 8px)'}}
        initialValue={isChecked}
    >
        {children}
    </Form.Item>
);

export default ValidateSwitch;