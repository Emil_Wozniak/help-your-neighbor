import {Form} from 'antd';
import {InputProps} from 'antd/lib/input';
import React from 'react';
import {missingArea} from "app/components/layout/grid/form-validators";
import {ColProps} from 'antd/lib/col';

interface ValidateFormItemProps extends InputProps {
    name: string;
    label: string;
    errorMsg?: string | JSX.Element
    InitialValue?: any
    children: JSX.Element | string
    wrapperCol?: ColProps
    labelCol?: ColProps;
}

const ValidateInput = ({
                           name,
                           label,
                           errorMsg,
                           children,
                           InitialValue,
                           wrapperCol,
                           labelCol
                       }: ValidateFormItemProps) => {
    const error = errorMsg ? errorMsg : `Nie może być pusty ${label}`
    return (
        <Form.Item
            wrapperCol={wrapperCol ? wrapperCol : {span: 16}}
            labelCol={labelCol ? labelCol : {span: 8}}
            name={name}
            label={label}
            rules={missingArea(error)}
            hasFeedback
            style={{width: "calc(90% - 8px)"}}
            initialValue={InitialValue}>
            {children}
        </Form.Item>
    );
};

export default ValidateInput;