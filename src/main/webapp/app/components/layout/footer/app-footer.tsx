import '../../../styles/footer.scss';
import React from 'react';
import {Layout} from "antd";

const {Footer} = Layout;

const AppFooter = () => (
    <Footer>
        This is your footer
    </Footer>
);

export default AppFooter;
