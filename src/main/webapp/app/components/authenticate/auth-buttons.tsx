import React from 'react';
import CustomButton from "app/components/layout/grid/custom-button";

type AuthButtonsProps = {
    register: boolean;
    change: Function;
}

const setColor = (isTrue) => isTrue ? "#fff" : "#000";

export const getBtnStyle = (value: boolean) => ({background: setColor(value), color: setColor(!value)});

const AuthButtons = ({register, change}: AuthButtonsProps) => (
    <>
        <CustomButton
            style={getBtnStyle(register)}
            onClick={change(false)}
            disabled={!register}>
            Login
        </CustomButton>
        <CustomButton
            style={getBtnStyle(!register)}
            onClick={change(true)}
            disabled={register}>
            Register
        </CustomButton>
    </>
);

export default AuthButtons;