import "./App.scss";
import 'react-toastify/dist/ReactToastify.css';
import React, {useEffect} from "react";
import {BrowserRouter, Router} from "react-router-dom";
import AppLayout from "app/components/layout/grid/app-layout";
import AppHeader from "app/components/layout/header/app-header";
import AppFooter from "app/components/layout/footer/app-footer";
import Routes from "./routes";
import {connect} from "react-redux";
import {Layout} from "antd";
import {getProfile} from "app/components/reducers/application-profile";
import {getUser} from "app/components/reducers/authentication";
import {setLocale} from 'app/components/reducers/locale';
import {IRootState} from "app/components/reducers";
import history from "app/config/history"
import { ToastContainer } from 'react-toastify';

export interface IAppProps extends StateProps, DispatchProps {
}

const App = (props: IAppProps) => {
    useEffect(() => {
        props.getUser();
        props.getProfile();
    }, []);

    const {isProd, ribbonEnv, user} = props;
    return (
        <BrowserRouter basename="/">
            <Router history={history} >
            <AppLayout size={10}>
                <Layout className="site-layout">
                    <AppHeader
                        locale={props.locale}
                        setLocale={props.setLocale}
                        isInProduction={isProd}
                        ribbonEnv={ribbonEnv}
                        user={user}/>
                    <ToastContainer />
                    <AppLayout size={8}>
                        <Routes/>
                    </AppLayout>
                    <AppFooter/>
                </Layout>
            </AppLayout>
            </Router>
        </BrowserRouter>
    );
};

const mapStateToProps = ({applicationProfile, authentication, locale}: IRootState) => ({
    locale: locale.currentLocale,
    isProd: applicationProfile.inProduction,
    ribbonEnv: applicationProfile.ribbonEnv,
    isAuth: authentication.isAuthenticated,
    user: authentication.account
});

const mapDispatchToProps = {getUser, getProfile, setLocale};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(App);
