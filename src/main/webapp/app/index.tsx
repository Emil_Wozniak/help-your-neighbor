import React from "react";
import "./index.css";
import DevTools from "./config/devtools";
import ReactDOM from "react-dom";
import AppComponent from "./App";
import initStore from "./config/store";
import setupAxiosInterceptors from './config/axios-interceptor';
import {clearAuthentication} from "app/components/reducers/authentication";
import {registerLocale} from './config/i18n/translation';
import {Provider} from "react-redux";
import {bindActionCreators} from 'redux';
import {CookiesProvider} from 'react-cookie';
import {loadIcons} from "app/config/icons";

const devTools = process.env.NODE_ENV === "development" ? <DevTools/> : null;
const store = initStore();
const actions = bindActionCreators({clearAuthentication}, store.dispatch);
setupAxiosInterceptors(() => actions.clearAuthentication('login.error.unauthorized'));
registerLocale(store);
loadIcons();

const rootEl = document.getElementById("root");

const provider = Component => (
    <Provider store={store}>
        <CookiesProvider>
            <div>
                {devTools}
                <Component/>
            </div>
        </CookiesProvider>
    </Provider>);

const render = Component => ReactDOM.render(provider(Component), rootEl);

render(AppComponent);
