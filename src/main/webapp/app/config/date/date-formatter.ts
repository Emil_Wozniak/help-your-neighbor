export const DateFormat = 'YYYY-MM-DD'

export const toLocalDate = (date: Date | string): string => reformatDate(date)

const reformatDate = (date: Date | string): string =>
    new Date(date)
        .toLocaleDateString()
        .substring(0, 10)