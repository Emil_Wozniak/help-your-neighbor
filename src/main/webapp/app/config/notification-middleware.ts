import {toast, ToastOptions} from 'react-toastify';

// noinspection TypeScriptValidateJSTypes
export const Success = (msg: string, options?: ToastOptions) => toast.success(
    msg,
    options ? options : {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });

export const Error = (msg: string, options?: ToastOptions) => toast.error(msg,
    options ? options : {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    })

export default () => next => action => {
    /**
     * The notification middleware serves to dispatch the initial pending promise to
     * the promise middleware, but adds a `then` and `catch.
     */
    try {
        const promise = next(action)
        if (promise.payload && promise.type) {
            const {payload, type} = promise;
            const {data, status} = payload;
            if (type.includes("CREATE") || type.includes("UPDATE")) {
                if (status > 199 && status < 300) {
                    const entity = type.split("_")[1].split("_")[0].toLowerCase()
                    if (entity === "announcement") {
                        Success(`Utworzono: ${entity}`)
                    } else if (!data.hasOwnProperty("email")) {
                        // success(data.toString());
                    }
                }
            }
        }
        return next(action)
        // eslint-disable-next-line no-empty
    } catch (error) {
    }
};
