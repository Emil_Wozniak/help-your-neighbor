import React from 'react';
import {Helmet} from "react-helmet";

type SocialMetaProps = {
    clientId: string
}
const SocialMeta = ({clientId}: SocialMetaProps) => {
    const element = document.createElement("meta");
    element.setAttribute('name', "google-sigin-client_id");
    element.setAttribute('content', `${clientId}`)
    return (
        <Helmet>
        </Helmet>
    );
};

export default SocialMeta;