import axios from 'axios';
import {toast} from 'react-toastify';

const TIMEOUT = 60 * 1000;

axios.defaults.timeout = TIMEOUT;
axios.defaults.xsrfCookieName = "XSRF-TOKEN"
axios.defaults.xsrfHeaderName = "X-XSRF-TOKEN"
// axios.defaults.baseURL = "http://localhost:8088"

const setupAxiosInterceptors = (_) => {
    const onRequestSuccess = config => {
        return config;
    };
    const onResponseSuccess = response => response;
    const onResponseError = error => {
        if (error.response && error.response.status) {
            const {status} = error.response;
            const location = window.location;
            if (status === 403 || status === 401 && location.pathname != "/") {
                toast.error('Unauthorized', {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
            return Promise.reject(error.response);
        }
    };
    axios.interceptors.request.use(onRequestSuccess);
    axios.interceptors.response.use(onResponseSuccess, onResponseError);
};

export default setupAxiosInterceptors;
