import {Dispatch, SetStateAction, useMemo, useState} from 'react';

/**
 * Replacement for useState which depends on prop value and avoids unnecessary re-renders.
 */
export function useStateBasedOnProps<T>(propValue: T): [T, Dispatch<SetStateAction<T>>] {
    const [, update] = useState(false);

    const [state, setState] = useMemo(() => {
        const state = {} as { value?: T; prevPropValue?: T };
        return [
            state,
            (value: SetStateAction<T>) => {
                if (value instanceof Function) {
                    state.value = value(state.value!);
                } else {
                    state.value = value;
                }
                update((tick) => !tick);
            }
        ];
    }, [update]);

    if (state.prevPropValue !== propValue) {
        state.prevPropValue = propValue;
        state.value = propValue;
    }

    return [state.value!, setState];
}