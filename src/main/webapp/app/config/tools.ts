/**
 * Check if the passed object is a promise
 * @param value the object to check
 */
export declare const isPromise: (value: any) => boolean;