import {Storage} from 'app/config/i18n/StorageType';
import TranslatorContext from 'app/config/i18n/TranslatorContext';

import { setLocale } from 'app/components/reducers/locale';

TranslatorContext.setDefaultLocale('pl');
TranslatorContext.setRenderInnerTextForMissingKeys(false);


export const languages = {
    pl: { name: 'pl' },
    en: { name: 'en' }
};

export const locales = Object.keys(languages).sort();

export const registerLocale = (store) => {
    store.dispatch(setLocale(Storage.session.get('locale', 'pl')));
};
