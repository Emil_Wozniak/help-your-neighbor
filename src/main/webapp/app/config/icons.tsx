import {library} from '@fortawesome/fontawesome-svg-core';
import {faGoogle} from '@fortawesome/free-brands-svg-icons/faGoogle';
import {faFacebook} from '@fortawesome/free-brands-svg-icons/faFacebook';
import {faGithub} from '@fortawesome/free-brands-svg-icons/faGithub';
import {faCog} from "@fortawesome/free-solid-svg-icons/faCog"
import {faUserCircle} from "@fortawesome/free-solid-svg-icons/faUserCircle"
import {faSearch} from "@fortawesome/free-solid-svg-icons/faSearch"
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons/faSignOutAlt"
import {faUserSlash} from "@fortawesome/free-solid-svg-icons/faUserSlash"
import {faCommentAlt} from "@fortawesome/free-solid-svg-icons/faCommentAlt"
import {faUserEdit} from "@fortawesome/free-solid-svg-icons/faUserEdit"
import {faPhone} from "@fortawesome/free-solid-svg-icons/faPhone"
import {faBullhorn} from "@fortawesome/free-solid-svg-icons/faBullhorn"
import {faScroll} from "@fortawesome/free-solid-svg-icons/faScroll"
import {faAngleRight} from "@fortawesome/free-solid-svg-icons/faAngleRight"
import {faAngleLeft} from "@fortawesome/free-solid-svg-icons/faAngleLeft"
import {faMailBulk} from "@fortawesome/free-solid-svg-icons/faMailBulk"
import {faEnvelope} from "@fortawesome/free-solid-svg-icons/faEnvelope"
import {faMoneyBill} from "@fortawesome/free-solid-svg-icons/faMoneyBill"

export const loadIcons = () => {
    library.add(
        faUserCircle,
        faUserEdit,
        faSignOutAlt,
        faUserSlash,
        faSearch,
        faCog,
        faCommentAlt,
        faMailBulk,
        faGoogle,
        faFacebook,
        faGithub,
        faPhone,
        faBullhorn,
        faScroll,
        faAngleRight,
        faAngleLeft,
        faEnvelope,
        faMoneyBill
    )
}