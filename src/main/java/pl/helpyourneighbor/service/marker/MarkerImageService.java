package pl.helpyourneighbor.service.marker;

import pl.helpyourneighbor.model.marker.MarkerImage;
import pl.helpyourneighbor.service.types.AllGetable;

public interface MarkerImageService extends AllGetable<MarkerImage> {
}
