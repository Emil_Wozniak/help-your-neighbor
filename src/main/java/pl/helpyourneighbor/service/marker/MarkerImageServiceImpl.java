package pl.helpyourneighbor.service.marker;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.helpyourneighbor.model.marker.MarkerImage;
import pl.helpyourneighbor.repo.MarkerImageRepository;

import java.util.Collection;

@Slf4j
@Value
@NonFinal
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service(value = "markerImageService")
public class MarkerImageServiceImpl implements MarkerImageService {

    MarkerImageRepository images;

    @Override
    public Collection<MarkerImage> getAll() {
        return images.findAll();
    }
}
