package pl.helpyourneighbor.service.hashtag;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.helpyourneighbor.model.announcement.Hashtag;
import pl.helpyourneighbor.repo.HashtagRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static pl.helpyourneighbor.config.constants.EntityConstant.*;

@Slf4j
@Value
@NonFinal
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service(value = "hashtagService")
public class HashtagServiceImpl implements HashtagService {

    private final static String ENTITY_TYPE = Hashtag.class.getSimpleName();

    HashtagRepository repository;

    @Override
    public Collection<Hashtag> getAll() {
        log.info(PERSIST_ALL_MSG, ENTITY_TYPE);
        return this.repository.findAll();
    }

    @Override
    public Optional<Hashtag> getOne(@NonNull Long id) {
        log.info(PERSIST_ONE_MSG, ENTITY_TYPE, id);
        return this.repository.findById(id);
    }

    @Override
    @Transactional
    public Hashtag save(@NonNull Hashtag entity) {
        log.info(PERSIST_SAVE_MSG, ENTITY_TYPE, entity.toString());
        return this.repository.save(entity);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        this.repository.findById(id)
                .ifPresent(entity -> {
                    log.info(PERSIST_DELETE_MSG, ENTITY_TYPE, entity.toString());
                    this.repository.delete(entity);
                });
    }
}
