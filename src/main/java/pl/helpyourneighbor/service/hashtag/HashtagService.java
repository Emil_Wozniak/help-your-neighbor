package pl.helpyourneighbor.service.hashtag;

import pl.helpyourneighbor.model.announcement.Hashtag;
import pl.helpyourneighbor.service.types.ApiService;

public interface HashtagService extends ApiService<Hashtag, Long> {

}
