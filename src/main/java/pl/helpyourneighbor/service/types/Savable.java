package pl.helpyourneighbor.service.types;

public interface Savable<T> {

    T save(T entity);
}
