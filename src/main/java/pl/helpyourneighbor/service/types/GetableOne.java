package pl.helpyourneighbor.service.types;

import java.util.Optional;

/**
 * @param <T>  entity type
 * @param <ID> id type
 */
public interface GetableOne<T, ID> {
    Optional<T> getOne(ID id);
}
