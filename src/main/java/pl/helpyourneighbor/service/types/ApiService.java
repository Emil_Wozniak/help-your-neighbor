package pl.helpyourneighbor.service.types;

import java.util.Collection;

public interface ApiService<T, ID> extends Savable<T>, GetableOne<T, ID>, Deletable<ID> {

    Collection<T> getAll();
}
