package pl.helpyourneighbor.service.types;

public interface Deletable<ID> {

    void delete(ID id);
}
