package pl.helpyourneighbor.service.types;

import org.springframework.security.oauth2.core.user.OAuth2User;

public interface SavePrincipal<T> {

    /**
     *
     * @param entity User instance
     * @param <P> user principals
     * @return instance of user DTO
     */
    <P extends OAuth2User>T save(P entity);
}
