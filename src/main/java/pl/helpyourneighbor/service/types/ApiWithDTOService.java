package pl.helpyourneighbor.service.types;

import pl.helpyourneighbor.model.user.User;

import javax.persistence.UniqueConstraint;
import java.util.Collection;

/**
 * @param <T>   entity type
 * @param <DTO> entity dto
 * @param <ID>  entity identifier, can be any type. Prefer types are Long when
 *              interface uses database primary key or String when interface uses
 *              {@link UniqueConstraint}.
 * @see User    type using String value of {@link User#getEmail()} as UniqueConstraint
 */
public interface ApiWithDTOService<T, DTO, ID> extends DtoSavable<T, DTO>, GetableOne<T, ID>, Deletable<ID> {

    Collection<T> getAll();
}
