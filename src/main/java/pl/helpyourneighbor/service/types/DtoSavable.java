package pl.helpyourneighbor.service.types;

public interface DtoSavable<T, DTO> {

    T save(DTO entity);
}
