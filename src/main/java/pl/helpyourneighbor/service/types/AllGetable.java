package pl.helpyourneighbor.service.types;

import java.util.Collection;

/**
 * @param <T> entity type
 */
public interface AllGetable<T> {

    Collection<T> getAll();
}
