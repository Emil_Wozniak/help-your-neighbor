package pl.helpyourneighbor.service.announcement;

import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.announcement.Location;
import pl.helpyourneighbor.repo.LocationRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import static pl.helpyourneighbor.config.constants.EntityConstant.*;

@Value
@Slf4j
@NonFinal
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service(value = "locationService")
public class LocationServiceImpl implements LocationService {

    private final static String ENTITY_TYPE = Location.class.getSimpleName();

    LocationRepository locations;

    @Override
    public Collection<Location> getAll() {
        log.info(PERSIST_ALL_MSG, ENTITY_TYPE);
        return this.locations.findAll();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        log.info(PERSIST_DELETE_MSG, ENTITY_TYPE, id);
        this.locations.findById(id).ifPresent(locations::delete);
    }

    @Override
    public Optional<Location> getOne(Long id) {
        log.info(PERSIST_ONE_MSG, ENTITY_TYPE, id);
        return this.locations.findById(id);
    }

    @Override
    @Transactional
    public Location save(Location entity) {
        final var optional = this.locations.findById(entity.getId());
        if (optional.isEmpty()) {
            log.info(PERSIST_SAVE_MSG, ENTITY_TYPE, entity.toString());
            return locations.save(entity);
        } else return optional.get();
    }

    /**
     * Resolves if {@link Location} table already contains object with the same localization
     * coordinates. If does will add reference of this object to announcement.
     *
     * @param announcement new entity of {@link Announcement}
     */
    public void resolveLocalization(Announcement announcement) {
        Stream
                .ofAll(announcement.getLocations())
                .map(it -> this.locations.findByLongitudeAndLatitude(it.getLongitude(), it.getLatitude()))
                .forEach(it -> it.ifPresent(location -> announcement.setLocations(Set.of(location))));
    }
}
