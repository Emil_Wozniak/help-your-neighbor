package pl.helpyourneighbor.service.announcement;

import io.vavr.control.Try;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.repo.AnnouncementRepository;
import pl.helpyourneighbor.service.user.UserService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static pl.helpyourneighbor.config.constants.AppConstants.*;

@Slf4j
@Value
@NonFinal
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service(value = "announcementRequestService")
public class AnnouncementServiceImpl implements AnnouncementService {

    private final static String ENTITY_TYPE = Announcement.class.getSimpleName();

    UserService users;
    AnnouncementRepository announcements;
    LocationService locations;

    @Override
    public Collection<Announcement> getAll() {
        return Try
                // TODO find user by ID
                .of(() -> this.announcements.findAnnouncementByUserEmail(users.getEmail()))
                .onSuccess(it -> log.info("Fetched entities: {}", it))
                .onFailure(error -> log.error(error.getMessage()))
                .getOrElse(List.of());
    }

    @Override
    public Optional<Announcement> getOne(@NonNull Long id) {
        return Try
                .of(() -> this.announcements.findById(id))
                .onFailure(error -> log.error(error.getMessage()))
                .onSuccess(it -> log.info(PERSIST_ONE_MSG, ENTITY_TYPE, id))
                .getOrElse(Optional.empty());
    }

    @Override
    @Transactional
    public Announcement save(@NonNull Announcement entity) {
        return Try
                .of(() -> this.announcements.save(entity))
                .onFailure(error -> log.error(error.getMessage()))
                .onSuccess(it -> log.info(PERSIST_SAVE_MSG, ENTITY_TYPE, it.toString()))
                .get();
    }

    @Override
    @Transactional
    public void delete(@NonNull Long id) {
        this.announcements.findById(id)
                .ifPresent(entity -> {
                    log.info(PERSIST_DELETE_MSG, ENTITY_TYPE, entity.toString());
                    this.announcements.delete(entity);
                });
    }
}

