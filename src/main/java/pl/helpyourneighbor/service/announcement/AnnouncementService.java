package pl.helpyourneighbor.service.announcement;

import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.service.types.ApiService;

public interface AnnouncementService extends ApiService<Announcement, Long> {

}
