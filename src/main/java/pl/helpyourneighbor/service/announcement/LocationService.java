package pl.helpyourneighbor.service.announcement;

import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.announcement.Location;
import pl.helpyourneighbor.service.types.ApiService;

public interface LocationService extends ApiService<Location, Long> {

    void resolveLocalization(Announcement announcement);
}
