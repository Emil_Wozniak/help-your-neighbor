package pl.helpyourneighbor.service.user;

import pl.helpyourneighbor.model.user.Authority;
import pl.helpyourneighbor.service.types.Deletable;
import pl.helpyourneighbor.service.types.AllGetable;
import pl.helpyourneighbor.service.types.GetableOne;
import pl.helpyourneighbor.service.types.Savable;

public interface AuthorityService extends GetableOne<Authority, String>, Deletable<Authority>, Savable<Authority>, AllGetable<Authority> {
}
