package pl.helpyourneighbor.service.user;

import pl.helpyourneighbor.model.announcement.Announcement;

public interface CreateAnnouncement {

    boolean createAnnouncement(Announcement announcement);

}
