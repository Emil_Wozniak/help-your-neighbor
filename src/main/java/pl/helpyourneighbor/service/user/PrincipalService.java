package pl.helpyourneighbor.service.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.security.oauth2.core.user.OAuth2User;
import pl.helpyourneighbor.model.user.User;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import static pl.helpyourneighbor.config.constants.UserConstants.EMAIL;

public interface PrincipalService {

    /**
     * @return {@link User#getEmail()} from SecurityContextHolder
     */
    @Nullable
    default String getEmail() {
        return getPrincipal().getAttribute(EMAIL);
    }

    default OAuth2User getPrincipal() {
        return (OAuth2User) getContext()
                .getAuthentication()
                .getPrincipal();
    }
}
