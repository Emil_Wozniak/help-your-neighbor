package pl.helpyourneighbor.service.user;

import io.vavr.control.Try;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.user.User;
import pl.helpyourneighbor.model.user.UserDTO;
import pl.helpyourneighbor.repo.AnnouncementRepository;
import pl.helpyourneighbor.repo.UserRepository;
import pl.helpyourneighbor.service.announcement.LocationService;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static pl.helpyourneighbor.config.constants.EntityConstant.*;
import static pl.helpyourneighbor.model.user.UserDetails.createDefaultDetails;

@Slf4j
@Value
@Service
@NonFinal
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class LocalUserService implements UserService {

    private final static String ENTITY_TYPE = User.class.getSimpleName();

    UserRepository repository;
    AnnouncementRepository announcements;
    LocationService locations;

    @Override
    public Collection<User> getAll() {
        return Try
                .of(repository::findAll)
                .onSuccess(it -> log.info(PERSIST_ALL_MSG, ENTITY_TYPE))
                .onFailure(error -> log.error(error.getMessage()))
                .getOrElse(List.of());
    }

    /**
     * @param email entity property.
     * @return entity if email matches stored entity email,
     * or EntityNotFoundException in other case.
     */
    @Override
    public Optional<User> getOne(@NonNull String email) {
        log.info(PERSIST_ONE_MSG, ENTITY_TYPE, email);
        return this.repository.findLocalByEmail(email);
    }

    /**
     * @param entity entity to save.
     * @return saved entity if valid,
     * or throws IllegalArgumentException in other case.
     * @implNote userAttributes implementation is not correct a proper details {@link OAuth2User#getAttributes()}
     */
    @Override
    @Transactional
    public User save(@NonNull UserDTO entity) {
        Map<String, Object> userAttributes = Map.of(); // TODO: this need to be implemented.
        return Try
                .of(() -> User.createUserLocal(
                        entity.getEmail(),
                        entity.getUserAuthorities(),
                        createDefaultDetails(entity),
                        userAttributes
                ))
                .map(this.repository::save)
                .onSuccess(it -> log.info(PERSIST_SAVE_MSG, ENTITY_TYPE, entity.toString()))
                .onFailure(error -> log.error(error.getMessage()))
                .get();
    }

    /**
     * Method deletes user from database if and only if entity with <b>email</b> value
     * exists in a {@code hyn_user} table. In any other case will throw and catch NullPointerException and create
     * logs with a proper message.
     *
     * @param email email property of the entity to remove.
     */
    @Override
    @Transactional
    public void delete(@NonNull String email) {
        Try.of(() -> this.repository.findLocalByEmail(email))
                .map(Optional::get)
                .peek(this.repository::delete)
                .onSuccess(user -> log.info(PERSIST_DELETE_MSG, ENTITY_TYPE, user))
                .onFailure(error -> log.error("No user with email: {}", email));
    }

    /**
     * @param user received UserSocial object
     * @return user entity
     */
    @Override
    @Transactional
    public UserDTO activate(User user) {
        return this.repository
                .findSocialByEmail(user.getEmail())
                .map(userSocial -> activateOrGet(userSocial, user))
                .orElseGet(() -> new UserDTO(user));
    }

    /**
     * Method binds announcement and user in both sides and stores user into database with all dependent entities.
     *
     * @param announcement new announcement entity
     * @return true as response of correct end of transaction, or false in any other case.
     */
    @Override
    @Modifying
    @Transactional
    public boolean createAnnouncement(Announcement announcement) {
        return Try
                .of(this::getEmail)
                .map(this.repository::findSocialByEmailEagerly)
                .map(Optional::get)
                .map(user -> {
                    // TODO does this function should check if announcement already exists
                    announcement.setUser(user);
                    this.locations.resolveLocalization(announcement);
                    user.addAnnouncement(announcement);
                    this.repository.save(user);
                    return true;
                })
                .onSuccess(it -> log.info("User save announcement: {}", announcement))
                .onFailure(error -> log.error(error.getMessage()))
                .getOrElse(false);
    }

    protected UserDTO activateOrGet(User social, User user) {
        return Try
                .of(user::getPhone)
                .map(phone -> {
                    val announcements = this.announcements.findByUser(social);
                    social.setPhone(phone);
                    social.setActivated(true);
                    social.setAnnouncements(announcements);
                    val email = social.getEmail();
                    return this.repository.activateUser(email, phone);
                })
                .map(result -> new UserDTO(result, this.getPrincipal()))
                .onSuccess(dto -> log.info("User activated on number {}", dto.getPhone()))
                .onFailure(error -> log.error("User phone number not found"))
                .getOrElse(new UserDTO(social));
    }
}
