package pl.helpyourneighbor.service.user;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.helpyourneighbor.model.user.Authority;
import pl.helpyourneighbor.repo.AuthorityRepository;

import java.util.Collection;
import java.util.Optional;

import static pl.helpyourneighbor.config.constants.EntityConstant.*;

@Slf4j
@Value
@NonFinal
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service(value = "authorityServiceImpl")
public class AuthorityServiceImpl implements AuthorityService {

    private final static String ENTITY_TYPE = Authority.class.getSimpleName();

    AuthorityRepository repository;

    @Override
    @Transactional
    public void delete(Authority entity) {
        log.info(PERSIST_ALL_MSG, ENTITY_TYPE);
        this.repository
                .findById(entity.getName())
                .ifPresent(this.repository::delete);
    }

    @Override
    public Optional<Authority> getOne(String name) {
        log.info(PERSIST_DELETE_MSG, ENTITY_TYPE, name);
        return this.repository.findById(name);
    }

    @Override
    public Collection<Authority> getAll() {
        log.info(PERSIST_ALL_MSG, ENTITY_TYPE);
        return this.repository.findAll();
    }

    /**
     * If authority is present in database it will return present instance,
     * otherwise it will store entity first and then return it.
     *
     * @param entity Authority instance
     * @return entity
     */
    @Override
    @Transactional
    public Authority save(Authority entity) {
        log.info(PERSIST_SAVE_MSG, ENTITY_TYPE, entity.toString());
        return this.repository.findById(entity.getName()).orElse(this.repository.save(entity));
    }
}
