package pl.helpyourneighbor.service.user;

import pl.helpyourneighbor.model.user.User;
import pl.helpyourneighbor.model.user.UserDTO;
import pl.helpyourneighbor.service.types.ApiWithDTOService;

import java.util.Collection;
import java.util.Optional;

/**
 * User service facade.
 * <p>
 * Type String is an identifier because UniqueConstraint for {@link User} type is an email.
 *
 * @see User entity type
 * @see UserDTO entity dto
 */
public interface UserService extends PrincipalService, ApiWithDTOService<User, UserDTO, String>, CreateAnnouncement {

    Collection<User> getAll();

    Optional<User> getOne(String id);

    User save(UserDTO user);

    void delete(String email);

    UserDTO activate(User user);

}
