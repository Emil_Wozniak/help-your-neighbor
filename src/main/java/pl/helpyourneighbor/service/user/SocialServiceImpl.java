package pl.helpyourneighbor.service.user;

import io.vavr.collection.Stream;
import io.vavr.control.Try;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.auth.UserPrincipal;
import pl.helpyourneighbor.model.provider.AuthProvider;
import pl.helpyourneighbor.model.user.Authority;
import pl.helpyourneighbor.model.user.User;
import pl.helpyourneighbor.model.user.UserDTO;
import pl.helpyourneighbor.repo.AuthorityRepository;
import pl.helpyourneighbor.repo.UserRepository;
import pl.helpyourneighbor.service.announcement.LocationService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import static pl.helpyourneighbor.config.constants.AppConstants.NO_PRINCIPALS;
import static pl.helpyourneighbor.config.constants.AppConstants.USER_CREATED;
import static pl.helpyourneighbor.config.constants.UserConstants.EMAIL;
import static pl.helpyourneighbor.model.provider.AuthProvider.getProvider;
import static pl.helpyourneighbor.model.user.Authority.createFromOAuthUser;

@Slf4j
@Value
@Service
@NonFinal
@RequiredArgsConstructor
public class SocialServiceImpl implements SocialService {

    UserRepository repository;
    AuthorityRepository authorities;
    LocationService locationService;

    public OAuth2User getCurrentUser() {
        return Try
                .of(() -> getContext().getAuthentication().getPrincipal())
                .map(this::convertToPrincipal)
                .onFailure(error -> log.error(NO_PRINCIPALS, error.getMessage()))
                .get();
    }

    @Override
    public Optional<User> findUserByEmail(@NonNull String email) {
        return this.repository.findSocialByEmail(email);
    }

    @Override
    @Transactional
    public UserDTO save(OAuth2User user) {
        return Try
                .of(() -> createUser(user))
                .peek(addAuthorities())
                .map(this.repository::save)
                .onSuccess(it -> log.debug(USER_CREATED, it))
                .map(it -> new UserDTO(user))
                .onFailure(error -> log.error(error.getMessage()))
                .get();
    }

    @NotNull
    private User createUser(OAuth2User user) {
        val authorities = getAuthorities(user);
        val provider = getProvider(AuthProvider.from(user));
        val email = (String) user.getAttribute(EMAIL);
        return User.createSocialUser(user.getAttributes(), email, provider, authorities);
    }

    @Override
    public void delete(UserDTO entity) {
        this.repository
                .findSocialByEmail(entity.getEmail())
                .ifPresent(this.repository::delete);
    }

    /**
     * Asserts that <code>user</code> email value does not exist in database. If does it creates new user and
     * returns those user details, otherwise returns user details directly from active user.
     *
     * @param user user data fetched from OAuth2 client.
     * @return Map of user principals attributes.
     */
    public Map<String, Object> getUserDetails(@NotNull OAuth2User user) {
        val email = (String) user.getAttribute(EMAIL);
        val socialByEmail = this.repository.findSocialByEmail(email);
        return socialByEmail.isEmpty()
                ? this.save(user).getAttributes()
                : getActiveUser(socialByEmail.get(), user).getAttributes();
    }

    protected UserDTO getActiveUser(@NonNull User social, @NonNull OAuth2User user) {
        return new UserDTO(social, user);
    }

    /**
     * Binds announcement and user in both sides and stores user into database with all dependent entities.
     *
     * @param announcement new announcement entity
     * @return true as response of correct end of transaction, or false in any other case.
     */
    @Override
    @Modifying
    @Transactional
    public boolean createAnnouncement(Announcement announcement) {
        return Try
                .of(this::getEmail)
                .map(this.repository::findSocialByEmailEagerly)
                .map(Optional::get)
                .map(user -> {
                    // TODO does this function should check if announcement already exists
                    announcement.setUser(user);
                    locationService.resolveLocalization(announcement);
                    user.addAnnouncement(announcement);
                    this.repository.save(user);
                    return true;
                })
                .onSuccess(it -> log.info("User save announcement: {}", announcement))
                .onFailure(error -> log.error(error.getMessage()))
                .getOrElse(false);
    }

    /**
     * Method checks if {@param object} <b>is not</b> instanceof String, if does it cast object to {@link OAuth2User}
     * type. In other case return empty UserPrincipal.
     * <p>
     * Empty UserPrincipal should not give any principals to access resources.
     *
     * @param object from {@link SecurityContextHolder}
     * @return user principal
     */
    private OAuth2User convertToPrincipal(Object object) {
        return !(object instanceof String)
                ? (OAuth2User) object
                : UserPrincipal.empty();
    }

    protected Consumer<@NotNull User> addAuthorities() {
        return userSocial -> Stream
                .ofAll(authorities.findAll())
                .map(Authority::getName)
                .forEach(name -> {
                    val roles = Stream
                            .ofAll(userSocial.getAuthorities())
                            .filter(role -> role.getName().equals(name))
                            .toJavaSet();
                    userSocial.setAuthorities(roles);
                });
    }

    /**
     * Methods creates {@link Authority} from user, then search them in {@link Authority} table in database,
     * each present value then is added to return set of authorities.
     *
     * @param user {@link OAuth2User} instance
     * @return unique set of authorities.
     */
    @NotNull
    private Set<Authority> getAuthorities(OAuth2User user) {
        Set<Authority> authorities = new HashSet<>();
        createFromOAuthUser(user).forEach(authority -> this.authorities
                .findById(authority.getName())
                .ifPresent(authorities::add));
        return authorities;
    }
}
