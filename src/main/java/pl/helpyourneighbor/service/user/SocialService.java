package pl.helpyourneighbor.service.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.oauth2.core.user.OAuth2User;
import pl.helpyourneighbor.model.user.User;
import pl.helpyourneighbor.model.user.UserDTO;
import pl.helpyourneighbor.service.types.Deletable;
import pl.helpyourneighbor.service.types.SavePrincipal;

import java.util.Map;
import java.util.Optional;

public interface SocialService
        extends PrincipalService, SavePrincipal<UserDTO>, Deletable<UserDTO>, CreateAnnouncement {

    /**
     * @return Current user from SecurityContextHolder.
     */
    OAuth2User getCurrentUser();

    /**
     * @implSpec
     * @param email {@link User#getEmail()}
     * @return Optional holding user, if user with that {@code email} exists in database or empty optional in other case.
     */
    Optional<User> findUserByEmail(@NonNull String email);

    /**
     * @implSpec
     * @param user A representation of a user {@code Principal}
     * @return user {@link OAuth2User#getAttributes()} which holds user details.
     */
    Map<String, Object> getUserDetails(@NotNull OAuth2User user);

}
