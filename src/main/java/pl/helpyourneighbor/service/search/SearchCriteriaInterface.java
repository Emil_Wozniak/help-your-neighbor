package pl.helpyourneighbor.service.search;

import pl.helpyourneighbor.model.search.SearchCriteria;

public interface SearchCriteriaInterface {

    String searchByAddress(SearchCriteria search);

}
