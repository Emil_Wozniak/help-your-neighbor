package pl.helpyourneighbor.service.search;

import org.springframework.data.jpa.repository.Query;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.search.SearchCriteria;

import java.util.List;

public interface AnnouncementSearchService {

    @Query("SELECT entity " +
            "FROM Announcement as entity " +
            "LEFT JOIN FETCH entity.locations " +
            "WHERE entity.locations.city =:city " +
            "AND entity.locations.longitude =:criteria "
    )
    List<Announcement> findAnnouncementByCriteria(SearchCriteria criteria);

}
