package pl.helpyourneighbor.service.search;

import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.val;
import org.springframework.stereotype.Service;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.announcement.Hashtag;
import pl.helpyourneighbor.model.announcement.Location;
import pl.helpyourneighbor.model.search.SearchCriteria;
import pl.helpyourneighbor.service.hashtag.HashtagService;
import pl.helpyourneighbor.service.user.SocialService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Value
@Service
@NonFinal
@RequiredArgsConstructor
public class AnnouncementSearchMockImpl implements AnnouncementSearchService {

    SocialService principals;
    HashtagService hashtagService;
    @NonFinal
    List<Hashtag> mockHashtags;
    @NonFinal
    List<Announcement> mockAnnouncementsList;
    @NonFinal
    Set<Location> mockLocation;

    public List<Announcement> findAnnouncementByCriteria(SearchCriteria criteria) {
        val user = this.principals.findUserByEmail(principals.getEmail());
        principals.getPrincipal();
        val mockAnnouncement = new Announcement();

        if (user.isPresent()) {
            mockAnnouncement.setId(1L);
            mockAnnouncement.setContent("Ogloszenie czeka");
            mockAnnouncement.setDateOfPublication(LocalDate.of(2019, 5, 2));
            mockAnnouncement.setDateEndOfPublication(LocalDate.of(2020, 5, 2));
            mockAnnouncement.setDateSaveAnnouncement(LocalDate.of(2019, 5, 2));
            mockAnnouncement.setPaid(true);
            mockAnnouncement.setDone(false);
            mockAnnouncement.setPro(false);
            mockAnnouncement.setUser(user.get());
            mockAnnouncement.setImage(2);
//        mockAnnouncement.setLocations();
            mockAnnouncement.setHashtags(mockHashtags);
            mockAnnouncementsList.add(mockAnnouncement);
        }
        return mockAnnouncementsList;
    }

    List<Hashtag> hashtagList(Hashtag hashtag) {
        this.mockHashtags = new ArrayList<>();
        mockHashtags.add(new Hashtag(1L, "Malowanie", mockAnnouncementsList));
        return mockHashtags;
    }

    Set<Location> LocationMockSet(Location location) {
        this.mockLocation = new HashSet<>();
        mockLocation.add(new Location(
                1L,
                12.15f,
                16.17f,
                "Wawa",
                "Grześtam",
                Stream.ofAll(mockAnnouncementsList).toJavaSet()));
        return mockLocation;
    }
}
