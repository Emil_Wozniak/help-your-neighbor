package pl.helpyourneighbor.service.search;

import org.springframework.stereotype.Service;
import pl.helpyourneighbor.model.search.SearchCriteria;

@Service
public class SearchService implements SearchCriteriaInterface {

    @Override
    public String searchByAddress(SearchCriteria search) {

        StringBuilder sql = new StringBuilder("SELECT announcement from announcements where ");

        if (search.getHashtag() != null) {
            sql.append("announcement.hashtag = ").append(search.getHashtag());
        }
//        TODO like above for all if
        if (search.getCity() != null) {
            sql.append(" and announcement.city =").append(search.getCity());
        }
//        if (search.getMarkerImage() != null) {
//            sql.append(" and announcement.mark_image =").append(search.getMarkerImage());
//        }
        if (search.getFrom() != null) {
            sql.append(" and announcement.from =").append(search.getFrom());
        }
        if (search.getWhen() != null) {
            sql.append(" and announcement.when =").append(search.getWhen());
        }
        if (search.getTo() != null) {
            sql.append(" and announcement.to =").append(search.getTo());
        }
        return sql.toString();
    }
}
