package pl.helpyourneighbor.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.helpyourneighbor.model.marker.MarkerImage;

public interface MarkerImageRepository extends JpaRepository<MarkerImage, Long> {
}
