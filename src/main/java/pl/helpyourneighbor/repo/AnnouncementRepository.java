package pl.helpyourneighbor.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.user.User;

import java.util.List;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {

    List<Announcement> findByUser(User user);

    @Query("SELECT example " +
            "FROM Announcement as example " +
            "LEFT JOIN FETCH example.locations " +
            "LEFT JOIN FETCH example.hashtags " +
            "LEFT JOIN FETCH example.user " +
            "WHERE example.user.email=:email ")
    List<Announcement> findAnnouncementByUserEmail(String email);

    @Query(            "FROM Announcement as example " +
            "LEFT JOIN FETCH example.locations " +
            "LEFT JOIN FETCH example.hashtags ")
    List<Announcement> findAnnouncements();
}
