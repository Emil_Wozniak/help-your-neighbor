package pl.helpyourneighbor.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.helpyourneighbor.model.user.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
