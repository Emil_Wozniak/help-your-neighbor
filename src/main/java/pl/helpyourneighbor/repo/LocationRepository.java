package pl.helpyourneighbor.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.helpyourneighbor.model.announcement.Location;

import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    Optional<Location> findByLongitudeAndLatitude(float longitude, float latitude);
}
