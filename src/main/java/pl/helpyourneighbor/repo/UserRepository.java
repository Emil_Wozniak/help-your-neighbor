package pl.helpyourneighbor.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.helpyourneighbor.model.user.User;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("SqlUnusedCte")
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT user FROM User AS user " +
            "LEFT JOIN FETCH user.details " +
            "LEFT JOIN FETCH user.authorities " +
            "WHERE user.email=:email")
    Optional<User> findSocialByEmail(String email);

    @Query("SELECT user FROM User AS user " +
            "LEFT JOIN FETCH user.details " +
            "LEFT JOIN FETCH user.authorities " +
            "LEFT JOIN FETCH user.announcements " +
            "WHERE user.email=:email")
    Optional<User> findSocialByEmailEagerly(String email);

    @Query("select user from User as user where user.email=:email ")
    Optional<User> findLocalByEmail(String email);

    @Query(value = "WITH updated " +
            "AS (UPDATE hyn_user SET phone=:phone, activated=true " +
            "WHERE email=:email RETURNING id) " +
            "SELECT * from hyn_user WHERE email=:email ",
            nativeQuery = true)
    User activateUser(String email, String phone);
    
}
