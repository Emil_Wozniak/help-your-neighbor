/**
 * Provides route endpoints handlers.
 *
 * @author emil.wozniak.591986@gmail.com
 * @since 1.0
 * @see pl.helpyourneighbor.web.routes
 */
package pl.helpyourneighbor.handler;