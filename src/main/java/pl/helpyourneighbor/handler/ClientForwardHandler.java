package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.servlet.function.ServerResponse.ok;
import static pl.helpyourneighbor.config.constants.AppConstants.INDEX_HTML;

@Service
@RequiredArgsConstructor
public class ClientForwardHandler {
    /**
     * Forwards any unmapped paths (except those containing a period)
     * to the client {@code index.html}.
     * functional equivalent to:
     * <pre>
     *     <code>
     * &commat;GetMapping(value = "/**\/{path:[^\\.]*}")
     * public String forward() {
     *      return "forward:/";
     * }
     *     </code>
     * </pre>
     * index.html on the classpath
     *
     * @return forward to client {@code index.html}.
     */
    @SuppressWarnings("unused")
    public @NotNull ServerResponse render(ServerRequest request) {
        return ok().contentType(TEXT_HTML).body(new ClassPathResource(INDEX_HTML));
    }
}
