package pl.helpyourneighbor.handler;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.model.user.UserDTO;
import pl.helpyourneighbor.service.user.SocialService;
import pl.helpyourneighbor.util.mapper.PrincipalMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import static org.springframework.web.servlet.function.ServerResponse.noContent;
import static org.springframework.web.servlet.function.ServerResponse.ok;
import static pl.helpyourneighbor.config.constants.AppConstants.ERROR_MSG;
import static pl.helpyourneighbor.config.constants.UserConstants.PHONE;

/**
 * Handler for OAuth user.
 */
@Slf4j
@Value
@Service
@NonFinal
@RequiredArgsConstructor
public class AuthorizationHandler {

    PrincipalMapper mapper;
    SocialService service;
    HttpServletRequest httpServletRequest;

    /**
     * Session endpoint.
     * <p>
     * Method gets current login user, then gets its details with all necessary properties.
     * </p>
     *
     * @param request Client request
     * @return Map of user principals attributes.
     * @see UserDTO
     * @see OAuth2User
     * @see SocialService#getCurrentUser()
     * @see SocialService#getUserDetails(OAuth2User)
     * @see AuthorizationHandler#appendProperty(Map)
     */
    @SneakyThrows
    @SuppressWarnings("unused")
    public @NotNull ServerResponse register(ServerRequest request) {
        return Try
                .of(this.service::getCurrentUser)
                .map(this.service::getUserDetails)
                .peek(this::appendProperty)
                .recover(error -> Map.of())
                .map(details -> ok().contentType(APPLICATION_JSON).body(details))
                .get();
    }

    /**
     * Gets user from {@link RequestContextHolder}.
     *
     * @param request client request.
     * @return user session object.
     */
    @SuppressWarnings("unused")
    public @NotNull ServerResponse account(ServerRequest request) {
        val principals = this.httpServletRequest.getUserPrincipal();
        return principals != null ?
                ok().contentType(APPLICATION_JSON).body(principals) :
                noContent().build();
    }

    @ResponseBody
    @GetMapping("/error")
    public String error(HttpServletRequest request) {
        val message = (String) request.getSession().getAttribute(ERROR_MSG);
        request.getSession().removeAttribute(ERROR_MSG);
        return message;
    }

    /**
     * @param principals current principals
     */
    private void appendProperty(Map<String, Object> principals) {
        if (getContext().getAuthentication() != null) {
            mapper.appendProperty(principals, PHONE);
        }
    }
}
