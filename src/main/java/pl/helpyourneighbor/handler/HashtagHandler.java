package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.model.announcement.Hashtag;
import pl.helpyourneighbor.service.hashtag.HashtagServiceImpl;
import pl.helpyourneighbor.util.extractor.IdExtractor;

import java.net.URI;

import static org.springframework.web.servlet.function.ServerResponse.*;

@Value
@NonFinal
@RequiredArgsConstructor
@Service(value = "hashtagHandler")
public class HashtagHandler implements ApiHandler, IdExtractor {

    HashtagServiceImpl service;

    /**
     * @param request client request
     * @return ServerResponse 204, "No Content"
     */
    @Override
    @SneakyThrows
    public @NotNull ServerResponse delete(ServerRequest request) {
        service.delete(id(request));
        return noContent().build();
    }

    /**
     * @param request client request
     * @return ServerResponse 204, "Created" with body content
     * of created HelpRequest.
     */
    @Override
    @SneakyThrows
    public @NotNull ServerResponse create(ServerRequest request) {
        val body = request.body(Hashtag.class);
        val entity = service.save(body);
        val uri = URI.create(entity.getId().toString());
        return created(uri).body(entity);
    }

    /**
     * @param request client request
     * @return 200, "OK" with body content of HelpRequest matching
     * id of param variable.
     */
    @Override
    public @NotNull ServerResponse getOne(ServerRequest request) {
        val id = id(request);
        val optional = service.getOne(id);
        return optional
                .map(value -> ok().body(value))
                .orElseGet(() -> noContent().build());
    }

    /**
     * @param request client request
     * @return 200, "OK" with entity body content
     */
    @Override
    public @NotNull ServerResponse getAll(ServerRequest request) {
        return ok().body(service.getAll());
    }
}

