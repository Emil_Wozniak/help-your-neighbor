package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.model.user.User;
import pl.helpyourneighbor.model.user.UserDTO;
import pl.helpyourneighbor.service.user.UserService;
import pl.helpyourneighbor.util.extractor.NonNullable;

import java.net.URI;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import static org.springframework.web.servlet.function.ServerResponse.*;

@Value
@NonFinal
@RequiredArgsConstructor
@Service(value = "userHandler")
public class UserHandler implements ApiHandler {

    UserService service;

    @SneakyThrows
    public @NotNull ServerResponse delete(ServerRequest request) {
        val id = getId(request);
        service.delete(id);
        getContext().setAuthentication(null);
        return noContent().build();
    }

    @SneakyThrows
    public @NotNull ServerResponse create(ServerRequest request) {
        val body = request.body(User.class);
        if (body.isNonNullProp()) {
            val dto = new UserDTO(body);
            val entity = service.save(dto);
            val uri = URI.create(entity.getId().toString());
            return created(uri).body(entity);
        } else return badRequest().body(body);
    }

    public @NotNull ServerResponse getOne(ServerRequest request) {
        val id = getId(request);
        val response = service.getOne(id);
        return response
                .map(user -> ok().contentType(APPLICATION_JSON).body(user))
                .orElse(notFound().build());
    }

    @SuppressWarnings("unused")
    public @NotNull ServerResponse getAll(ServerRequest request) {
        return ok().contentType(APPLICATION_JSON).body(service.getAll());
    }

    @SneakyThrows
    public @NotNull ServerResponse update(ServerRequest request) {
        val user = request.body(User.class);
        val update = service.activate(user);
        return ok().contentType(APPLICATION_JSON).body(update);
    }

    @NotNull
    private String getId(ServerRequest request) {
        return request.pathVariable("id");
    }
}
