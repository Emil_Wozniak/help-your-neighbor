package pl.helpyourneighbor.handler;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

/**
 * Abstract Server-side HTTP response producer.
 * <p>
 * This Interface provides all basic methods for typical HTTP request and helper methods.
 */
public interface ApiHandler {

    /**
     * @param request client request
     * @return ServerResponse 204, "No Content"
     */
    @NotNull ServerResponse delete(ServerRequest request);

    /**
     * @param request client request
     * @return 201, "Created" with entity body content
     */
    @NotNull ServerResponse create(ServerRequest request);

    /**
     * @param request client request
     * @return 200, "OK" with entity body content
     */
    @NotNull ServerResponse getOne(ServerRequest request);

    /**
     * @param request client request
     * @return 200, "OK" with entity body content
     */
    @NotNull ServerResponse getAll(ServerRequest request);

}
