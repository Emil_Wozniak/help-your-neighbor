package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.model.management.ProfileInfo;

import java.util.function.Function;

import static org.springframework.web.servlet.function.ServerResponse.ok;

@Slf4j
@Service
@RequiredArgsConstructor
public class ManagementHandler {

    @Value("${spring.profiles.active}")
    private String activeProfile;

    private final Function<Void, ProfileInfo> produceInfo = (it) -> ProfileInfo
            .info()
            .profiles(activeProfile)
            .ribbon(activeProfile.contains("dev"))
            .get();

    @SuppressWarnings("unused")
    public @NotNull ServerResponse info(ServerRequest request) {
        return ok().body(produceInfo.apply(null));
    }
}
