package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.search.SearchCriteria;
import pl.helpyourneighbor.service.announcement.AnnouncementService;
import pl.helpyourneighbor.service.search.AnnouncementSearchService;
import pl.helpyourneighbor.service.user.SocialService;
import pl.helpyourneighbor.util.extractor.IdExtractor;

import static org.springframework.web.servlet.function.ServerResponse.*;

@Value
@NonFinal
@RequiredArgsConstructor
@Service(value = "announcementHandler")
public class AnnouncementHandler implements ApiHandler, IdExtractor {

    SocialService users;
    AnnouncementService service;
    AnnouncementSearchService searchService;

    /**
     * @param request client request
     * @return ServerResponse 204, "No Content"
     */
    @Override
    @SneakyThrows
    public @NotNull ServerResponse delete(ServerRequest request) {
        service.delete(id(request));
        return noContent().build();
    }

    /**
     * @param request client request
     * @return ServerResponse 204, "Created" with body content
     * of created HelpRequest.
     */
    @Override
    @SneakyThrows
    public @NotNull ServerResponse create(ServerRequest request) {
        val announcement = request.body(Announcement.class);
        val isStored = users.createAnnouncement(announcement);
        return isStored
                ? ok().body(announcement)
                : badRequest().body(announcement);
    }

    /**
     * @param request client request
     * @return 200, "OK" with body content of HelpRequest matching
     * id of param variable.
     */
    @Override
    public @NotNull ServerResponse getOne(ServerRequest request) {
        return this.service.getOne(id(request))
                .map(value -> ok().body(value))
                .orElseGet(() -> noContent().build());
    }

    /**
     * @param request client request
     * @return 200, "OK" with entity body content
     */
    @Override
    public @NotNull ServerResponse getAll(ServerRequest request) {
        return ok().body(this.service.getAll());
    }

    /**
     * @param request client request with body of {@link SearchCriteria}
     * @return 200, "OK" with entity body content
     */
    @SneakyThrows
    public @NotNull ServerResponse getByCriteria(ServerRequest request) {
        val criteria = request.body(SearchCriteria.class);
        val response = this.searchService.findAnnouncementByCriteria(criteria);
        return ok().body(response);
    }
}
