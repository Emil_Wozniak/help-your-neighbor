package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.service.marker.MarkerImageService;
import pl.helpyourneighbor.util.extractor.IdExtractor;

import static org.springframework.web.servlet.function.ServerResponse.ok;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarkerImageHandler implements IdExtractor {

    private final MarkerImageService service;

    /**
     * @param request client request
     * @return 200, "OK" with entity body content
     */
    public @NotNull ServerResponse getAll(ServerRequest request) {
        return ok().body(service.getAll());
    }
}
