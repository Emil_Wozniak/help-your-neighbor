package pl.helpyourneighbor.handler;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.model.search.MapCoordinates;

import static org.springframework.web.servlet.function.ServerResponse.ok;

@Slf4j
@Value
@Service
@NonFinal
@RequiredArgsConstructor
public class CoordinatesHandler {

    @SneakyThrows
    public @NotNull ServerResponse post(ServerRequest request) {
        val coordinates = request.body(MapCoordinates.class);
        return ok().body(coordinates);
    }
}
