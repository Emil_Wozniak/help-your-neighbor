/**
 * Provides classes for OAuth Provider.
 *
 * @author emil.wozniak.591986@gmail.com
 * @since 1.0
 * @see <a href="https://en.wikipedia.org/wiki/List_of_OAuth_providers">List of OAuth providers</a>
 * @see <a href="https://auth0.com/docs/protocols/protocol-oauth2">OAuth 2.0 Authorization Framework</a>
 */
package pl.helpyourneighbor.model.provider;