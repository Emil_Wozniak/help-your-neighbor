package pl.helpyourneighbor.model.provider;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import pl.helpyourneighbor.config.errors.DeserializerException;

import java.util.stream.Stream;

@Slf4j
public class AuthProviderDeserializer extends JsonDeserializer<AuthProvider> {

    @Override
    public AuthProvider deserialize(JsonParser parser, DeserializationContext context) {
        return Try
                .of(() -> {
                    val node = (JsonNode) parser.getCodec().readTree(parser);
                    val name = node.get("name").asText();
                    return Stream
                            .of(AuthProvider.values())
                            .filter(value -> value.getName().equals(name))
                            .findFirst()
                            .orElseThrow(() ->
                                    new IllegalArgumentException("name " + name + " is not recognized"));
                })
                .onFailure(error -> log.error(error.getMessage()))
                .getOrElseThrow(() -> new DeserializerException("Can't create object"));

    }
}
