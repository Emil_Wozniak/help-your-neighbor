package pl.helpyourneighbor.model.provider;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = AuthProviderDeserializer.class)
public enum AuthProvider implements Serializable {
    /**
     * Provider type for application itself.
     */
    LOCAL("LOCAL"),
    /**
     * Provider type for Google.
     */
    GOOGLE("GOOGLE"),
    /**
     * Provider type for Github.
     */
    GITHUB("GITHUB");

    private final static Map<String, AuthProvider> labels = new HashMap<>();

    /**
     * Provider key property specific for Google.
     */
    public static final String ISS = "iss";
    /**
     * Provider value property specific for Google.
     */
    public static final String GOOGLE_ISS = "https://accounts.google.com";

    static {
        Arrays.stream(values()).forEach(provider -> labels.put(provider.name, provider));
    }

    private final String name;

    public static AuthProvider getProvider(String name) {
        if (name != null && labels.containsKey(name.toUpperCase())) {
            return labels.get(name.toUpperCase());
        } else {
            val message = "Provider of type " + name + " is not known";
            throw new UnknownProviderException(message);
        }
    }

    /**
     * returns OAuth provider name depending on {@link OAuth2User#getAttributes()}  }
     * @param user current user in he security context.
     * @return google if {@link OAuth2User#getAttributes()} contains
     * {@link AuthProvider#ISS}
     */
    public static String from(OAuth2User user) {
        val attributes = user.getAttributes();
        return isGoogle(attributes) ? GOOGLE.name : GITHUB.name;
    }

    private static boolean isGoogle(Map<String, Object> attributes) {
        return attributes.containsKey(ISS)
                && attributes.get(ISS).toString().equals(GOOGLE_ISS);
    }

    static class UnknownProviderException extends RuntimeException {
        public UnknownProviderException(String message) {
            super(message);
        }
    }
}
