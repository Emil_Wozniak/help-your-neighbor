package pl.helpyourneighbor.model.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.helpyourneighbor.model.announcement.Hashtag;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class SearchCriteria {
    private String city;
    private boolean isPaid;
    private LocalDate when;
    private Hashtag hashtag;
    private LocalDate from;
    private LocalDate to;
}
