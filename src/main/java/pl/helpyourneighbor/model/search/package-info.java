/**
 * Provides class for search {@link pl.helpyourneighbor.model.announcement.Announcement} and classes in entity fields.
 *
 * @author Iwona007
 * @since 1.0
 */
package pl.helpyourneighbor.model.search;