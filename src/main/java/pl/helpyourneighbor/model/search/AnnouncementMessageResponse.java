package pl.helpyourneighbor.model.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.helpyourneighbor.model.announcement.Announcement;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementMessageResponse {
    private Announcement announcement;
}
