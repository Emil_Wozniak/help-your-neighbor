package pl.helpyourneighbor.model.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.helpyourneighbor.model.announcement.Hashtag;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnnouncementMessageRequest {
    private String city;
    private boolean isPaid;
    private LocalDate when;
    private Hashtag hashtag;
    private LocalDate from;
    private LocalDate to;
    private MapCoordinates coordinates;
}
