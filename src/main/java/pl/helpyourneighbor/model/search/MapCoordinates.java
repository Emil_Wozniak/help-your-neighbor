package pl.helpyourneighbor.model.search;

import lombok.Value;

@Value
public class MapCoordinates {
    float north;
    float south;
    float east;
    float west;
    double zoom; // 0-18 either integer or double
}
