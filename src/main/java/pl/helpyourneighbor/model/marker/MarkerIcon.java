package pl.helpyourneighbor.model.marker;

/**
 * Class represents all posible types of icons.
 * @author emil.wozniak.591986@gmail.com
 * @version 1.0
 * @since 09.01.2021
 */
@SuppressWarnings("SpellCheckingInspection")
public enum MarkerIcon {
    BRUSH,
    SPATULA,
    BABY,
    DOG,
    CAT,
    BAG,
    TAP,
    COFFEE,
    POT,
    GARBAGE,
    NEEDLE,
    BROOM,
    RUNNING,
    ROLLERS,
    ICESKATES,
    SKATEBOARD,
    FOOTBALL,
    VOLLEYBALL,
    BASKETBALL,
    LAPTOP,
    PHONE,
    TV,
    AUDIO,
    OUTLET,
    PLANT,
    WALK,
    CINEMA,
    THEATRE,
    LIGHTBULB,
    CARLIFT,
    BOARDGAME,
    TIRE,
    CARREPAIR,
    REPAIR,
    FURNITURE,
    STOCKPILE
}
