package pl.helpyourneighbor.model.marker;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Cache;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY;
import static pl.helpyourneighbor.config.constants.AppConstants.MARKER_IMAGE;
import static pl.helpyourneighbor.config.constants.AppConstants.MARKER_IMG_SEQ;

/**
 * Class represents Leaflet map marker icon image data.
 * <p>
 * Entities of this class should be unmodifiable and unique, therefore it has uniqueConstraints of enum type
 * {@link MarkerIcon} what assert that only one instance of this object exists in a container and
 * it will be cached in memory what decreases a database usage.
 *<p>
 * <span style="color:yellow">
 *     Any try of create new instance, for example: in csv file requires reboot an application.
 * </span>
 *
 * @see <a href="https://leafletjs.com/reference-1.7.1.html#marker-icon">Leaflet marker</a>
 * @see <a href="src/main/webapp/app/assets/img/emots">png files</a>
 */
@Data
@Entity
@Cache(usage = READ_ONLY)
@Table(name = MARKER_IMAGE, uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class MarkerImage {

    @Id
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = IDENTITY, generator = MARKER_IMG_SEQ)
    @SequenceGenerator(name = MARKER_IMAGE, sequenceName = MARKER_IMG_SEQ, allocationSize = 1)
    private Long id;

    @Column(nullable = false, insertable = false, updatable = false)
    private String type;

    @Column(nullable = false, insertable = false, updatable = false)
    private String description;

    @Enumerated(value = STRING)
    @Column(nullable = false, insertable = false, updatable = false)
    private MarkerIcon name;
}
