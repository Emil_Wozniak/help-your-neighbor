/**
 * Domain classes for leaflet marker icons.
 *
 * @author emil.wozniak.591986@gmail.com
 * @since 1.0
 */
package pl.helpyourneighbor.model.marker;

