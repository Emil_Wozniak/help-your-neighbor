package pl.helpyourneighbor.model.auth;


import config.auth.UserPrincipals;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.helpyourneighbor.model.user.User;

import java.util.Map;

import static java.util.Collections.singletonList;
import static pl.helpyourneighbor.config.constants.AuthoritiesConstants.USER;

@Setter
@Getter
@NoArgsConstructor
public class UserPrincipal extends UserPrincipals {

    private Long id;
    private String email;

    public static UserPrincipal empty() {
        return new UserPrincipal();
    }

    public static UserPrincipal create(User user) {
        return (UserPrincipal) UserPrincipal
                .builder()
                .id(user.getId())
                .email(user.getEmail())
                .authorities(singletonList(new SimpleGrantedAuthority(USER)))
                .build();
    }

    public static UserPrincipal create(User user, Map<String, Object> attributes) {
        final var userPrincipal = UserPrincipal.create(user);
        userPrincipal.setAttributes(attributes);
        return userPrincipal;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return String.valueOf(id);
    }
}