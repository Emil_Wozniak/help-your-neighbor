package pl.helpyourneighbor.model.announcement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;
import static pl.helpyourneighbor.config.constants.EntityConstant.HASHTAG;
import static pl.helpyourneighbor.config.constants.EntityConstant.HASHTAG_SEQ;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = HASHTAG)
public class Hashtag implements Serializable {

    private static final long serialVersionUID = 21L;

    @Id
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @SequenceGenerator(name = HASHTAG, sequenceName = HASHTAG_SEQ, allocationSize = 1)
    @GeneratedValue(strategy = AUTO, generator = HASHTAG_SEQ)
    private Long id;

    @NonNull
    @Column(name = "name", nullable = false)
    private String name;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = LAZY, cascade = ALL)
    private List<Announcement> announcementList;

}
