package pl.helpyourneighbor.model.announcement;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.BatchSize;
import pl.helpyourneighbor.model.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;
import static pl.helpyourneighbor.config.constants.AppConstants.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = ANNOUNCEMENT)
public class Announcement implements Serializable {

    private static final long serialVersionUID = 20L;

    @Id
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = IDENTITY, generator = ANNOUNCEMENT_SEQ)
    @SequenceGenerator(name = ANNOUNCEMENT, sequenceName = ANNOUNCEMENT_SEQ, allocationSize = 1)
    private Long id;

    @NonNull
    @Column(name = "content", nullable = false, length = 140)
    private String content;

    @NonNull
    @JsonFormat(shape = STRING, pattern = LOCAL_DATE_FORMAT)
    @Column(name = "date_of_publication", nullable = false)
    private LocalDate dateOfPublication;

    @NonNull
    @JsonFormat(shape = STRING, pattern = LOCAL_DATE_FORMAT)
    @Column(name = "date_end_of_publication", nullable = false)
    private LocalDate dateEndOfPublication;

    @NonNull
    @JsonFormat(shape = STRING, pattern = LOCAL_DATE_FORMAT)
    @Column(name = "date_save_announcement", nullable = false)
    private LocalDate dateSaveAnnouncement;

    @Column(name = "is_paid", nullable = false)
    private boolean isPaid;

    @Column(name = "is_done", nullable = false)
    private boolean isDone;

    @Column(name = "is_pro", nullable = false)
    private boolean isPro;

    @Column(name = "image", nullable = false)
    private int image;

    @NonNull
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "user_id")
    @ManyToOne(cascade = ALL, fetch = LAZY, optional = false)
    private User user;

    @NonNull
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @BatchSize(size = 20)
    @ManyToMany(cascade = ALL)
    @JsonIgnoreProperties(value = {"announcements"})
    @JoinTable(
            name = "announcement_location",
            joinColumns = {@JoinColumn(name = "announcements_id", referencedColumnName = ID)},
            inverseJoinColumns = {@JoinColumn(name = "location_id", referencedColumnName = ID)}
    )
    private Set<Location> locations;

    @NonNull
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = LAZY, cascade = ALL)
    @JsonIgnoreProperties(value = {"announcements"})
    @JoinTable(
            name = "announcement_hashtag_list",
            joinColumns = {@JoinColumn(name = "announcement_id", referencedColumnName = ID)},
            inverseJoinColumns = {@JoinColumn(name = "hashtag_id", referencedColumnName = ID)}
    )
    private List<Hashtag> hashtags;

}
