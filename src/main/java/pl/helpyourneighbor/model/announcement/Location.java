package pl.helpyourneighbor.model.announcement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;
import static pl.helpyourneighbor.config.constants.AppConstants.LOCATION;
import static pl.helpyourneighbor.config.constants.AppConstants.LOCATION_SEQ;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = LOCATION)
public class Location {

    private static final long serialVersionUID = 22L;

    @Id
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @SequenceGenerator(name = LOCATION, sequenceName = LOCATION_SEQ, allocationSize = 1)
    @GeneratedValue(strategy = IDENTITY, generator = LOCATION_SEQ)
    private Long id;

    @NonNull
    @Column(name = "latitude", nullable = false)
    private Float latitude;

    @NonNull
    @Column(name = "longitude", nullable = false)
    private Float longitude;

    @NonNull
    @Column(name = "city", nullable = false)
    private String city;

    @NonNull
    @Column(name = "address", nullable = false)
    private String address;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = LAZY, cascade = ALL, mappedBy = "locations")
    private Set<Announcement> announcements;

}
