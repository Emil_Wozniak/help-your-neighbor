package pl.helpyourneighbor.model.user;

import lombok.NonNull;

import static org.apache.commons.lang3.Validate.*;

public class Name {
    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 40;
    private static final String VALID_CHARACTERS = "[A-Za-z0-9_-]+";

    private final String value;

    public Name(@NonNull String value) {
        notBlank(value);
        final String trimmed = value.trim();
        inclusiveBetween(MIN_LENGTH,
                MAX_LENGTH,
                trimmed.length());
        matchesPattern(trimmed,
                VALID_CHARACTERS,
                "Allowed characters are: %s", VALID_CHARACTERS);
        this.value = trimmed;
    }

    public String value() {
        return value;
    }
}
