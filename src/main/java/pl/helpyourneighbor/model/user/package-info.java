/**
 * Authorization entities and classes in entity fields.
 *
 * @version 1.0
 * @author emil.wozniak.591986@gmail.com
 * @since 11.02.2021
 */
package pl.helpyourneighbor.model.user;