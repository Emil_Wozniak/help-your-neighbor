package pl.helpyourneighbor.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.BatchSize;
import pl.helpyourneighbor.config.constants.AppConstants;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.provider.AuthProvider;
import pl.helpyourneighbor.util.extractor.NonNullable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;
import static pl.helpyourneighbor.config.constants.AppConstants.*;
import static pl.helpyourneighbor.config.constants.ContactConstants.EMAIL_REGEX;
import static pl.helpyourneighbor.config.constants.EntityConstant.USER_SEQ;
import static pl.helpyourneighbor.model.provider.AuthProvider.LOCAL;
import static pl.helpyourneighbor.model.user.I18n.*;

/**
 * User entity.
 * <p>
 * PSQL has already {@code user} table, user entity need to have different name
 * acronym <b>hyn_user</b> stands for <b>help your neighbor user</b>.
 * <p>
 * See <a href="https://www.postgresql.org/docs/current/user-manag.html" >PSQL User</a>
 *
 * @see AppConstants#SEQUENCER entity uses default hibernate sequenser.
 * @see Announcement user announcement entities.
 */
@Data
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "User")
@Table(name = "hyn_user", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class User implements UserModel, Serializable, NonNullable {

    private static final long serialVersionUID = 10L;

    @Id
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @SequenceGenerator(name = USER, sequenceName = USER_SEQ)
    @GeneratedValue(strategy = AUTO, generator = USER_SEQ)
    private Long id;

    @NotBlank
    @Pattern(regexp = EMAIL_REGEX)
    @Column(name = EMAIL, nullable = false)
    private String email;

    @Column(name = PHONE, nullable = false)
    private String phone;

    @Column(name = ACTIVATED, nullable = false)
    private Boolean activated;

    @Column(name = BANNED, nullable = false)
    private Boolean banned;

    @Enumerated(value = STRING)
    @Column(name = LANG, nullable = false, length = 5)
    private I18n langKey;

    @NonNull
    @Enumerated(STRING)
    private AuthProvider provider;

    @Transient
    private Map<String, Object> principal;

    @NonNull
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @Column(name = "announcements_amount", nullable = false)
    private Short announcementsAmount;

    @BatchSize(size = 20)
    @ManyToMany(fetch = EAGER, cascade = ALL)
    @JoinTable(
            name = "hyn_user_authority",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = ID)},
            inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")}
    )
    private Set<Authority> authorities;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnoreProperties(value = USER)
    @OneToMany(mappedBy = USER, fetch = LAZY, cascade = ALL)
    private List<Announcement> announcements;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @PrimaryKeyJoinColumn
    @OneToOne(mappedBy = USER, fetch = EAGER, cascade = ALL)
    private UserDetails details;

    public static User createSocialUser(Map<String, Object> principal,
                                        String email,
                                        AuthProvider provider,
                                        Set<Authority> authorities) {
        return new User(
                DEFAULT_ID,
                email,
                DEFAULT_PHONE,
                TRUE,
                FALSE,
                PL,
                provider,
                principal,
                DEFAULT_AMOUNT,
                authorities,
                null,
                null);
    }

    public static User createUserLocal(@NotBlank String email,
                                       Set<Authority> authorities,
                                       UserDetails details,
                                       Map<String, Object> principal) {
        return new User(
                DEFAULT_ID,
                email,
                DEFAULT_PHONE,
                TRUE,
                FALSE,
                PL,
                LOCAL,
                principal,
                DEFAULT_AMOUNT,
                authorities,
                null,
                details);
    }


    public void addAnnouncement(Announcement announcement) {
        if (this.announcements != null) {
            this.announcements.add(announcement);
        } else {
            log.warn("Announcement is null, drop execution");
        }
    }

    @Override
    public boolean isNonNullProp() {
        return Arrays
                .stream(this.toString().split(","))
                .skip(1) // entity name + id
                .anyMatch(value -> !value.contains("null"));
    }
}
