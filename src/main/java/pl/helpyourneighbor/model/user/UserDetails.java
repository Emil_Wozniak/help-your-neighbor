package pl.helpyourneighbor.model.user;

import config.SecurityConfig;
import lombok.*;
import org.springframework.security.oauth2.core.user.OAuth2User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

import static javax.persistence.GenerationType.AUTO;
import static org.apache.commons.lang.Validate.notNull;
import static pl.helpyourneighbor.config.constants.AppConstants.SEQUENCER;

/**
 * UserDetails contains properties that are provided through
 * {@link OAuth2User} but are not necessary for store in the
 * database {@code hyn_user} table. Therefore this entity is
 * for {@link User} only or any other Entity that doesn't
 * provide below details into the {@link SecurityConfig}
 * service.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "hyn_user_details")
@Table(name = "hyn_user_details")
public class UserDetails implements Serializable {

    private static final long serialVersionUID = 11L;

    @Id
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = AUTO, generator = SEQUENCER)
    private Long id;

    /**
     * User full name or login
     */
    @NonNull
    @Column(name = "name")
    private String name;

    @NonNull
    @Column(name = "image_url", length = 500)
    private String imageUrl;

    @NonNull
    @Column(name = "locale", length = 4)
    private String locale;

    @MapsId
    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "hyn_user_id")
    private User user;

    @Builder
    @SuppressWarnings("unused")
    public UserDetails(
            @NotBlank String name,
            String imageUrl,
            String locale) {
        notNull(name);
        this.name = new Name(name).value();
        this.imageUrl = imageUrl;
        this.locale = locale;
    }

    public static UserDetails createDefaultDetails(UserDTO entity) {
        return UserDetails
                .builder()
                .imageUrl(entity.getImageUrl())
                .locale(entity.getLocale())
                .build();

    }
}
