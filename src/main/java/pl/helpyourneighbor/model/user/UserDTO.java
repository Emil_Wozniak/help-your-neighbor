package pl.helpyourneighbor.model.user;

import lombok.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import pl.helpyourneighbor.model.provider.AuthProvider;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static javax.persistence.EnumType.STRING;
import static pl.helpyourneighbor.config.constants.AppConstants.DEFAULT_LANGUAGE;
import static pl.helpyourneighbor.config.constants.ContactConstants.EMAIL_REGEX;
import static pl.helpyourneighbor.config.constants.ContactConstants.PHONE_REGEX;
import static pl.helpyourneighbor.config.constants.UserConstants.*;
import static pl.helpyourneighbor.model.user.Authority.createFromOAuthUser;

/**
 * A DTO representing a user, with his authorities.
 * <p>
 * Class stores and returns {@link OAuth2User} attributes.
 */
@Value
public class UserDTO implements OAuth2User {

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    Long id;

    @NotBlank
    String name;

    @NotBlank
    @NonNull
    @Pattern(regexp = EMAIL_REGEX)
    String email;

    @Pattern(regexp = PHONE_REGEX)
    String phone;

    String imageUrl;

    @NonNull
    Boolean activated;

    @NonNull
    Boolean banned;

    String locale;

    @NonNull
    @Enumerated(STRING)
    AuthProvider provider;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    Set<Authority> authorities;

    @Builder(builderClassName = "user")
    public UserDTO(User user) {
        val details = user.getDetails();
        this.id = user.getId();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.activated = user.getActivated();
        this.banned = user.getBanned();
        this.provider = user.getProvider();
        this.name = isDetails(details) ? details.getName() : null;
        this.locale = isDetails(details) ? details.getLocale() : null;
        this.imageUrl = isDetails(details) ? details.getImageUrl() : null;
        this.authorities = user.getAuthorities();
    }

    /**
     * <b>Active user constructor</b>
     * <p>
     * Constructor assumes that the user exists in the security context.
     * First argument is an object which comes from {@code hyn_user} table, if and only if User was found by
     * {@link User#getEmail()}. Second argument is an object which comes from
     * {@link SecurityContextHolder#getContext()}.getAuthentication().getPrincipal()
     * </p>
     * <p>
     * It is used to merge data from both fetched {@link User} and {@link OAuth2User} principals.
     * </p>
     *
     * @param user       {@link User} from database.
     * @param principals {@link OAuth2User} from SecurityContextHolder
     * @see SecurityContextHolder#getContext()
     * @see SecurityContext#getAuthentication()
     * @see Authentication#getPrincipal()
     */
    public UserDTO(User user, OAuth2User principals) {
        val dto = new UserDTO(principals);
        val details = user.getDetails();
        this.id = user.getId();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.activated = user.getActivated();
        this.banned = user.getBanned();
        this.provider = user.getProvider();
        this.name = isDetails(details) ? details.getName() : dto.getName();
        this.locale = isDetails(details) ? details.getLocale() : DEFAULT_LANGUAGE;
        this.imageUrl = isDetails(details) ? details.getImageUrl() : dto.getImageUrl();
        this.authorities = user.getAuthorities();
    }

    public UserDTO(OAuth2User principal) {
        val authorities = createFromOAuthUser(principal);
        val provider = AuthProvider.getProvider(AuthProvider.from(principal));
        this.id = null;
        this.phone = "";
        this.banned = false;
        this.name = principal.getAttribute(NAME);
        this.imageUrl = principal.getAttribute(PICTURE);
        this.email = principal.getAttribute(EMAIL);
        this.locale = principal.getAttribute(LOCALE);
        this.activated = true;
        this.provider = provider;
        this.authorities = authorities;
    }

    private boolean isDetails(UserDetails details) {
        return details != null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    public Set<Authority> getUserAuthorities() {
        return this.authorities;
    }

    @Override
    public Map<String, Object> getAttributes() {
        // @formatter:off
        return Map.of(
                NAME,           this.name != null ? this.name : "",
                IMAGE_URL,      this.imageUrl != null ? this.imageUrl : "",
                EMAIL,          this.email != null ? this.email : "",
                AUTHORITIES,    this.authorities,
                LOCALE,         this.locale,
                PROVIDER,       this.provider,
                PHONE,          this.phone != null ? phone : false,
                ACTIVATED,      this.activated
        );
        // @formatter:on

    }
}
