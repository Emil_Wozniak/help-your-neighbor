package pl.helpyourneighbor.model.user;

import lombok.Getter;

/**
 * Internationalization possible locales.
 */
@Getter
public enum I18n {
    PL, EN
}
