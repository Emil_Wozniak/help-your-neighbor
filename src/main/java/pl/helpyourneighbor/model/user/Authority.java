package pl.helpyourneighbor.model.user;

import io.vavr.collection.Stream;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Authority")
@Table(name = "hyn_authority")
public class Authority implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Size(max = 50)
    @Column(length = 50)
    private String name;

    public static Set<Authority> createFromOAuthUser(OAuth2User user) {
        return Stream
                .ofAll(user.getAuthorities())
                .filter(it -> it.getAuthority().length() < 10)
                .map(it -> new Authority(it.getAuthority()))
                .toJavaSet();
    }

    @Override
    public String getAuthority() {
        return this.name;
    }
}
