package pl.helpyourneighbor.model.management;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.helpyourneighbor.handler.ManagementHandler;

/**
 * This class transfer application information from management endpoint to the client side.
 * <p>
 * It can be received from {@code /route/management/info} endpoint, which is exposed in Routes.
 * Exposed configuration is in {@code *.yml} files in {@code classpath:/resource}
 *
 * @see ManagementHandler
 */
@Getter
@Setter
@Builder(builderMethodName = "info", buildMethodName = "get")
public class ProfileInfo {

    /**
     * Google client id for register or login through Google account.
     */
    private String clientId;
    /**
     * Application profiles.
     */
    private String profiles;
    /**
     * Dev profile ribbon.
     */
    private Boolean ribbon;
}