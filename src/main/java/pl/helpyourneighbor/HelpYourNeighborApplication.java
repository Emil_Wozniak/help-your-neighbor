package pl.helpyourneighbor;

import lombok.SneakyThrows;
import lombok.val;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import static pl.helpyourneighbor.config.profiles.ProfileUtil.addDefaultProfile;
import static pl.helpyourneighbor.util.logger.StartupLogger.register;

@SpringBootApplication
public class HelpYourNeighborApplication {

    @SneakyThrows
    public static void main(String[] args) {
        val application = new SpringApplicationBuilder(HelpYourNeighborApplication.class).build();
        addDefaultProfile(application);
        val env = application.run(args).getEnvironment();
        register(env);
    }
}
