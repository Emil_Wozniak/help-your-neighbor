package pl.helpyourneighbor.util.function.json;

import com.fasterxml.jackson.databind.JsonNode;

import java.time.LocalDate;

public interface NodeExtractor {

    static NodeExtractor in(JsonNode node) {
        return new JsonValue(node);
    }

    /**
     * Method for accessing value of the specified field of
     * an object node by name and returns target nested
     * object.
     *
     * @param name target field name
     * @return Json node of the target nested object
     */
    NodeExtractor from(String name);
    NodeExtractor from(int number);

    /**
     * Method for accessing value of the specified field of
     * an object node by name and returns its string value.
     *
     * @param name target field name
     * @return string value of the field
     */
    String text(String name);

    /**
     * Method for accessing value of the specified field of
     * an object node by name and returns its number value.
     *
     * @param name target field name
     * @return number value of the field
     */
    Integer integer(String name);


    /**
     * Method for accessing value of the specified field of
     * an object node by name and returns its boolean value.
     *
     * @param name target field name
     * @return boolean value of the field
     */
    Boolean bool(String name);

    LocalDate localDate(String name);


}
