package pl.helpyourneighbor.util.function.json;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static java.time.ZoneOffset.UTC;

@Value
@RequiredArgsConstructor
class JsonValue implements NodeExtractor {

    JsonNode node;

    public NodeExtractor from(String name) {
        return new JsonValue(this.node.get(name));
    }

    @Override
    public NodeExtractor from(int number) {
        return new JsonValue(this.node.get(0));
    }

    @Override
    public String text(String name) {
        return this.node.get(name).asText();
    }

    @Override
    public Integer integer(String name) {
        return this.node.get(name).asInt();
    }

    @Override
    public Boolean bool(String name) {
        return this.node.get(name).asBoolean();
    }

    @Override
    public LocalDate localDate(String name) {
        return Instant
                .parse(this.text(name))
                .atZone(ZoneId.of(UTC.getId()))
                .toLocalDate();
    }
}
