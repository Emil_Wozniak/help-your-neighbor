package pl.helpyourneighbor.util.mapper;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Slf4j
@Component
public class PrincipalMapper {

    /**
     * <pre>
     *     {@code
     * - checks does SecurityContextHolder contains Principals
     * - checks does principals contains PHONE, if does puts it to new map of attributes
     * - recreate OAuth2User
     * - recreate token and add it as new Authentication to SecurityContextHolder.
     *     }
     * </pre>
     *
     * @param principals current principals
     */
    public void appendProperty(Map<String, Object> principals, String property) {
        val principal = getPrincipal();
        val authorities = principal.getAuthorities();
        val attributes = principal.getAttributes();
        val name = "sub";
        val map = tryAppend(principals, property, attributes);
        val user = (OAuth2User) new DefaultOAuth2User(authorities, map, name);
        val token = new OAuth2AuthenticationToken(user, authorities, name);
        getContext().setAuthentication(token);
    }

    private Map<String, Object> tryAppend(Map<String, Object> principals, String property, Map<String, Object> attributes) {
        return Try
                .of(() -> new HashMap<>(attributes))
                .peek(it -> it.put(property, principals.get(property)))
                .onFailure(error -> log.error("Property: '{}' is not part of the principals.", property))
                .onSuccess(prop -> log.info("{} added to properties", property))
                .getOrElse(new HashMap<>(attributes));
    }

    private OAuth2User getPrincipal() {
        return (OAuth2User) getContext().getAuthentication().getPrincipal();
    }
}
