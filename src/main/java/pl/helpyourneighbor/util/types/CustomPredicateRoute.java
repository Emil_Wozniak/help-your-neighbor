package pl.helpyourneighbor.util.types;

import io.vavr.collection.Stream;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.function.RequestPredicate;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.ServerResponse;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.servlet.function.RequestPredicates.*;
import static org.springframework.web.servlet.function.RouterFunctions.nest;

public abstract class CustomPredicateRoute {

    public static RequestPredicate POST(String pattern) {
        return method(POST).and(path(pattern)).and(accept(APPLICATION_JSON));
    }

    /**
     * @param pattern the pattern to match to
     * @param paths   supplied for the nested router function to delegate to
     * @return router function with nested functions
     */
    @NotNull
    @SafeVarargs
    public static RouterFunction<ServerResponse> nestPath(
            String pattern,
            RouterFunction<ServerResponse>... paths) {
        val head = new AtomicReference<>(paths[0]);
        getTail(paths).forEach(element -> head.getAndUpdate(it -> it.and(element)));
        return nest(path(pattern), head.get());
    }

    /**
     * Function gets all next nested router functions after the head.
     *
     * @param paths nested router functions
     * @return nested router functions
     */
    @NotNull
    private static List<RouterFunction<ServerResponse>> getTail(RouterFunction<ServerResponse>[] paths) {
        return Stream
                .ofAll(List.of(paths))
                .drop(1)
                .toJavaList();
    }
}
