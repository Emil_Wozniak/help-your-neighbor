package pl.helpyourneighbor.util.logger;

import io.vavr.control.Try;
import lombok.val;
import org.openjdk.jmh.annotations.Benchmark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.Objects;

import static java.net.InetAddress.getLocalHost;
import static pl.helpyourneighbor.config.constants.AppConstants.STARTUP_INDENT;
import static pl.helpyourneighbor.config.constants.AppConstants.STARTUP_LINE;

/**
 * Prints message containing application properties such as <pre>{@code
 * http protocol
 * server port number
 * context path
 * host address
 * application name
 * all active profiles
 * }</pre>
 */
public interface StartupLogger {

    Logger log = LoggerFactory.getLogger(StartupLogger.class);

    String SSL_KEY = "server.ssl.key-store";
    String CONTEXT_PATH = "server.servlet.context-path";
    String ERROR_MSG = "The host name could not be determined, using `localhost` as fallback";
    String LOCAL = "localhost";
    String APP_NAME = "spring.application.name";
    String PORT = "server.port";
    String ROOT = "/";

    static void register(Environment env) {
        val protocol = getProtocol(env);
        val serverPort = getServerPort(env);
        val contextPath = getContextPath(env);
        val hostAddress = getHostAddress();
        val appName = env.getProperty(APP_NAME);
        val activeProfiles = env.getActiveProfiles();

        console(STARTUP_LINE);
        console("Application ", appName, " is running! Access URLs:");
        console("Local: \t\t", protocol, "://localhost:", serverPort, contextPath);
        console("External: \t", protocol, "://", hostAddress, ":", serverPort, contextPath);
        console("Profile(s): \t", Arrays.toString(activeProfiles));
        console(STARTUP_LINE);
        System.out.println();
    }

    private static String getProtocol(Environment env) {
        return Try
                .of(() -> env.getProperty(SSL_KEY))
                .filter(Objects::nonNull)
                .getOrElse(() -> "http");
    }

    static String getServerPort(Environment env) {
        return env.getProperty(PORT);
    }

    private static String getContextPath(Environment env) {
        return Try
                .of(() -> env.getProperty(CONTEXT_PATH))
                .filter(it -> !it.isBlank())
                .getOrElse(ROOT);
    }

    private static String getHostAddress() {
        return Try
                .of(() -> getLocalHost().getHostAddress())
                .onFailure(it -> log.warn(ERROR_MSG))
                .getOrElse(LOCAL);
    }

    private static void console(String... args) {
        System.out.print(STARTUP_INDENT);
        Arrays.stream(args).forEach(System.out::print);
    }
}

