package pl.helpyourneighbor.util.extractor;

public interface NonNullable {

    /**
     * Asserts that any of entity properties <b>don't</b> have null value.
     *
     * @return true if any of entity properties doesn't have null value
     * @inheritDoc
     */
    boolean isNonNullProp();

}
