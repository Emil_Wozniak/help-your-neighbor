package pl.helpyourneighbor.util.extractor;

import lombok.NonNull;
import lombok.val;
import org.springframework.web.servlet.function.ServerRequest;

import static pl.helpyourneighbor.config.constants.AppConstants.ID;

public interface IdExtractor {
    /**
     * Returns long value of {@code ServerRequest} path variable.
     *
     * @param request client server request
     * @return id Long value
     */
    default @NonNull Long id(ServerRequest request) {
        val id = request.pathVariable(ID);
        return Long.valueOf(id);
    }
}
