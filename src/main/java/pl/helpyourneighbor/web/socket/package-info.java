/**
 * Provides reactive web sockets.
 * <p>
 * Sockets are exposed on port <b>7000</b>.
 *
 * @version 1.0
 * @author emil.wozniak.591986@gmail.com
 * @since 11.02.2021
 */
package pl.helpyourneighbor.web.socket;