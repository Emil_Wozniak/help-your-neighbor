package pl.helpyourneighbor.web.socket;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.val;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import pl.helpyourneighbor.model.announcement.Announcement;
import pl.helpyourneighbor.model.search.AnnouncementMessageRequest;
import pl.helpyourneighbor.model.search.AnnouncementMessageResponse;
import pl.helpyourneighbor.repo.AnnouncementRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static java.time.Duration.ofSeconds;
import static java.util.concurrent.Executors.newCachedThreadPool;

@Value
@NonFinal
@Controller
@RequiredArgsConstructor
public class AnnouncementSocket {

    AnnouncementRepository repository;

    @MessageMapping("announcements")
    public Flux<AnnouncementMessageResponse> criteria(AnnouncementMessageRequest request) {
        return getAnnouncementFlux(request).delayElements(ofSeconds(1));
    }

    private Flux<AnnouncementMessageResponse> getAnnouncementFlux(@NonNull AnnouncementMessageRequest request) {
        return Mono
                .fromFuture(this.findAll(request)
                        .toCompletableFuture()
                        .thenApply(Collection::stream))
                .flatMapMany(Flux::fromStream)
                .flatMap(it -> Flux.fromStream(
                        () -> Stream.generate(
                                () -> new AnnouncementMessageResponse(it))
                ));
    }

    @Async
    CompletableFuture<List<Announcement>> findAll(@NonNull AnnouncementMessageRequest request) {
        val completableFuture = new CompletableFuture<List<Announcement>>();
        newCachedThreadPool().submit(() -> {
            completableFuture.complete(repository.findAnnouncements());
            return null;
        });
        return completableFuture;
    }
}
