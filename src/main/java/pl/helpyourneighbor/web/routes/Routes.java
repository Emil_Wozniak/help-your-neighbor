package pl.helpyourneighbor.web.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.RouterFunctions;
import org.springframework.web.servlet.function.ServerResponse;
import pl.helpyourneighbor.handler.*;

import static org.springframework.web.servlet.function.RequestPredicates.*;
import static org.springframework.web.servlet.function.RouterFunctions.route;
import static pl.helpyourneighbor.config.constants.PathConstant.MANAGE;
import static pl.helpyourneighbor.util.types.CustomPredicateRoute.POST;
import static pl.helpyourneighbor.util.types.CustomPredicateRoute.nestPath;

@Configuration
class Routes {

    @Bean(name = "allRoutes")
    public RouterFunction<ServerResponse> routes(
            ClientForwardHandler client,
            UserHandler users,
            AuthorizationHandler authorization,
            ManagementHandler management,
            AnnouncementHandler announcements,
            MarkerImageHandler markerImages,
            CoordinatesHandler coordinates,
            HashtagHandler hashtags) {

        // @formatter:off
        return  RouterFunctions.route().GET("/*",    client::render)
                .nest(path("/route"), () ->
                                route().GET(    MANAGE,               management::info).build()
                        .and(   route().GET(    "/register",authorization::register).build())
                        .and(   route().GET(    "/account", authorization::account).build())
                        .and(nestPath("/users",
                                route(GET(      ""),        users::getAll),
                                route(GET(      "/{id}"),   users::getOne),
                                route(POST(     ""),        users::create),
                                route(PUT(      ""),        users::update),
                                route(DELETE(   "/{id}"),   users::delete)))
                        .and(nestPath("/announcements",
                                route(GET(      ""),         announcements::getAll),
                                route(GET(      "/criteria"),announcements::getByCriteria),
                                route(GET(      "/criteria"),announcements::getByCriteria),
                                route(GET(      "/{id}"),    announcements::getOne),
                                route(POST(     ""),         announcements::create),
                                route(DELETE(   "/{id}"),    announcements::delete)))
                        .and(nestPath("/hashtags",
                                route(GET(      ""),        hashtags::getAll),
                                route(GET(      "/{id}"),   hashtags::getOne),
                                route(POST(     ""),        hashtags::create),
                                route(DELETE(   "/{id}"),   hashtags::delete)))
                        .and(nestPath("/marker-images",
                                route(GET(      ""),        markerImages::getAll)))
                        .and(nestPath("/coordinates",
                                route(POST(      ""),       coordinates::post)))
                )
                .build();
        // @formatter:on
    }
}
