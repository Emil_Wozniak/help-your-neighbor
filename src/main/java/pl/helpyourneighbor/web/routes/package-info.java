/**
 * Provides http routes.
 *
 * @author emil.wozniak.591986@gmail.com
 * @since 1.0
 */
package pl.helpyourneighbor.web.routes;