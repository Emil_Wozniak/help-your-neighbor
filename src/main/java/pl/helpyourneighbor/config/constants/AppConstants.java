package pl.helpyourneighbor.config.constants;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class AppConstants implements EntityConstant {

    public static final String DEFAULT_LANGUAGE = "pl";

    public static final String STARTUP_LINE = "----------------------------------------------------------";
    public static final String STARTUP_INDENT = "\n\t";

    public static final String NO_PRINCIPALS = "User principals doesn't exist {}";
    public static final String USER_CREATED = "Created User: {}";

    public static final String INDEX_HTML = "static/index.html";

    public static final String LOCAL_DATE_FORMAT = "yyyy-MM-dd";

    public static final String BLANK = "";
}
