package pl.helpyourneighbor.config.constants;

/**
 * First argument is always a type.
 * Second is entity to string value or id.
 */
public interface EntityConstant extends UserConstants {
    String PERSIST_ONE_MSG = "Request find entity {}: {}";
    String PERSIST_ALL_MSG = "Request find entities {}";
    String PERSIST_SAVE_MSG = "Request save entity {}: {}";
    String PERSIST_DELETE_MSG = "Request delete entity {}: {}";

    String ID = "id";
    String USER = "user";
    String LOCATION = "location";
    String HASHTAG = "hashtag";
    String ANNOUNCEMENT = "announcement";
    String MARKER_IMAGE = "marker_image";

    String SEQUENCER = "hibernate_sequence";
    String USER_SEQ = "user_seq";
    String ANNOUNCEMENT_SEQ = "announcement_seq";
    String MARKER_IMG_SEQ = "marker_img_seq";
    String LOCATION_SEQ = "location_seq";
    String HASHTAG_SEQ = "hashtag_seq";
    String ERROR_MSG = "error.message";

}
