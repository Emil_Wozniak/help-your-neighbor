package pl.helpyourneighbor.config.constants;

import lombok.NoArgsConstructor;

/**
 * Constants for Spring Security authorities.
 */
@NoArgsConstructor
public final class AuthoritiesConstants {

    public static final String ROLE = "ROLE_";

    public static final String ADMIN = ROLE + "ADMIN";

    public static final String USER = ROLE + "USER";

    public static final String ANONYMOUS = ROLE + "ANONYMOUS";

}
