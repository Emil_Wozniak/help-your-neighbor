package pl.helpyourneighbor.config.constants;

public interface PathConstant {

    String ID = "/{id}";
    String MANAGE = "/management/info";

}
