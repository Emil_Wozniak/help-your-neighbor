package pl.helpyourneighbor.config.constants;

public interface UserConstants {
    String NAME = "name";
    String IMAGE_URL = "imageUrl";
    String EMAIL = "email";
    String AUTHORITIES = "authorities";
    String LOCALE = "locale";
    String PROVIDER = "provider";
    String PICTURE = "picture";
    String PHONE = "phone";
    String ACTIVATED = "activated";
    String BANNED = "banned";
    String LANG= "lang_key";

    Long DEFAULT_ID = null;
    short DEFAULT_AMOUNT = 5;
    String DEFAULT_PHONE = "000000000";
}
