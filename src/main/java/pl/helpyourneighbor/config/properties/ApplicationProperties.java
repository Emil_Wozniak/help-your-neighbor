package pl.helpyourneighbor.config.properties;

import config.constant.SecurityApplicationProperties;
import config.constant.SecurityDefaults;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Properties specific to Help Your Neighbor.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link MailProps} for a good example.
 */
@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "hyn", ignoreUnknownFields = false)
public class ApplicationProperties implements SecurityApplicationProperties {

    private final Async async = new Async();
    private final Http http = new Http();
    private final Cache cache = new Cache();
    private final Mail mail = new Mail();
    private final Metrics metrics = new Metrics();
    private final Logging logging = new Logging();
    private final Social social = new Social();
    private final Gateway gateway = new Gateway();
    private final Registry registry = new Registry();
    private final ClientApp clientApp = new ClientApp();
    private final AuditEvents auditEvents = new AuditEvents();
    private final CorsConfiguration cors = new CorsConfiguration();
    private final Security security = new HYNSecurity();

//    @Setter
//    @Value("${spring.security.oauth2.client.registration.google.clientSecret}")
//    private String secret;

    @Getter
    @Setter
    public static class Async {
        private int corePoolSize = Defaults.Async.corePoolSize;
        private int maxPoolSize = Defaults.Async.maxPoolSize;
        private int queueCapacity = Defaults.Async.queueCapacity;
    }

    @Getter
    @Setter
    public static class Http {

        private final Http.Cache cache = new Http.Cache();

        @Getter
        @Setter
        public static class Cache {

            private int timeToLiveInDays = Defaults.Http.Cache.timeToLiveInDays;
        }
    }

    @Getter
    @Setter
    public static class Cache {

        private final Cache.Hazelcast hazelcast = new Cache.Hazelcast();

        @Getter
        @Setter
        public static class Hazelcast {

            private final Cache.Hazelcast.ManagementCenter managementCenter = new Cache.Hazelcast.ManagementCenter();
            private int timeToLiveSeconds = Defaults.Cache.Hazelcast.timeToLiveSeconds;
            private int backupCount = Defaults.Cache.Hazelcast.backupCount;

            @Getter
            @Setter
            public static class ManagementCenter {

                private boolean enabled = Defaults.Cache.Hazelcast.ManagementCenter.enabled;
                private int updateInterval = Defaults.Cache.Hazelcast.ManagementCenter.updateInterval;
                private String url = Defaults.Cache.Hazelcast.ManagementCenter.url;
                public boolean isEnabled() {
                    return enabled;
                }
            }

            @Getter
            @Setter
            public static class Infinispan {

                private final Cache.Hazelcast.Infinispan.Local local = new Cache.Hazelcast.Infinispan.Local();
                private final Cache.Hazelcast.Infinispan.Distributed distributed = new Cache.Hazelcast.Infinispan.Distributed();
                private final Cache.Hazelcast.Infinispan.Replicated replicated = new Cache.Hazelcast.Infinispan.Replicated();
                private String configFile = Defaults.Cache.Infinispan.configFile;
                private boolean statsEnabled = Defaults.Cache.Infinispan.statsEnabled;

                @Getter
                @Setter
                public static class Local {

                    private long timeToLiveSeconds = Defaults.Cache.Infinispan.Local.timeToLiveSeconds;
                    private long maxEntries = Defaults.Cache.Infinispan.Local.maxEntries;

                }

                @Getter
                @Setter
                public static class Distributed {

                    private long timeToLiveSeconds = Defaults.Cache.Infinispan.Distributed.timeToLiveSeconds;
                    private long maxEntries = Defaults.Cache.Infinispan.Distributed.maxEntries;
                    private int instanceCount = Defaults.Cache.Infinispan.Distributed.instanceCount;

                }

                @Getter
                @Setter
                public static class Replicated {

                    private long timeToLiveSeconds = Defaults.Cache.Infinispan.Replicated.timeToLiveSeconds;
                    private long maxEntries = Defaults.Cache.Infinispan.Replicated.maxEntries;
                }
            }

            @Getter
            @Setter
            public static class Memcached {

                private boolean enabled = Defaults.Cache.Memcached.enabled;
                private String servers = Defaults.Cache.Memcached.servers;
                private int expiration = Defaults.Cache.Memcached.expiration;
                private boolean useBinaryProtocol = Defaults.Cache.Memcached.useBinaryProtocol;

                public boolean isEnabled() {
                    return enabled;
                }
            }
        }
    }

    @Getter
    @Setter
    public static class Mail {

        private boolean enabled = Defaults.Mail.enabled;
        private String from = Defaults.Mail.from;
        private String baseUrl = Defaults.Mail.baseUrl;
        public boolean isEnabled() {
            return enabled;
        }
    }

    @Getter
    @Setter
    public static class Metrics {

        private final Metrics.Logs logs = new Metrics.Logs();

        @Getter
        @Setter
        public static class Logs {

            private boolean enabled = Defaults.Metrics.Logs.enabled;
            private long reportFrequency = Defaults.Metrics.Logs.reportFrequency;
            public boolean isEnabled() {
                return enabled;
            }
        }
    }

    @Getter
    @Setter
    public static class Logging {

        private final Logging.Logstash logstash = new Logging.Logstash();
        private boolean useJsonFormat = Defaults.Logging.useJsonFormat;
        public boolean isUseJsonFormat() {
            return useJsonFormat;
        }

        @Getter
        @Setter
        public static class Logstash {

            private boolean enabled = Defaults.Logging.Logstash.enabled;
            private String host = Defaults.Logging.Logstash.host;
            private int port = Defaults.Logging.Logstash.port;
            private int queueSize = Defaults.Logging.Logstash.queueSize;
            public boolean isEnabled() {
                return enabled;
            }
        }
    }


    @Getter
    @Setter
    public static class Social {

        private String redirectAfterSignIn = Defaults.Social.redirectAfterSignIn;

    }

    @Getter
    @Setter
    public static class Gateway {

        private final Gateway.RateLimiting rateLimiting = new Gateway.RateLimiting();

        private Map<String, List<String>> authorizedMicroservicesEndpoints = Defaults.Gateway.authorizedMicroservicesEndpoints;

        @Getter
        @Setter
        public static class RateLimiting {

            private boolean enabled = Defaults.Gateway.RateLimiting.enabled;
            private long limit = Defaults.Gateway.RateLimiting.limit;
            private int durationInSeconds = Defaults.Gateway.RateLimiting.durationInSeconds;

            public boolean isEnabled() {
                return enabled;
            }
        }
    }

    @Getter
    @Setter
    public static class Registry {

        private String password = Defaults.Registry.password;
    }

    @Getter
    @Setter
    public static class ClientApp {

        private String name = Defaults.ClientApp.name;

    }

    @Getter
    @Setter
    public static class AuditEvents {
        private int retentionPeriod = Defaults.AuditEvents.retentionPeriod;
    }

    @Getter
    static class HYNSecurity implements Security {

        private final SecurityApplicationProperties.Security.ClientAuthorization clientAuthorization = new HYNClientAuthorization();
        private final SecurityApplicationProperties.Security.Authentication authentication = new HYNAuthentication();
        private final SecurityApplicationProperties.Security.RememberMe rememberMe = new HYNRememberMe();
        private final SecurityApplicationProperties.Security.OAuth2 oauth2 = new HYNOAuth2();

        @Getter
        @Setter
        public static class HYNClientAuthorization implements ClientAuthorization {

            private String accessTokenUri = SecurityDefaults.Security.ClientAuthorization.accessTokenUri;
            private String tokenServiceId = SecurityDefaults.Security.ClientAuthorization.tokenServiceId;
            private String clientId = SecurityDefaults.Security.ClientAuthorization.clientId;
            private String clientSecret = SecurityDefaults.Security.ClientAuthorization.clientSecret;

        }

        @Setter
        @Getter
        public static class HYNAuthentication implements Authentication {

            private final SecurityApplicationProperties.Security.Authentication.Jwt jwt = new HYNJwt();
            private List<String> authorizedRedirectUris = new ArrayList<>();

            @Setter
            @Getter
            public static class HYNJwt implements Security.Authentication.Jwt {

                private String secret = SecurityDefaults.Security.ClientAuthorization.clientSecret;
                private String base64Secret = SecurityDefaults.Security.Authentication.Jwt.base64Secret;
                private long tokenValidityInSeconds = SecurityDefaults.Security.Authentication.Jwt
                        .tokenValidityInSeconds;
                private long tokenValidityInSecondsForRememberMe = SecurityDefaults.Security.Authentication.Jwt
                        .tokenValidityInSecondsForRememberMe;
            }
        }

        @Setter
        @Getter
        public static class HYNRememberMe implements RememberMe {
            private String key = SecurityDefaults.Security.RememberMe.key;
        }

        @Getter
        public static class HYNOAuth2 implements OAuth2 {
            private final List<String> audience = new ArrayList<>();

        }
    }
}
