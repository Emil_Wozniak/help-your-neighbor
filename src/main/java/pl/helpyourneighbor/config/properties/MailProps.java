package pl.helpyourneighbor.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Properties specific to Monopoly.
 *
 * <p> Properties are configured in the application.yml file. </p>
 * <p> This class also load properties in the Spring Environment
 * from the git.properties and META-INF/build-info.properties
 * files if they are found in the classpath.</p>
 */
@ConfigurationProperties(prefix = "hyn-mail", ignoreUnknownFields = false)
@PropertySources({
        @PropertySource(value = "classpath:git.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:META-INF/build-info.properties", ignoreResourceNotFound = true)
})
public class MailProps {

    private final Mail mail = new Mail();

    @Setter
    @Getter
    public static class Mail {

        private boolean enabled = Defaults.Mail.enabled;

        private String from = Defaults.Mail.from;

        private String baseUrl = Defaults.Mail.baseUrl;

        public boolean isEnabled() {
            return enabled;
        }

    }

    public Mail getMail() {
        return mail;
    }
}
