package pl.helpyourneighbor.config.properties;

import config.constant.SecurityDefaults;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Defaults implements SecurityDefaults {

    public interface Mail {
        boolean enabled = false;
        String from = "";
        String baseUrl = "";
    }

    public interface Async {

        int corePoolSize = 2;
        int maxPoolSize = 50;
        int queueCapacity = 10000;
    }

    public interface Http {

        interface Cache {

            int timeToLiveInDays = 1461; // 4 years (including leap day)
        }
    }

    public interface Cache {

        interface Hazelcast {

            int timeToLiveSeconds = 3600; // 1 hour
            int backupCount = 1;

            interface ManagementCenter {

                boolean enabled = false;
                int updateInterval = 3;
                String url = "";
            }
        }

        interface Infinispan {

            String configFile = "default-configs/default-jgroups-tcp.xml";
            boolean statsEnabled = false;

            interface Local {

                long timeToLiveSeconds = 60; // 1 minute
                long maxEntries = 100;
            }

            interface Distributed {

                long timeToLiveSeconds = 60; // 1 minute
                long maxEntries = 100;
                int instanceCount = 1;
            }

            interface Replicated {

                long timeToLiveSeconds = 60; // 1 minute
                long maxEntries = 100;
            }
        }

        interface Memcached {

            boolean enabled = false;
            String servers = "localhost:11211";
            int expiration = 300; // 5 minutes
            boolean useBinaryProtocol = true;
        }
    }

    public interface Metrics {

        interface Logs {

            boolean enabled = false;
            long reportFrequency = 60;

        }
    }

    public interface Logging {

        boolean useJsonFormat = false;

        interface Logstash {

            boolean enabled = false;
            String host = "localhost";
            int port = 5000;
            int queueSize = 512;
        }
    }

    public interface Social {

        String redirectAfterSignIn = "/#/home";
    }

    public interface Gateway {

        Map<String, List<String>> authorizedMicroservicesEndpoints = new LinkedHashMap<>();

        interface RateLimiting {

            boolean enabled = false;
            long limit = 100_000L;
            int durationInSeconds = 3_600;

        }
    }

    interface Ribbon {

        String[] displayOnActiveProfiles = null;
    }

    public interface Registry {

        String password = null;
    }

    public interface ClientApp {

        String name = "helpYourNeighbor";
    }

    public interface AuditEvents {

        int retentionPeriod = 30;
    }
}
