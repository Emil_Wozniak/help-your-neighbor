/**
 * Internationalization configurations package.
 *
 * @author emil.wozniak.591986@gmail.com
 * @since 1.0
 */
package pl.helpyourneighbor.config.i18n;