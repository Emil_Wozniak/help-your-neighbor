package pl.helpyourneighbor.config.i18n;

import lombok.NoArgsConstructor;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.TimeZoneAwareLocaleContext;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;
import static org.springframework.util.StringUtils.*;
import static org.springframework.web.util.WebUtils.getCookie;

@NoArgsConstructor(access = PACKAGE, staticName = "localeResolver")
public class ReactCookieLocaleResolver extends CookieLocaleResolver {

    public static final String QUOTE = "%22";

    @Override
    public @NotNull Locale resolveLocale(@NotNull HttpServletRequest request) {
        parseCookieIfNecessary(request);
        return (Locale) request.getAttribute(LOCALE_REQUEST_ATTRIBUTE_NAME);
    }

    @Override
    public @NotNull LocaleContext resolveLocaleContext(final @NotNull HttpServletRequest request) {
        parseCookieIfNecessary(request);
        return new TimeZoneAwareLocaleContext() {
            @Override
            public Locale getLocale() {
                return (Locale) request.getAttribute(LOCALE_REQUEST_ATTRIBUTE_NAME);
            }

            @Override
            public TimeZone getTimeZone() {
                return (TimeZone) request.getAttribute(TIME_ZONE_REQUEST_ATTRIBUTE_NAME);
            }
        };
    }

    private void parseCookieIfNecessary(HttpServletRequest request) {
        if (request.getAttribute(LOCALE_REQUEST_ATTRIBUTE_NAME) == null) {
            val cookie = getCookie(request, Objects.requireNonNull(getCookieName()));
            Locale locale = null;
            TimeZone timeZone = null;
            if (cookie != null) {
                var value = cookie.getValue();
                value = replace(value, QUOTE, "");
                var localePart = value;
                val spaceIndex = localePart.indexOf(' ');
                String timeZonePart = null;
                if (spaceIndex != -1) {
                    localePart = value.substring(0, spaceIndex);
                    timeZonePart = value.substring(spaceIndex + 1);
                }
                locale = !"-".equals(localePart)
                        ? parseLocaleString(localePart.replace('-', '_'))
                        : null;
                if (timeZonePart != null) {
                    timeZone = parseTimeZoneString(timeZonePart);
                }
                IsTrace(cookie, locale, timeZone);
            }
            request.setAttribute(LOCALE_REQUEST_ATTRIBUTE_NAME, locale != null
                    ? locale
                    : determineDefaultLocale(request));

            request.setAttribute(TIME_ZONE_REQUEST_ATTRIBUTE_NAME, timeZone != null
                    ? timeZone
                    : determineDefaultTimeZone(request));
        }
    }

    private void IsTrace(Cookie cookie, Locale locale, TimeZone timeZone) {
        if (logger.isTraceEnabled()) {
            logger.trace("Parsed cookie value [" + cookie.getValue() + "] into locale '" + locale +
                    "'" + (timeZone != null ? " and time zone '" + timeZone.getID() + "'" : ""));
        }
    }

    String quote(String string) {
        return QUOTE + string + QUOTE;
    }
}
