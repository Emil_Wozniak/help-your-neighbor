package pl.helpyourneighbor.config;

import config.OAuthClients;
import config.OAuthConfig;
import config.SecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {SecurityConfig.class, OAuthConfig.class, OAuthClients.class})
class ApplicationSecurity {
}
