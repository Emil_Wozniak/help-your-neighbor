package pl.helpyourneighbor.config;

import config.SecurityWebConfig;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import pl.helpyourneighbor.config.properties.ApplicationProperties;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.EnumSet;

import static java.net.URLDecoder.decode;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.servlet.DispatcherType.*;
import static org.springframework.boot.web.server.MimeMappings.DEFAULT;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;
import static pl.helpyourneighbor.config.constants.AppProfile.SPRING_PROFILE_PRODUCTION;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Slf4j
@Value
@NonFinal
@Configuration
@RequiredArgsConstructor
class WebConfig implements SecurityWebConfig, ServletContextInitializer, WebServerFactoryCustomizer<WebServerFactory> {

    ApplicationProperties properties;
    Environment env;

    @Override
    public void onStartup(ServletContext servletContext) {
        if (env.getActiveProfiles().length != 0) {
            log.info("Web application configuration, using profiles: {}", (Object[]) env.getActiveProfiles());
        }
        EnumSet<DispatcherType> types = EnumSet.of(REQUEST, FORWARD, ASYNC);
        if (env.acceptsProfiles(Profiles.of(SPRING_PROFILE_PRODUCTION))) {
            initCachingHttpHeadersFilter(servletContext, types);
        }
        log.info("Web application fully configured");
    }

    /**
     * Initializes the caching HTTP Headers Filter.
     */
    @SuppressWarnings("unused")
    private void initCachingHttpHeadersFilter(ServletContext servletContext, EnumSet<DispatcherType> types) {
        log.debug("Registering Caching HTTP Headers Filter");
    }

    @Bean
    public CorsFilter corsFilter() {
        val source = new UrlBasedCorsConfigurationSource();
        val config = properties.getCors();
        if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
            log.debug("Registering CORS filter");
            source.registerCorsConfiguration("/api/**", config);
            source.registerCorsConfiguration("/management/**", config);
        }
        return new CorsFilter(source);
    }


    @Override
    public void customize(WebServerFactory factory) {
        setMimeMappings(factory);
        setLocationForStaticAssets(factory);
    }

    @Override
    public void setMimeMappings(WebServerFactory server) {
        if (server instanceof ConfigurableServletWebServerFactory) {
            val mappings = new MimeMappings(DEFAULT);
            mappings.add("html", TEXT_HTML_VALUE + ";charset=" + UTF_8.name().toLowerCase());
            mappings.add("json", TEXT_HTML_VALUE + ";charset=" + UTF_8.name().toLowerCase());
            var servletWebServer = (ConfigurableServletWebServerFactory) server;
            servletWebServer.setMimeMappings(mappings);
        }
    }

    @Override
    public void setLocationForStaticAssets(WebServerFactory server) {
        if (server instanceof ConfigurableServletWebServerFactory) {
            ConfigurableServletWebServerFactory servletWebServer = (ConfigurableServletWebServerFactory) server;
            File root;
            val prefixPath = resolvePathPrefix();
            root = new File(prefixPath + "build/resources/main/static/");
            if (root.exists() && root.isDirectory()) {
                servletWebServer.setDocumentRoot(root);
            }
        }
    }

    /**
     * Resolve path prefix to static resources.
     */
    @Override
    public String resolvePathPrefix() {
        val fullExecutablePath = Try
                .of(() -> decode(this.getClass().getResource("").getPath(), UTF_8.name()))
                .recover(UnsupportedEncodingException.class, error -> this.getClass().getResource("").getPath())
                .get();
        val rootPath = Paths.get(".").toUri().normalize().getPath();
        val extractedPath = fullExecutablePath.replace(rootPath, "");
        val extractionEndIndex = extractedPath.indexOf("build/");
        return extractionEndIndex <= 0 ? "" : extractedPath.substring(0, extractionEndIndex);
    }
}
