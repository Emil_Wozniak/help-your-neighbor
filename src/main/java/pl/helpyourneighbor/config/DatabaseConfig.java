package pl.helpyourneighbor.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "pl.helpyourneighbor.repo")
class DatabaseConfig {
}
