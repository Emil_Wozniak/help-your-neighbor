package pl.helpyourneighbor.config.profiles;

import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;
import static pl.helpyourneighbor.config.constants.AppProfile.SPRING_PROFILE_DEVELOPMENT;

@NoArgsConstructor(access = PRIVATE)
public class ProfileUtil {

    private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

    /**
     * Set a default to use when no profile is configured.
     *
     * @param app the Spring application.
     */
    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap<>();
        /*
         * The default profile to use when no other profiles are defined
         * This cannot be set in the application.yml file.
         * See https://github.com/spring-projects/spring-boot/issues/1219
         */
        defProperties.put(SPRING_PROFILE_DEFAULT, SPRING_PROFILE_DEVELOPMENT);
        app.setDefaultProperties(defProperties);
    }
}
