package pl.helpyourneighbor.config.errors;

import static pl.helpyourneighbor.config.errors.ErrorConstants.LOGIN_ALREADY_USED_TYPE;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public LoginAlreadyUsedException() {
        super(
            LOGIN_ALREADY_USED_TYPE,
            "LoginContainer name already used!",
            "userManagement",
            "user-exists");
    }
}
