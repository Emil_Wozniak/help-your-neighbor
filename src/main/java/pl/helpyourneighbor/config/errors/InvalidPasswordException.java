package pl.helpyourneighbor.config.errors;

import org.zalando.problem.AbstractThrowableProblem;

import static org.zalando.problem.Status.*;
import static pl.helpyourneighbor.config.errors.ErrorConstants.*;

public class InvalidPasswordException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public InvalidPasswordException() {
        super(INVALID_PASSWORD_TYPE, "Incorrect password", BAD_REQUEST);
    }
}
