package pl.helpyourneighbor.config.errors;

import static pl.helpyourneighbor.config.errors.ErrorConstants.EMAIL_HAS_NO_EMPLOYEE_TYPE;

public class NotEmployeeEmailError extends BadRequestAlertException {
    private static final long serialVersionUID = 1L;

    public NotEmployeeEmailError() {
        super(
            EMAIL_HAS_NO_EMPLOYEE_TYPE,
            "Email is not belonging to any employee!",
            "userManagement",
            "nonEmployeeEmail");
    }
}
