package pl.helpyourneighbor.config.errors;

public class DeserializerException extends RuntimeException {
    public DeserializerException(String message) {
        super(message);
    }
}
