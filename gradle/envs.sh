#!/usr/bin/env bash
input=".env"
while IFS= read -r line
do
  echo "$line"
  export line
done < "$input"