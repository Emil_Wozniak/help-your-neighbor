package pl.helpyourneighbor;

import org.openjdk.jmh.annotations.Benchmark;

public class BenchMark {

    @Benchmark
    public double foldedLog() {
        int x = 8;

        return Math.log(x);
    }
}
