package pl.helpyourneighbor;

import java.io.IOException;

public class BenchmarkRunner {
    public static void main(String[] args) {
        try {
            org.openjdk.jmh.Main.main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
