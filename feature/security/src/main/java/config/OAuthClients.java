package config;

import config.constant.AuthoritiesConstant;
import io.vavr.Tuple2;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;

import java.util.Arrays;
import java.util.List;

import static config.constant.AuthoritiesConstant.CLIENT_ID;
import static config.constant.AuthoritiesConstant.CLIENT_SECRET;

@Value
@NonFinal
@Configuration
@RequiredArgsConstructor
public class OAuthClients {

    Environment environment;

    @Bean
    ClientRegistrationRepository clientRegistration() {
        return new InMemoryClientRegistrationRepository(clients("google", "github"));
    }

    /**
     * Finds {@link CommonOAuth2Provider} matching one of the {@param names} value.
     * <p>
     * CommonOAuth2Provider instance <b>requires</b> <i><b>environment properties</b></i>
     * equals to <b>CLIENT_NAME</b> + CLIENT_ID and <b>CLIENT_NAME</b> + CLIENT_SECRET.
     *
     * @param names registration client names
     * @return list of ClientRegistration instances
     * @see AuthoritiesConstant#CLIENT_ID
     * @see AuthoritiesConstant#CLIENT_SECRET
     */
    private List<ClientRegistration> clients(String... names) {
        return Stream
                .ofAll(Arrays.asList(CommonOAuth2Provider.values()))
                .zipWithIndex()
                .filter(it -> it._1().name().equals(getName(it._2(), names)))
                .map(Tuple2::_1)
                .map(it -> it
                        .getBuilder(it.name().toLowerCase())
                        .clientId(environment.getRequiredProperty(it.name() + CLIENT_ID))
                        .clientSecret(environment.getRequiredProperty(it.name() + CLIENT_SECRET))
                        .build())
                .toJavaList();
    }

    @NotNull
    private String getName(int it, String[] names) {
        return it < names.length ? names[it].toUpperCase() : "null";
    }
}
