package config;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static config.constant.JwtConstant.JWT_ERROR_CODE;
import static config.constant.SecurityConstant.*;
import static java.util.Objects.requireNonNull;
import static org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;

@Configuration
public class OAuthConfig {

    @NotNull
    Predicate<Map<String, Object>> login = attributes -> SPRING_PROJECT.equals(attributes.get("login"));
    Predicate<ClientRegistration> isNotGithub = registration -> !GITHUB.equals(registration.getRegistrationId());

    @Bean
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public WebClient rest(
            ClientRegistrationRepository clients,
            OAuth2AuthorizedClientRepository auth) {
        val oauth2 = new ServletOAuth2AuthorizedClientExchangeFilterFunction(clients, auth);
        return WebClient.builder().filter(oauth2).build();
    }

    /**
     * It will use AccessToken to grand Client registration and get {@link OAuth2User}
     * from OAuth resource server like Github or Google.
     *
     * @param rest requesting web client.
     * @return service holding {@link OAuth2User}.
     */
    @Bean
    public OAuth2UserService<OAuth2UserRequest, OAuth2User> oauth2UserService(WebClient rest) {
        val delegate = new DefaultOAuth2UserService();
        return (request) -> {
            val tmp = delegate.loadUser(request);
            val registration = request.getClientRegistration();
            return isNotGithub.test(registration)
                    ? appendAdditionalAttributes(request, tmp)
                    : githubUser(rest, request, tmp, registration);
        };
    }

    /**
     * Rebuild DefaultOAuth2User instance by assigning additional data to it.
     *
     * @param request the {@link OAuth2UserService} request
     * @param user     {@link DefaultOAuth2UserService#loadUser} response as {@link DefaultOAuth2User}
     * @return new instance of the {@link DefaultOAuth2User} with additional data in the attributes.
     */
    @NotNull
    private DefaultOAuth2User appendAdditionalAttributes(OAuth2UserRequest request, OAuth2User user) {
        val registration = request.getClientRegistration();
        val name = getRequestName(request);
        val attributes = user.getAttributes();
        val provider = registration.getRegistrationId();
        Map<String, Object> ext = new HashMap<>(attributes);
        ext.put("provider", provider);
        ext.put("activated", true);
        val authorities = user.getAuthorities();
        return new DefaultOAuth2User(authorities, ext, name);
    }

    private String getRequestName(OAuth2UserRequest request) {
        return request
                .getClientRegistration()
                .getProviderDetails()
                .getUserInfoEndpoint()
                .getUserNameAttributeName();
    }

    private OAuth2User githubUser(
            WebClient rest,
            OAuth2UserRequest request,
            OAuth2User user,
            ClientRegistration registration) {
        if (isLogin(rest, request, user, registration)) {
            return appendAdditionalAttributes(request, user);
        }
        throw new OAuth2AuthenticationException(new OAuth2Error(JWT_ERROR_CODE, OAUTH_DESC, ""));
    }

    private boolean isLogin(
            WebClient rest,
            OAuth2UserRequest request,
            OAuth2User user,
            ClientRegistration registration) {
        return requireNonNull(getResponse(rest, request, user, registration))
                .stream()
                .anyMatch(login);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private List<Map<String, Object>> getResponse(
            WebClient rest,
            OAuth2UserRequest request,
            OAuth2User user,
            ClientRegistration registration) {
        return rest
                .get()
                .uri(getOrgUrl(user))
                .attributes(userAttributes(request, user, registration))
                .retrieve()
                .bodyToMono(List.class)
                .log()
                .block();
    }

    @NotNull
    private Consumer<Map<String, Object>> userAttributes(
            OAuth2UserRequest request,
            OAuth2User user,
            ClientRegistration registration) {
        val client = getOAuthClient(request, user, registration);
        return oauth2AuthorizedClient(client);
    }

    @NotNull
    private OAuth2AuthorizedClient getOAuthClient(
            OAuth2UserRequest request,
            OAuth2User user,
            ClientRegistration registration) {
        val name = user.getName();
        val token = request.getAccessToken();
        return new OAuth2AuthorizedClient(registration, name, token);
    }

    private String getOrgUrl(OAuth2User user) {
        return requireNonNull(user.getAttribute(ORG_URL));
    }
}

