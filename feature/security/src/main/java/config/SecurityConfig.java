package config;

import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CorsFilter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import static config.constant.AuthoritiesConstant.ADMIN;
import static config.constant.SecurityConstant.CSP;
import static config.constant.SecurityConstant.POLICY;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.security.web.csrf.CookieCsrfTokenRepository.withHttpOnlyFalse;
import static org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN;

@Slf4j
@EnableWebSecurity
@RequiredArgsConstructor
@Import(SecurityProblemSupport.class)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final CorsFilter corsFilter;
    private final SecurityProblemSupport problemSupport;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(OPTIONS, "/**")
                .antMatchers("/app/**/*.{js,html}")
                .antMatchers("/i18n/**")
                .antMatchers("/content/**")
                .antMatchers("/test/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        final var handler = new SimpleUrlAuthenticationFailureHandler("/");
        // @formatter:off
                http
                    .csrf()
                    .csrfTokenRepository(withHttpOnlyFalse())
                .and()
                    .addFilterBefore(corsFilter, CsrfFilter.class)
                    .exceptionHandling()
                    .authenticationEntryPoint(problemSupport)
                    .accessDeniedHandler(problemSupport)
                .and()
                    .headers()
                    .contentSecurityPolicy(CSP)
                .and()
                    .referrerPolicy(STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
                    .and()
                    .featurePolicy(POLICY)
                .and()
                    .frameOptions()
                    .deny()
                .and()
                    .authorizeRequests()
                    .antMatchers("/api/**").permitAll()
                    .antMatchers("/route/register/**").permitAll()
                    .antMatchers("/route/account/**").permitAll()
                    .antMatchers("/route/provider/**").permitAll()
                    .antMatchers("/route/management/**").permitAll()
                    .antMatchers("/route/**").authenticated()
                    .antMatchers("/management/health").permitAll()
                    .antMatchers("/management/info").permitAll()
                    .antMatchers("/management/**").hasAuthority(ADMIN)
                .and()
                   .logout().logoutSuccessUrl("/").permitAll()
                .and()
                    .oauth2Login(oauth -> oauth.failureHandler((request, response, exception) -> {
                    request.getSession().setAttribute("error.message", exception.getMessage());
                    handler.onAuthenticationFailure(request,response,exception);
                }));
        // @formatter:on
    }

    /**
     * Map authorities from "groups" or "roles" claim in ID Token.
     *
     * @return a {@link GrantedAuthoritiesMapper} that maps groups from
     * the IdP to Spring Security Authorities.
     */
    @Bean
    public GrantedAuthoritiesMapper userAuthoritiesMapper() {
        return (authorities) -> Stream
                .ofAll(authorities)
                .filter(authority -> authority instanceof OidcUserAuthority)
                .toJavaSet();
    }
}

