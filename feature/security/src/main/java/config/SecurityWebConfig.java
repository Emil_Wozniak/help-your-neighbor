package config;

import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.web.filter.CorsFilter;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
public interface SecurityWebConfig {

    void setMimeMappings(WebServerFactory server);

    void setLocationForStaticAssets(WebServerFactory server);

    /**
     * Resolve path prefix to static resources.
     */
    String resolvePathPrefix();
}
