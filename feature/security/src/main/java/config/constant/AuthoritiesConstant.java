package config.constant;

import lombok.NoArgsConstructor;

/**
 * Constants for Spring Security authorities.
 */
@NoArgsConstructor
public final class AuthoritiesConstant {

    public static final String ROLE = "ROLE_";

    public static final String ADMIN = ROLE + "ADMIN";

    public static final String GOOGLE = "GOOGLE";
    public static final String GITHUB = "GITHUB";

    public static final String CLIENT_ID = "_CID";
    public static final String CLIENT_SECRET = "_CS";

//    public static final String GOOGLE_CLIENT_ID = GOOGLE + CLIENT_ID;
//    public static final String GOOGLE_CLIENT_SECRET = GOOGLE + CLIENT_SECRET;
//    public static final String GITHUB_CLIENT_ID = GITHUB + CLIENT_ID;
//    public static final String GITHUB_CLIENT_SECRET = GITHUB + CLIENT_SECRET;


}
