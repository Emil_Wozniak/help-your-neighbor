package config.constant;

import static config.cors.CSPDirectives.create;
import static config.cors.CSP_URLS.*;

public final class SecurityConstant {

    public static final String OAUTH_DESC = "Not in Spring Team";
    public static final String ORG_URL = "organizations_url";
    public static final String GITHUB = "github";
    public static final String SPRING_PROJECT = "spring-projects";
    public static final String POLICY = "geolocation *; midi 'none'; sync-xhr 'none'; microphone 'none'; camera 'none'; magnetometer 'none'; gyroscope 'none'; fullscreen 'self'; payment 'none'";
    /**
     * Security config.constant package private only
     */
    public static final String CSP = create(DEFAULTS, FRAMES, SCRIPTS, STYLES, IMAGES, FONTS, true);
}

