package config.constant;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Properties are configured in the {@code application.yml} file
 */
@Configuration
@SuppressWarnings("ConfigurationProperties")
@ConfigurationProperties(prefix = "hyn", ignoreUnknownFields = false)
public interface SecurityApplicationProperties {

    Security getSecurity();

    CorsConfiguration getCors();

//    String getSecret();

    interface Security {

        ClientAuthorization getClientAuthorization();

        Authentication getAuthentication();

        RememberMe getRememberMe();

        OAuth2 getOauth2();

        interface ClientAuthorization {
        }

        interface Authentication {
            Jwt getJwt();

            interface Jwt {
                long getTokenValidityInSeconds();

                String getSecret();
            }
        }

        interface RememberMe {
        }

        interface OAuth2 {
        }
    }

}
