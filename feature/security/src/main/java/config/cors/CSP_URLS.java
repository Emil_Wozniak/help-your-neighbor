package config.cors;

/**
 * Security config.constant package private only
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public interface CSP_URLS {
    String UNSAFE = " 'unsafe-inline' 'unsafe-eval' ; ";
    String G_STATIC = "https://fonts.gstatic.com ";
    String WE_LOVE_FONT_FONTS = "https://weloveiconfonts.com/api/fonts/entypo/entypo.woff https://weloveiconfonts.com/api/fonts/entypo/entypo.ttf ";
    String WE_LOVE_FONT = "http://weloveiconfonts.com/api/ ";
    String EMAIL_JS = "https://api.emailjs.com/api/v1.0/email/send ";
    String GOOGLE_STORAGE = "https://storage.googleapis.com ";
    String GOOGLE_APIS = "https://apis.google.com/js/api.js ";
    String GOOGLE_API = "https://apis.google.com/_/scs/apps-static/_/js/k=oz.gapi.pl.w-vC-amqpTQ.O/m=auth2/rt=j/sv=1/d=1/ed=1/am=wQE/rs=AGLTcCMNvasC4WzCtM8QIQeUjHE5IBzUMw/cb=gapi.loaded_0 ";
    String GOOGLE_FONTS = "https://fonts.googleapis.com ";
    String GOOGLE_ACCOUNT = "https://accounts.google.com/ ";
    String GOOGLE_IMAGE = "https://lh3.googleusercontent.com/ https://lh4.googleusercontent.com/ ";
    String MAP_URL = "http://www.destination360.com/europe/poland/warsaw/hilton-warsaw-map.gif ";
    String LEAFLET_CSS = "https://unpkg.com/leaflet@1.6.0/dist/leaflet.css ";
    String LEAFLET_JS = "https://unpkg.com/leaflet@1.6.0/dist/leaflet.js ";
    String LEAFLET_IMG = "https://unpkg.com/leaflet@1.6.0/dist/images";

    String OSM_A_IMG = "http://a.tile.osm.org ";
    String OSM_B_IMG = "http://b.tile.osm.org ";
    String OSM_C_IMG = "http://c.tile.osm.org ";

    String CyclOSM_A_IMG = "https://a.tile-cyclosm.openstreetmap.fr/cyclosm/ ";
    String CyclOSM_B_IMG = "https://b.tile-cyclosm.openstreetmap.fr/cyclosm/ ";
    String CyclOSM_C_IMG = "https://c.tile-cyclosm.openstreetmap.fr/cyclosm/ ";

    String LEAFLET_SHADOW = LEAFLET_IMG + "/marker-shadow.png ";
    String LEAFLET_ICON = LEAFLET_IMG + "/marker-icon.png ";
    String LEAFLET_LAYER = LEAFLET_IMG + "/layers.png ";
    String GEOCODE = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/ ";
    String GAPI = GOOGLE_API + GOOGLE_APIS + GOOGLE_STORAGE;

    String ESRI = "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/ ";

    String DEFAULTS = GOOGLE_FONTS + EMAIL_JS + GEOCODE + " ";
    String FRAMES = GOOGLE_ACCOUNT;
    String SCRIPTS = LEAFLET_JS + EMAIL_JS + GAPI + GOOGLE_ACCOUNT;
    String STYLES = GOOGLE_FONTS + WE_LOVE_FONT_FONTS + WE_LOVE_FONT + LEAFLET_CSS;
    String OSM_IMAGES = OSM_A_IMG + OSM_B_IMG + OSM_C_IMG;
    String CyclOSM_C_IMG_IMAGES = CyclOSM_A_IMG + CyclOSM_B_IMG + CyclOSM_C_IMG;
    String IMAGES = GOOGLE_IMAGE + MAP_URL + LEAFLET_LAYER + LEAFLET_ICON + LEAFLET_SHADOW + OSM_IMAGES + CyclOSM_C_IMG_IMAGES + ESRI;
    String FONTS = G_STATIC + WE_LOVE_FONT_FONTS;

}
