package config.cors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static config.cors.CSPDirectives.Directive.*;
import static config.cors.CSP_URLS.UNSAFE;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

/**
 * Generates CSP for {@link Directive}, which matches some of the Content Security Policy
 * (CSP).
 * <p>
 * Params will be assign to the directive with a proper rules such as 'unsafe-eval' 'unsafe-inline'.
 *
 * See <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy">CSP</a>
 */
public interface CSPDirectives {

    Logger log = LoggerFactory.getLogger(CSPDirectives.class);

    String CSP_SUFFIX = "-src ";

    static String create(
            String defaults,
            String frames,
            String scripts,
            String styles,
            String images,
            String fonts,
            boolean unsafeAll) {
        return generate(
                Map.of(
                        DEFAULT, defaults,
                        FRAME, frames,
                        SCRIPT, scripts,
                        STYLE, styles,
                        IMG, images,
                        FONT, fonts
                ),
                unsafeAll);
    }

    enum Directive {
        DEFAULT,
        FRAME,
        SCRIPT,
        STYLE,
        IMG,
        FONT;

        protected static String generate(Map<Directive, String> rules, boolean unsafeAll) {
            final var self = "'self' ";
            final var data = "data: ";
            final var blob = "blob: ";
            return rules
                    .entrySet()
                    .stream()
                    .map(it -> {
                        final var directive = it.getKey();
                        final var rule = it.getValue();
                        switch (directive) {
                            case DEFAULT:
                            case STYLE:
                            case SCRIPT:
                                return getName(directive) + self + rule;
                            case FRAME:
                            case FONT:
                                return getName(directive) + self + data + rule;
                            case IMG:
                                return getName(directive) + self + data + blob + rule;
                            default:
                                return "";
                        }
                    })
                    .sorted()
                    .map(it -> it + (unsafeAll ? UNSAFE : ""))
                    .collect(toSet())
                    .stream().collect(joining("", "", ""));
        }

        private static String getName(Directive directive) {
            return directive.name().toLowerCase() + CSP_SUFFIX;
        }
    }

}
