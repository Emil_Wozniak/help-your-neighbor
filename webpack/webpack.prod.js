const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WorkboxPlugin = require("workbox-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const sass = require("sass");
const path = require("path");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const writeFilePlugin = require("write-file-webpack-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MergeJsonWebpackPlugin = require('merge-jsons-webpack-plugin');

const utils = require("./utils.js");

const getTsLoaderRule = env => {
    const rules = [
        {
            loader: "cache-loader",
            options: {cacheDirectory: path.resolve("build/cache-loader")}
        },
        {
            loader: "thread-loader",
            options: {workers: require("os").cpus().length - 1}
        },
        {
            loader: "ts-loader",
            options: {
                transpileOnly: true,
                happyPackMode: true
            }
        }
    ];
    return rules;
};

const ENV = "production";

module.exports = {
    mode: ENV,
    entry: {main: "./src/main/webapp/app/index"},
    output: {
        path: utils.root("build/resources/main/static/"),
        filename: "app/[name].[hash].bundle.js",
        chunkFilename: "app/[name].[hash].chunk.js"
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
        modules: ["node_modules"],
        alias: utils.mapTypescriptAliasToWebpackAlias()
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: getTsLoaderRule(ENV),
                include: [utils.root("./src/main/webapp/app")],
                exclude: [utils.root("node_modules")]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: "react-svg-loader",
                        options: {
                            jsx: true,
                            tsx: true,
                            ts: true
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg|woff2?|ttf|eot)$/i,
                loader: "file-loader",
                options: {
                    digest: "hex",
                    hash: "sha512",
                    name: "content/[hash].[ext]"
                }
            },
            {
                enforce: "pre",
                test: /\.jsx?$/,
                loader: "source-map-loader"
            },
            {
                test: /\.([jt])sx?$/,
                enforce: "pre",
                loader: "eslint-loader",
                exclude: [utils.root("node_modules")]
            },
            {
                enforce: "pre",
                test: /\.s?css$/,
                loader: "stripcomment-loader"
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../"
                        }
                    },
                    "css-loader",
                    "postcss-loader",
                    {
                        loader: "sass-loader",
                        options: {implementation: sass}
                    }
                ]
            }
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendors",
                    chunks: "all"
                }
            }
        },
        runtimeChunk: false,
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                terserOptions: {
                    ecma: 6,
                    toplevel: true,
                    module: true,
                    beautify: false,
                    comments: false,
                    compress: {
                        warnings: false,
                        ecma: 6,
                        module: true,
                        toplevel: true
                    },
                    output: {
                        comments: false,
                        beautify: false,
                        indent_level: 2,
                        ecma: 6
                    },
                    mangle: {
                        keep_fnames: true,
                        module: true,
                        toplevel: true
                    }
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "content/[name].[hash].css",
            chunkFilename: "content/[name].[hash].css"
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: true
        }),
        new WorkboxPlugin.GenerateSW({
            clientsClaim: true,
            skipWaiting: true
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({title: "Development"}),
        new FriendlyErrorsWebpackPlugin(),
        new BrowserSyncPlugin(
            {
                host: "localhost",
                port: 9000,
                proxy: {
                    target: `http://localhost:9060`, proxyOptions: {changeOrigin: false}
                },
                socket: {clients: {heartbeatTimeout: 60000}}
            },
            {reload: false}
        ),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new writeFilePlugin(),
        new webpack.WatchIgnorePlugin([utils.root("src/test")]),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: `'${ENV}'`,
                BUILD_TIMESTAMP: `'${new Date().getTime()}'`,
                VERSION: `'${process.env.hasOwnProperty("APP_VERSION") ? process.env.APP_VERSION : "DEV"}'`,
                DEBUG_INFO_ENABLED: true
            }
        }),
        new HtmlWebpackPlugin({
            template: "./src/main/webapp/index.html",
            chunksSortMode: "auto",
            inject: "body",
            base: "/"
        }),
        new MergeJsonWebpackPlugin({
            output: {
                groupBy: [
                    {pattern: "./src/main/webapp/i18n/pl/*.json", fileName: "./i18n/pl.json"},
                    {pattern: "./src/main/webapp/i18n/en/*.json", fileName: "./i18n/en.json"}
                ]
            }
        }),
    ].filter(Boolean)
};

