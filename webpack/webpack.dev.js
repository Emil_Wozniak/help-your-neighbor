const webpack = require("webpack");
const utils = require("./utils.js");
const sass = require("sass");
const path = require("path");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const writeFilePlugin = require("write-file-webpack-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MergeJsonWebpackPlugin = require('merge-jsons-webpack-plugin');

const ENV = "development";

const getTsLoaderRule = env => {
    const rules = [
        {
            loader: "cache-loader",
            options: {cacheDirectory: path.resolve("build/cache-loader")}
        },
        {
            loader: "thread-loader",
            options: {workers: require("os").cpus().length - 1}
        },
        {
            loader: "ts-loader",
            options: {
                transpileOnly: true,
                happyPackMode: true
            }
        }
    ];
    if (env === "development") {
        rules.unshift({
            loader: "react-hot-loader/webpack"
        });
    }
    return rules;
};

// noinspection JSUnresolvedFunction
module.exports = {
    devtool: "cheap-module-source-map",
    mode: ENV,
    entry: ["./src/main/webapp/app/index"],
    output: {
        path: utils.root("build/resources/main/static/"),
        filename: "app/[name].bundle.js",
        chunkFilename: "app/[id].chunk.js"
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
        modules: ["node_modules"],
        alias: utils.mapTypescriptAliasToWebpackAlias()
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: getTsLoaderRule(ENV),
                include: [utils.root("./src/main/webapp/app")],
                exclude: [utils.root("node_modules")],
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader",
                    {
                        loader: "sass-loader",
                        options: {implementation: sass}
                    }
                ]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'svg-react-loader',
                        options: {
                            jsx: true,
                            tsx: true,
                            ts: true
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg|woff2?|ttf|eot)$/i,
                loader: "file-loader",
                options: {
                    digest: "hex",
                    hash: "sha512",
                    name: "content/[hash].[ext]"
                }
            },
            {
                enforce: "pre",
                test: /\.jsx?$/,
                loader: "source-map-loader"
            },
            {
                test: /\.([jt])sx?$/,
                enforce: "pre",
                exclude: [utils.root("node_modules")]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: '@svgr/webpack',
                        options: { babel: false },
                    }
                ],
            },
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendors",
                    chunks: "all"
                }
            }
        }
    },
    devServer: {
        hot: true,
        contentBase: "./build/resources/main/static/",
        proxy: [
            {
                context: ["/route", "/route/register"],
                target: `http://localhost:8088`,
                secure: false,
                changeOrigin: true
            }
        ],
        watchOptions: {ignored: /node_modules/},
        historyApiFallback: true
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({title: "Development"}),
        new FriendlyErrorsWebpackPlugin(),
        new BrowserSyncPlugin(
            {
                host: "localhost",
                port: 9000,
                proxy: {
                    target: `http://localhost:9060`,
                    proxyOptions: {changeOrigin: false}
                },
                socket: {clients: {heartbeatTimeout: 60000}}
            },
            {
                reload: false
            }
        ),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new writeFilePlugin(),
        new webpack.WatchIgnorePlugin([utils.root("src/test")]),
        new webpack.EnvironmentPlugin(['NODE_ENV', 'DEBUG']),
        new webpack.DefinePlugin({
            "process.env": JSON.stringify({
                NODE_ENV: `'${ENV}'`,
                BUILD_TIMESTAMP: `'${new Date().getTime()}'`,
                VERSION: `'${process.env.hasOwnProperty("APP_VERSION") ? process.env.APP_VERSION : "DEV"}'`,
                DEBUG_INFO_ENABLED: true,
                SERVER_API_URL: `8088`
            })
        }),
        new CopyWebpackPlugin([
            {from: './src/main/webapp/content/', to: 'content'},
            {from: './src/main/webapp/favicon.ico', to: 'favicon.ico'},
            {from: './src/main/webapp/manifest.json', to: 'manifest.json'},
            {from: './src/main/webapp/robots.txt', to: 'robots.txt'}
        ]),
        new HtmlWebpackPlugin({
            template: "./src/main/webapp/index.html",
            chunksSortMode: "auto",
            inject: "body",
            base: "/"
        }),
        new MergeJsonWebpackPlugin({
            output: {
                groupBy: [
                    {pattern: "./src/main/webapp/i18n/pl/*.json", fileName: "./i18n/pl.json"},
                    {pattern: "./src/main/webapp/i18n/en/*.json", fileName: "./i18n/en.json"}
                ]
            }
        }),
    ].filter(Boolean)
};
