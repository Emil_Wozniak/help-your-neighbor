const webpack = require("webpack");
const developmentConfig = require("./webpack.dev.js");
const productionConfig = require("./webpack.prod.js");

module.exports = env => {
  switch (env.NODE_ENV) {
    case "development":
      return developmentConfig;
    case "production":
      return productionConfig;
    default:
      throw new Error("No matching configuration was found!");
  }
};
