# Help Your Neighbor

[[_TOC_]]

## Development

To run project you should have installed all requirements (below)
and execute `yarn install` before project start.

Note that some files I formatted to increase readability, if you are using IntelliJ go
to `File > Settings > Editor > Code Style` select `Formatter Control` and
`Enable formatter markers in comments`

## React

Application includes frontend in [Webapp](src/main/webapp), if you want to activate it in development mode you need to
have **nodejs** and **Yarn**.

Front project will be automatically fired up for you on application start up.

#### Frontend development

Notice that yarn is not working on watch mode by default, if you want to keep track changes on **React**
project you have to run manually, instruction below.

Terminal first `yarn install` then `yarn start` inside working directory, I don't recommend run inside IDE

## PostgreSQL

Project is using PostgreSQL, you need to have it installed on your local machine.

Database configuration:

- PORT: *5432*
- USER: *pomoz*
- PASSWORD: *pomoz*
- DATABASE: *help*

If your connection fail please check configuration files:

Resources:
[DEV](src/main/resources/application-dev.yml)
[MAIN](src/main/resources/application.yml)
[PROD](src/main/resources/application-prod.yml)

## Structure

[Spring](src/main/java/pl/pl.helpyourneighbor) should include only Spring logic

[Webapp](src/main/webapp) React frontend client

[Assets](assets) includes additional documentation

[Authorization](authorization) Spring Security configuration

## Requirements

- Gradle        __6.7.1__
- Groovy        __3.0.6__               JVM: 11.0.6 Vendor: Eclipse OpenJ9 OS: Linux or similar
- Java          __11__                  (11.0.6.j9-adpt), Java 15 can cause failure in Gradle compiler
- Kotlin        __1.4.10-release-411__  (JRE 11.0.6+10) or latest (DSL only)
- Nodejs        __15.3.0__
- Yarn          __1.22.10__
- PostgreSQL    __psql (PostgreSQL) 12.5__

Change gradle wrapper
`
./gradle wrapper --gradle-version {version}
`

## Test cases

Fake user credentials fot OAuth

- login: jsasiad25@gmail.com
- hasło: Pomozsasiadowi1

## Context

Default liquibase context is *faker*, this property is specified in
[Gradle build file](./build.gradle) in liquibase task

### Custom context

You can use custom context by specifying it as a parameter.

Example:
```gradle liquibaseUpdate -Pcontexts=prod```

## RSocket

To obtain connection with RSocket endpoint install [this tool](https://github.com/making/rsc)

In above repository you can find installation instruction for all popular OS, including Windows and Linux.

Usage:

```bash
rsc tcp://localhost:8888 --stream --route announcements --log --debug -l assets/socket-request.json
```

## Benchmark

To fire all benchmark tests:

```bash
gradle feature:benchmark:jmh
```

## Security

To maintain high level of security please use [Snyk](https://snyk.io/) to investigate
[NPM](./package.json) and [Maven](./build.gradle) vulnerabilities. 

Snyk will help also investigate current dependencies versions. 

## Tech stack

Languages:

- **Java**
- **Kotlin**
- **Groovy**
- **Typescript**    __4.1.2__
- **Javascript**

Frameworks:

- Spring            __2.4.0__
- Hibernate         __5.4.15.Final__
- Web
- JDBC
- PostgreSQL        __42.2.18__ depends on Spring
- RSocket
- Spock             __2.0-M4-groovy-3.0__ or latest
- Reactjs           __16.13.1__ or latest
- Webflux           *OAuth2 Client only*

Tools:

- Lombok            __1.18.12__
- Bytebuddy
- Vavr              __0.10.2__
- Common math       __3.6.1__
- Zalando           __0.26.1__
- Liquibase plugin: __2.0.4__, hibernate: __3.10.0__
- Gradle            __6.7.1__